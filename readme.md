# Voxelwagen

A generic voxel engine with a unique automation game.

## Components

Voxelwagen will (also already partially can) run multiple different games -
including it's own automation game. Additionally clients for other voxel games
will be implemented.

- **Games**
  - **Voxelwagen Game** _work in progress_
  - **Minecraft Classic** _work in progress_ (see [classenwagen] repo)
  - **Minecraft** _work in progress_ (see [minewagen] repo)
  - **Minetest** _not started yet_
- **Engine** _work in progress_
- **Tools**
  - **Model editor** _work in progress_
  - **Music editor** _not started yet_

[classenwagen]: https://codeberg.org/voxelwagen/classenwagen
[minewagen]: https://codeberg.org/voxelwagen/minewagen

## License

GNU Affero General Public License: See [`/COPYING`](./COPYING)
