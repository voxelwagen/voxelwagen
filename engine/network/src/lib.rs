/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
pub mod crypto;
pub mod error;
pub mod packets;
pub mod peer;
pub mod transport;

use crate::peer::Peer;
use bincode::{Decode, Encode};
use error::NetworkError;
use log::{error, info, trace, warn};
use std::{
    collections::BTreeMap,
    fmt::Debug,
    net::{SocketAddr, UdpSocket},
    sync::{Arc, RwLock},
};

pub trait NetworkCallbacks<P>: Send + Sync + 'static {
    fn on_packet(&self, address: SocketAddr, packet: P);
    fn on_establish(&self, address: SocketAddr, active: bool);
}

pub struct NetworkState<P> {
    secret: Vec<u8>,
    sock: UdpSocket,
    peers: RwLock<BTreeMap<SocketAddr, Peer<P>>>,
    callbacks: Arc<dyn NetworkCallbacks<P>>,
}

pub trait Packet: Encode + Decode + Send + Clone + Sync + Debug + 'static {}
impl<T: Encode + Decode + Clone + Send + Sync + Debug + 'static> Packet for T {}

impl<P: Packet> NetworkState<P> {
    pub fn new(secret: Vec<u8>, callbacks: Arc<dyn NetworkCallbacks<P>>) -> Arc<Self> {
        let bind_addr = std::env::var("NETWORK_BIND").unwrap_or("0.0.0.0:0".to_string());
        let sock = UdpSocket::bind(bind_addr).expect("UDP socket bind failed");
        info!("socket bound to {}", sock.local_addr().unwrap());
        let st = Arc::new(Self {
            callbacks,
            secret,
            sock,
            peers: Default::default(),
        });
        let st2: Arc<NetworkState<P>> = st.clone();
        std::thread::Builder::new()
            .name("network receive".into())
            .spawn(move || st2.receive())
            .expect("cant spawn network thread");
        st
    }

    pub fn add(self: &Arc<Self>, address: SocketAddr) {
        info!("adding peer {address}");
        self.peers
            .write()
            .unwrap()
            .insert(address, Peer::new_active(address, self.clone()));
    }

    pub fn send(&self, address: SocketAddr, packet: P) {
        if let Some(g) = self.peers.write().unwrap().get_mut(&address) {
            g.send(packet)
        } else {
            warn!("discarded packet delivery to unknown peer {address:?}")
        }
    }
    pub fn broadcast(&self, packet: P) {
        self.peers
            .write()
            .unwrap()
            .iter_mut()
            .for_each(|(_, p)| p.send(packet.clone()))
    }

    fn receive(self: Arc<Self>) {
        let mut buf = [0u8; 8196];
        loop {
            match self.sock.recv_from(&mut buf) {
                Ok((size, addr)) => self.on_recv(addr, &mut buf[..size]),
                Err(e) => error!("recv failed: {e}"),
            }
        }
    }
    fn on_recv(self: &Arc<Self>, source: SocketAddr, packet: &mut [u8]) {
        trace!("recv from {source}: {packet:02x?}");
        let mut g = self.peers.write().unwrap();
        let p = g
            .entry(source)
            .or_insert_with(|| Peer::new_passive(source, self.clone()));
        match p.handle(packet) {
            Err(e) => warn!("error in packet handling: {e:?}"),
            Ok(Some(active)) => self.callbacks.on_establish(source, active),
            _ => (),
        }
    }
}
