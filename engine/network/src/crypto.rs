/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::packets::{ECDH_SIZE, HASH_SIZE};
use log::debug;
use rand::thread_rng;
use sha2::{Digest, Sha512};
use x25519_dalek::PublicKey;

pub struct Ecdh {
    pub private: x25519_dalek::EphemeralSecret,
}

impl Ecdh {
    pub fn generate() -> Self {
        debug!("generating x25519 dalek key");
        let private = x25519_dalek::EphemeralSecret::random_from_rng(thread_rng());
        Self { private }
    }
    pub fn public(&self) -> [u8; ECDH_SIZE] {
        debug!("deriving pk");
        PublicKey::from(&self.private).to_bytes()
    }
    pub fn dh(self, other_pk: [u8; ECDH_SIZE]) -> [u8; ECDH_SIZE] {
        debug!("compute dh");
        self.private
            .diffie_hellman(&PublicKey::from(other_pk))
            .to_bytes()
    }
}

pub fn hash_pk(pk: [u8; ECDH_SIZE], secret: &[u8]) -> [u8; HASH_SIZE] {
    let mut hasher: Sha512 = Sha512::new();
    hasher.update(pk);
    hasher.update(secret);
    let hash = hasher.finalize();
    hash[..HASH_SIZE].try_into().unwrap()
}
