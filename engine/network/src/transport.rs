use log::debug;

use crate::{packets::TransportLayerPacket, Packet};

pub(crate) struct Transport<P> {
    out: Vec<TransportLayerPacket<P>>,
    // resend_store: VecDeque<P>,
    // reorder_buffer: Vec<P>,
}

impl<P: Packet> Transport<P> {
    pub fn new() -> Self {
        Self {
            // resend_store: Default::default(),
            // reorder_buffer: Default::default(),
            out: Default::default(),
        }
    }

    pub(crate) fn flush(&mut self) -> Vec<TransportLayerPacket<P>> {
        std::mem::take(&mut self.out)
    }
    pub(crate) fn send(&mut self, p: P) {
        debug!("send {p:?}");
        self.out.push(TransportLayerPacket {
            seq_num: 0,
            ack_num: 0,
            payload: Some(p),
        });
    }
    pub(crate) fn recv(&mut self, p: TransportLayerPacket<P>, mut out: impl FnMut(P)) {
        if let Some(p) = p.payload {
            debug!("recv {p:?}");
            out(p)
        }
    }
}
