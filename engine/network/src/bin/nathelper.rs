/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use bincode::config::standard;
use std::net::UdpSocket;

fn main() {
    let addr = std::env::var("NETWORK_BIND").unwrap_or("0.0.0.0:12345".to_string());
    let sock = UdpSocket::bind(addr).unwrap();
    while let Ok((_, addr)) = sock.recv_from(&mut []) {
        let _ = sock.send_to(&bincode::encode_to_vec(addr, standard()).unwrap(), addr);
    }
}
