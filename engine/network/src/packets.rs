/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use bincode::{Decode, Encode};

pub const ECDH_SIZE: usize = 32;
pub const HASH_SIZE: usize = 32;

#[derive(Debug, Encode, Decode)]
pub(crate) struct Handshake {
    pub pk: [u8; ECDH_SIZE],
    pub pk_auth: [u8; HASH_SIZE],
}

#[derive(Debug, Encode, Decode)]
pub(crate) struct TransportLayerPacket<P> {
    pub seq_num: u64,
    pub ack_num: u64,
    pub payload: Option<P>,
}
