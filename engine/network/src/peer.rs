/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{
    crypto::{hash_pk, Ecdh},
    packets::{Handshake, TransportLayerPacket},
    transport::Transport,
    NetworkError, NetworkState, Packet,
};
use aes_gcm_siv::{AeadInPlace, Aes256GcmSiv, KeyInit};
use bincode::config::standard;
use log::{debug, info, warn, trace};
use rand::random;
use std::{net::SocketAddr, sync::Arc};

pub struct Peer<P> {
    address: SocketAddr,
    nst: Arc<NetworkState<P>>,
    transport: Transport<P>,
    state: PeerState,
}

pub(crate) enum PeerState {
    New { ecdh: Option<Ecdh>, active: bool },
    Established { key: Box<Aes256GcmSiv> },
}

impl<P: Packet> Peer<P> {
    pub fn new_active(address: SocketAddr, nst: Arc<NetworkState<P>>) -> Self {
        let ecdh = Ecdh::generate();
        let pk = ecdh.public();
        let pk_auth = hash_pk(pk, &nst.secret);
        let p = Self {
            address,
            nst,
            state: PeerState::New {
                active: true,
                ecdh: Some(ecdh),
            },
            transport: Transport::new(),
        };
        debug!("sending initial handshake to {address}");
        p.send_raw(&bincode::encode_to_vec(Handshake { pk, pk_auth }, standard()).unwrap());
        p
    }
    pub fn new_passive(address: SocketAddr, nst: Arc<NetworkState<P>>) -> Self {
        Self {
            transport: Transport::new(),
            address,
            nst,
            state: PeerState::New {
                active: false,
                ecdh: Some(Ecdh::generate()),
            },
        }
    }

    fn send_raw(&self, packet: &[u8]) {
        trace!("send to {}: {packet:02x?}", self.address);
        if let Err(e) = self.nst.sock.send_to(packet, self.address) {
            warn!("unable to send packet to {}: {e}", self.address)
        }
    }
    fn flush_transport(&mut self) {
        match &mut self.state {
            PeerState::Established { key } => {
                let key = key.clone(); // TODO maybe dont clone
                for packet in self.transport.flush() {
                    let mut packet = bincode::encode_to_vec(packet, standard()).unwrap();
                    let nonce = [(); 12].map(|_| random::<u8>());
                    let tag = key
                        .encrypt_in_place_detached((&nonce).into(), &[], &mut packet)
                        .expect("why should this fail?");
                    let mut p = Vec::new();
                    p.extend(nonce);
                    p.extend(tag);
                    p.extend(packet.iter());
                    self.send_raw(&p);
                }
            }
            _ => warn!("flush ignored, peer not ready."),
        }
    }
    pub fn send(&mut self, p: P) {
        self.transport.send(p);
        self.flush_transport(); // TODO temporary
    }

    /// Returns if the peer has just been established and if so, if it was the connecting peer.
    pub fn handle(&mut self, packet: &mut [u8]) -> Result<Option<bool>, NetworkError> {
        let mut next_state = None;
        let mut ret = None;
        match &mut self.state {
            PeerState::New {
                ecdh,
                active: first,
            } => {
                let first = *first;
                let ecdh = ecdh.take().unwrap();
                let (packet, _) = bincode::decode_from_slice::<Handshake, _>(packet, standard())?;
                if packet.pk_auth != hash_pk(packet.pk, &self.nst.secret) {
                    return Err(NetworkError::HandshakeInvalid);
                }
                if !first {
                    let pk = ecdh.public();
                    let pk_auth = hash_pk(pk, &self.nst.secret);
                    self.send_raw(
                        &bincode::encode_to_vec(Handshake { pk, pk_auth }, standard()).unwrap(),
                    );
                }
                let shared_secret = ecdh.dh(packet.pk);
                info!(
                    "handshake with {} complete, shared secret[..6]={:02x?}",
                    self.address,
                    &shared_secret[0..6]
                );
                ret = Some(first);
                next_state = Some(PeerState::Established {
                    key: Aes256GcmSiv::new_from_slice(&shared_secret).unwrap().into(),
                });
            }
            PeerState::Established { key } => {
                // TODO would be nice to use bincode for everything but we are missing decode_from_slice_mut(). :(
                if packet.len() < 12 + 16 {
                    return Err(NetworkError::PacketTruncated);
                }
                let (nonce, r) = packet.split_at_mut(12);
                let (tag, packet) = r.split_at_mut(16);
                key.decrypt_in_place_detached((&*nonce).into(), &[], packet, (&*tag).into())
                    .map_err(|_| NetworkError::DecryptFail)?;

                let (packet, _) = bincode::decode_from_slice::<TransportLayerPacket<P>, _>(
                    packet,
                    bincode::config::standard(),
                )?;
                self.transport
                    .recv(packet, |p| self.nst.callbacks.on_packet(self.address, p));
            }
        }
        if let Some(next_state) = next_state {
            self.state = next_state;
        }
        Ok(ret)
    }
}
