use thiserror::Error;

#[derive(Error, Debug)]
pub enum NetworkError {
    #[error("packet decode failed: {0}")]
    Decode(#[from] bincode::error::DecodeError),
    #[error("decryption failed. packet corrupt or malicious")]
    DecryptFail,
    #[error("packet is truncated")]
    PacketTruncated,
    #[error("handshake invalid")]
    HandshakeInvalid,
}
