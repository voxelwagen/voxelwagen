/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::synths::{
    envelope::{amplitude, Falloff},
    filters::{speed, vibrato},
    stuff::{const_harmonics, gain, harmonics},
    waveforms::{noise, sine, softsaw, square},
    Synth,
};
use std::f64::consts::E;

pub fn piano(freq: f64) -> impl Synth + 'static {
    amplitude(
        Falloff::Linear(3.),
        const_harmonics(
            &[(1.0, 1.0), (0.8, 2.0), (0.2, 4.0), (0.2, 8.0), (0.2, 16.0)],
            vibrato(gain(0.0005, sine(1.)), sine(freq)),
        ),
    )
}
pub fn guitar(freq: f64) -> impl Synth + 'static {
    gain(
        freq.sqrt() * 0.03,
        amplitude(
            Falloff::Linear(0.005 * freq),
            harmonics(3, 2.0, 0.5, vibrato(gain(0.0001, sine(10.)), softsaw(freq))),
        ),
    )
}
pub fn violin(freq: f64) -> impl Synth + 'static {
    amplitude(
        Falloff::Linear(0.001 * freq),
        const_harmonics(
            &[(1.0, 1.0), (0.8, 2.0), (0.2, 4.0), (0.2, 8.0), (0.2, 16.0)],
            vibrato(gain(0.0002, sine(2.)), square(freq)),
        ),
    )
}

pub enum Drum {
    Kick,
    Snare,
    Hihat,
    Clap,
}

impl Synth for Drum {
    fn sample(&mut self, t: f64) -> f64 {
        match self {
            Drum::Hihat => amplitude(Falloff::Linear(7.), noise).sample(t),
            Drum::Snare => todo!(),
            Drum::Clap => todo!(),
            Drum::Kick => gain(
                10.,
                amplitude(
                    Falloff::Linear(7.),
                    speed(|x: f64| (E.powf(-x * 1.) * 10. + 40.), sine(1.)),
                ),
            )
            .sample(t),
        }
    }
}
