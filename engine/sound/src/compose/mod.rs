/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use std::ops::{Add, Range};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Pitch(pub i32);

pub mod pitchclass {
    use super::Pitch;
    pub const C: Pitch = Pitch(0);
    pub const CS: Pitch = Pitch(1);
    pub const D: Pitch = Pitch(2);
    pub const DS: Pitch = Pitch(3);
    pub const E: Pitch = Pitch(4);
    pub const F: Pitch = Pitch(5);
    pub const FS: Pitch = Pitch(6);
    pub const G: Pitch = Pitch(7);
    pub const GS: Pitch = Pitch(8);
    pub const A: Pitch = Pitch(9);
    pub const AS: Pitch = Pitch(10);
    pub const B: Pitch = Pitch(11);
}
pub mod lengths {
    pub const WN: f64 = 1.;
    pub const HN: f64 = 0.5;
    pub const QN: f64 = 0.25;
    pub const EN: f64 = 0.125;
    pub const SN: f64 = 0.0625;
}

impl Pitch {
    pub fn frequency(&self) -> f64 {
        2f64.powf((self.0 - 48 + 3) as f64 / 12.) * 440.
    }
}

pub fn sequence(keep: f64, s: &[(f64, Pitch)]) -> Vec<(Range<f64>, Pitch)> {
    let mut c = 0.;
    let mut o = Vec::new();
    for (len, p) in s {
        o.push((c..c + *len + keep, *p));
        c += *len;
    }
    o
}

impl Add<i32> for Pitch {
    type Output = Pitch;
    fn add(self, rhs: i32) -> Self::Output {
        Pitch(self.0 + rhs)
    }
}

pub fn merge_events(
    ks: Vec<impl Iterator<Item = (Range<f64>, Pitch)> + Clone>,
) -> Vec<(Range<f64>, Pitch)> {
    let mut o = Vec::new();
    for k in ks {
        o.extend(k.clone())
    }
    o.sort_by_key(|(r, _)| (r.start * 10000.) as i64);
    o
}

pub fn load_pianoletternotes(s: &str) -> Vec<(Range<f64>, Pitch)> {
    s.split("\n\n")
        .enumerate()
        .flat_map(|(si, section)| {
            merge_events(
                section
                    .split('\n')
                    .filter(|e| !e.is_empty())
                    .map(|track| {
                        let (track_meta, track_inner) = track.split_once('|').unwrap();
                        let track_inner = track_inner.split_once('|').unwrap().0;
                        let track_octave = track_meta
                            .split_once(':')
                            .unwrap_or(("", track_meta))
                            .1
                            .parse::<i32>()
                            .unwrap();
                        track_inner.chars().enumerate().filter_map(move |(i, c)| {
                            Pitch::from_letter(c).map(|e| {
                                let t = (i + si * 26) as f64;
                                (t..t + 5., e + track_octave * 12)
                            })
                        })
                    })
                    .collect::<Vec<_>>(),
            )
        })
        .collect()
}

impl Pitch {
    pub fn from_letter(c: char) -> Option<Self> {
        use pitchclass::*;
        match c {
            'a' => Some(A),
            'A' => Some(AS),
            'b' => Some(B),
            'c' => Some(C),
            'C' => Some(CS),
            'd' => Some(D),
            'D' => Some(DS),
            'e' => Some(E),
            'f' => Some(F),
            'F' => Some(FS),
            'g' => Some(G),
            'G' => Some(GS),
            '-' => None,
            _ => panic!("no"),
        }
    }
}
