/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(exclusive_range_pattern)]
pub mod compose;
pub mod instruments;
pub mod sequencer;
pub mod synths;

use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    Stream,
};
use log::{debug, error};
use std::{
    io::{stdout, Write},
    rc::Rc,
    sync::{Arc, RwLock},
    time::{Duration, Instant},
};
use synths::{DynSynth, Synth};

pub struct Sound {
    pub synth: Arc<RwLock<Box<dyn Synth + Send + Sync + 'static>>>,
    pub time: Arc<RwLock<f64>>,
    pub stream: Rc<Stream>,
}

impl Sound {
    pub fn new(synth: Box<dyn Synth + Send + Sync + 'static>) -> Self {
        let host = cpal::default_host();
        let outdev = host.default_output_device().unwrap();

        let outconf = outdev.default_output_config().unwrap();
        let time = Arc::new(RwLock::new(0.));
        let synth = Arc::new(RwLock::new(synth));
        let stream = {
            let synth = synth.clone();
            let time = time.clone();
            let mut local_time = 0.;
            let stream = outdev
                .build_output_stream(
                    &outconf.config(),
                    move |buffer: &mut [f32], _cbinfo| {
                        let mut sy = synth.write().unwrap();
                        let dt = 1. / outconf.sample_rate().0 as f64;
                        let ts = Instant::now();
                        let n_samples = buffer.len();
                        for e in buffer.chunks_exact_mut(outconf.channels() as usize) {
                            let sample = sy.sample(local_time) as f32;
                            for e in e {
                                *e = sample;
                            }
                            local_time += dt;
                        }
                        *time.write().unwrap() = local_time;
                        debug!(
                            "took {:?} to generate {:?} of audio",
                            ts.elapsed(),
                            Duration::from_secs_f64(dt * n_samples as f64)
                        )
                    },
                    |err| error!("audio output error: {err:?}"),
                    None,
                )
                .unwrap();

            stream.play().unwrap();
            Rc::new(stream)
        };

        Self {
            synth,
            stream,
            time,
        }
    }

    pub fn time(&self) -> f64 {
        *self.time.read().unwrap()
    }
}

pub fn export_synth(synth: &mut DynSynth, duration: f64) {
    let dt = 1. / 48000.;

    let mut buffer = Vec::new();
    let mut t = 0.;
    let mut ts = Instant::now();
    while t < duration {
        buffer.push(synth.sample(t));
        t += dt;
        if buffer.len() > 48000 {
            debug!(
                "({:.0} / {:.0}) took {:?} to generate 1s of audio",
                t,
                duration,
                ts.elapsed(),
            );
            stdout().write_all(bytemuck::cast_slice(&buffer)).unwrap();
            ts = Instant::now();
            buffer.clear();
        }
    }
}
