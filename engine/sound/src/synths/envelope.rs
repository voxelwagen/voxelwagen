/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::Synth;

#[inline]
pub fn amplitude<G: Synth, T: Synth>(mut gain: G, mut inner: T) -> impl Synth {
    move |t: f64| 2f64.powf(gain.sample(t)) * inner.sample(t)
}

pub enum Falloff {
    Exponential,
    Linear(f64),
}
impl Synth for Falloff {
    #[inline]
    fn sample(&mut self, t: f64) -> f64 {
        match self {
            Falloff::Exponential => 0.1f64.powf(t),
            Falloff::Linear(speed) => (t * -*speed).min(1.),
        }
    }
}

pub struct ADSR {
    pub attack: f64,
    pub decay: f64,
    pub sustain: f64,
    pub release: f64,
    pub length: f64,
}
impl Synth for ADSR {
    fn sample(&mut self, t: f64) -> f64 {
        if t < self.attack {
            t * (1. / self.attack)
        } else if t < self.decay {
            (t - self.attack) * ((1. - self.sustain) / -self.decay)
        } else if t < self.length {
            self.sustain
        } else {
            (t - self.length) * (self.sustain / -self.release)
        }
    }
}
