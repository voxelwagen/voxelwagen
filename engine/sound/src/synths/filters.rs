/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::Synth;
use std::f64::consts::PI;

pub fn vibrato(mut off: impl Synth, mut inner: impl Synth) -> impl Synth {
    move |x: f64| inner.sample(x + off.sample(x))
}
pub fn speed(mut fac: impl Synth, mut inner: impl Synth) -> impl Synth {
    move |x: f64| inner.sample(x * fac.sample(x))
}
pub fn tremolo(mut a: impl Synth, mut b: impl Synth) -> impl Synth {
    move |x: f64| a.sample(x) * b.sample(x)
}

pub fn lowpass(mut cutoff: impl Synth, mut inner: impl Synth) -> impl Synth {
    let mut xp = 0.;
    let mut yp = 0.;
    move |x: f64| {
        let dx = x - xp;
        let beta = 2. * PI * cutoff.sample(x) * dx;
        let alpha = beta / (beta + 1.);

        let yn = inner.sample(x);
        let y = alpha * yn + (1. - alpha) * yp;
        yp = y;
        xp = x;
        y
    }
}

pub fn highpass(mut cutoff: impl Synth, mut inner: impl Synth) -> impl Synth {
    let mut xp = 0.;
    let mut yp = 0.;
    let mut zp = 0.;
    move |x: f64| {
        let dx = x - xp;
        let beta = 2. * PI * cutoff.sample(x) * dx;
        let alpha = beta / (beta + 1.);

        let yn = inner.sample(x);
        let y = alpha * yp + (1. - alpha) * (yn - zp);
        zp = yn;
        yp = y;
        xp = x;
        y
    }
}
