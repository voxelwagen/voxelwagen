/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::Synth;
use std::f64::consts::PI;

#[inline]
pub fn sine(freq: f64) -> impl Synth {
    move |t: f64| (t * freq * PI * 2.).sin()
}
#[inline]
pub fn square(freq: f64) -> impl Synth {
    move |t: f64| if (t * freq).fract() > 0.5 { 1. } else { 0. }
}
#[inline]
pub fn saw(freq: f64) -> impl Synth {
    move |t: f64| ((t * freq * 2.) % 2.) - 1.
}
#[inline]
pub fn softsaw(freq: f64) -> impl Synth {
    move |t: f64| (((t * freq * 2.) % 2.) - 1.) * 0.3 + (t * freq * PI * 2.).cos() * 0.7
}
#[inline]
pub fn smoothsquare(freq: f64, exp: f64) -> impl Synth {
    move |t: f64| (t * freq * PI * 2.).sin().powf(exp)
}

pub fn noise(_t: f64) -> f64 {
    // pub fn xorshit(mut x: u32) -> u32 {
    //     x ^= x << 13;
    //     x ^= x >> 17;
    //     x ^= x << 5;
    //     x
    // }
    // xorshit((t * 1000000.) as u32) as f64 / u32::MAX as f64 * 2. - 1.
    rand::random::<f64>() * 2. - 1.
}
