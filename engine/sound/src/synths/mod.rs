/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
pub mod envelope;
pub mod filters;
pub mod stuff;
pub mod waveforms;

use crate::compose::Pitch;

pub type DynSynth = Box<dyn Synth>;
pub trait Synth: Send + Sync + 'static {
    fn sample(&mut self, t: f64) -> f64;
}

impl<T: FnMut(f64) -> f64 + Send + Sync + 'static> Synth for T {
    fn sample(&mut self, t: f64) -> f64 {
        self(t)
    }
}
impl Synth for f64 {
    fn sample(&mut self, _t: f64) -> f64 {
        *self
    }
}

pub type DynInstrument = Box<dyn Instrument>;
pub trait Instrument: Send + Sync + 'static {
    fn note(&self, pitch: Pitch) -> DynSynth;
}
impl<T: Fn(Pitch) -> DynSynth + Send + Sync + 'static> Instrument for T {
    fn note(&self, t: Pitch) -> DynSynth {
        self(t)
    }
}
