/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::{DynSynth, Synth};

pub fn harmonics<T: Synth>(
    count: usize,
    gain_factor: f64,
    freq_factor: f64,
    mut inner: T,
) -> impl Synth {
    move |t: f64| {
        (0..count)
            .map(|o| inner.sample(t * freq_factor.powi(o as i32)) * gain_factor.powi(o as i32))
            .sum()
    }
}

pub fn const_harmonics<T: Synth>(harmonics: &'static [(f64, f64)], mut inner: T) -> impl Synth {
    move |t: f64| {
        harmonics
            .iter()
            .map(|(gain, factor)| inner.sample(t * factor) * gain)
            .sum()
    }
}

#[inline]
pub fn repeat<T: Synth>(modulus: f64, mut inner: T) -> impl Synth {
    move |t: f64| inner.sample(t % modulus)
}

#[inline]
pub fn gain<T: Synth>(f: f64, mut inner: T) -> impl Synth {
    move |t: f64| inner.sample(t) * f
}

pub fn sequence(length: f64, mut ks: Vec<DynSynth>) -> impl Synth {
    move |t: f64| {
        ks.iter_mut()
            .enumerate()
            .map(|(i, s)| {
                let t = (t - (i as f64 / length)).rem_euclid(length);
                s.sample(t)
            })
            .sum()
    }
}
