/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::synths::{DynSynth, Synth};
use std::{
    collections::BTreeMap,
    ops::Range,
    sync::{Arc, RwLock},
};

pub struct Sequencer {
    pub queue: Arc<RwLock<BTreeMap<Span, DynSynth>>>,
}
pub struct Span(pub Range<f64>);

impl Synth for Sequencer {
    fn sample(&mut self, t: f64) -> f64 {
        let mut g = self.queue.write().unwrap();

        // remove old
        while g
            .first_key_value()
            .map(|(Span(span), _)| span.end < t)
            .unwrap_or(false)
        {
            g.pop_first();
        }

        let mut out = 0.;
        for (span, synth) in g.iter_mut() {
            if span.0.start > t {
                break;
            }
            out += synth.sample(t - span.0.start)
        }
        out
    }
}

impl Sequencer {
    pub fn new() -> (Self, Self) {
        let queue = Arc::new(RwLock::new(BTreeMap::new()));
        (
            Self {
                queue: queue.clone(),
            },
            Self { queue },
        )
    }
    pub fn extend(&self, events: impl Iterator<Item = (Span, DynSynth)>) {
        self.queue.write().unwrap().extend(events)
    }
}

impl PartialEq for Span {
    fn eq(&self, _other: &Self) -> bool {
        false
    }
}
impl Eq for Span {
    fn assert_receiver_is_total_eq(&self) {}
}
impl PartialOrd for Span {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for Span {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.0.start.partial_cmp(&other.0.start) {
            Some(std::cmp::Ordering::Equal) => match self.0.end.partial_cmp(&other.0.end) {
                Some(std::cmp::Ordering::Equal) => std::cmp::Ordering::Less,
                x => x.unwrap(),
            },
            x => x.unwrap(),
        }
    }
}
