//    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
//    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
//    Copyright (C) 2024 metamuffin <metamuffin.org>


@group(0) @binding(0) var atlas: texture_2d<f32>;
var<push_constant> view_projection: mat4x4<f32>;

struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) uv: vec2<f32>,
    @location(2) atlas: vec2<f32>,
    @location(3) ao: f32,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(1) uv: vec2<f32>,
    @location(2) atlas: vec2<f32>,
    @location(3) ao: f32,
}

@vertex
fn vertex_stage(model: VertexInput) -> VertexOutput {
    var out: VertexOutput;
    out.uv = model.uv;
    out.atlas = model.atlas;
    out.ao = model.ao;
    out.clip_position = view_projection * vec4<f32>(model.position, 1.0);
    return out;
}

@fragment
fn fragment_stage(in: VertexOutput) -> @location(0) vec4<f32> {
    var texcolor = textureLoad(atlas, vec2<i32>(floor(in.atlas)), 0);
    var color = texcolor.rgb * in.ao;
    return vec4<f32>(color, texcolor.a);
}
