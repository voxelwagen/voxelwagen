/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::bufferpool::{BufferKind, BufferPool, ReusableBuffer};
use crate::repr::Vertex;
use wgpu::{IndexFormat, RenderPass};

pub struct MeshBundle {
    pub solid: Mesh,
    pub translucent: Mesh,
}

pub struct Mesh {
    pub vertex_count: usize,
    pub index_count: usize,
    pub buffers: Option<(ReusableBuffer, ReusableBuffer)>,
}
impl Mesh {
    pub fn empty() -> Self {
        Self {
            buffers: None,
            index_count: 0,
            vertex_count: 0,
        }
    }
    pub fn new(bpool: &BufferPool, vertex: Vec<Vertex>, index: Vec<u32>) -> Self {
        if vertex.is_empty() && index.is_empty() {
            Self {
                vertex_count: 0,
                index_count: 0,
                buffers: None,
            }
        } else {
            Self {
                vertex_count: vertex.len(),
                index_count: index.len(),
                buffers: Some((
                    // TODO dont allocate when we should be able to cast.
                    bpool.request_init(bytemuck::cast_slice(&vertex).to_vec(), BufferKind::Vertex),
                    bpool.request_init(bytemuck::cast_slice(&index).to_vec(), BufferKind::Index),
                )),
            }
        }
    }
    pub fn recycle(mut self) {
        if let Some((a, b)) = self.buffers.take() {
            a.recycle();
            b.recycle();
        }
    }
    pub fn visible(&self) -> bool {
        self.buffers.is_some()
    }
    pub fn draw<'b>(&'b self, rpass: &mut RenderPass<'b>) {
        if let Some(b) = &self.buffers {
            rpass.set_vertex_buffer(0, b.0.buffer.slice(..));
            rpass.set_index_buffer(b.1.buffer.slice(..), IndexFormat::Uint32);
            rpass.draw_indexed(0..self.index_count as u32, 0, 0..1);
        }
    }
}
