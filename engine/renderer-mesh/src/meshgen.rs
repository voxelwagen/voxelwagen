/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::{
    bufferpool::BufferPool,
    mesh::{Mesh, MeshBundle},
    repr::Vertex,
};
use crate::atlas::{Atlas, TAtlas};
use std::{ops::Range, sync::RwLock};
use voxelwagen_common::{
    face::{Axis, BlockFace},
    range::ForEachExt,
    vector::{Vec2, Vec3},
    BPos, EPos, Material,
};
use voxelwagen_world::preload::PreloadedWorld;

pub fn build_mesh(
    bpool: &BufferPool,
    atlas: &RwLock<Atlas>,
    range: Range<BPos>,
    step: usize,
    world: &PreloadedWorld<Material>,
) -> MeshBundle {
    let (builder_solid, builder_trans) = generate_mesh(atlas, range, step, world);
    MeshBundle {
        translucent: Mesh::new(bpool, builder_trans.vertex, builder_trans.index),
        solid: Mesh::new(bpool, builder_solid.vertex, builder_solid.index),
    }
}

pub fn generate_mesh(
    mut atlas: impl TAtlas,
    range: Range<BPos>,
    step: usize,
    world: &PreloadedWorld<Material>,
) -> (MeshBuilder, MeshBuilder) {
    let mut builder_solid = MeshBuilder::default();
    let mut builder_trans = MeshBuilder::default();
    range.clone().for_each_step(step, |p| {
        let b = world[p];
        if b.is_invisible() {
            return;
        }
        let mesh = if b.is_opaque() {
            &mut builder_solid
        } else {
            &mut builder_trans
        };
        for face in BlockFace::LIST {
            let adj = world[p + face.dir() * step as i64];
            let mut skip = false;
            skip |= adj.is_opaque();
            skip |= b.fluid() && adj.fluid();
            if skip {
                continue;
            }
            let aob = aoblocks(face).map(|e| world[e * step as i64 + p].is_opaque());
            let ao = [
                aovalue(aob[0], aob[1], aob[2]),
                aovalue(aob[2], aob[3], aob[4]),
                aovalue(aob[6], aob[7], aob[0]),
                aovalue(aob[4], aob[5], aob[6]),
            ];
            let tc = atlas.add_texture(Vec2::splat(1), vec![b]);
            mesh.face(
                p.epos(),
                face,
                &[
                    tc.clone(),
                    tc.clone(),
                    tc.clone(),
                    tc.clone(),
                    tc.clone(),
                    tc.clone(),
                ],
                ao,
                step as f32,
            )
        }
    });
    (builder_solid, builder_trans)
}

#[derive(Default)]
pub struct MeshBuilder {
    pub vertex: Vec<Vertex>,
    pub index: Vec<u32>,
}
impl MeshBuilder {
    pub fn face(
        &mut self,
        origin: EPos,
        or: BlockFace,
        texcoord: &[Range<Vec2<usize>>; 6],
        ao: [f32; 4],
        size: f32,
    ) {
        let ib = self.vertex.len() as u32;
        let (vertex, index) = face(or, ao, texcoord);
        self.vertex.extend(vertex.map(|v| Vertex {
            position: (Vec3::from(v.position) * size + origin).array(),
            uv: [v.uv[0] * size, v.uv[1] * size],
            ..v
        }));
        self.index.extend(index.map(|i| ib + i))
    }
}

fn face(
    face: BlockFace,
    ao: [f32; 4],
    texcoord: &[Range<Vec2<usize>>; 6],
) -> ([Vertex; 4], [u32; 6]) {
    let isotropy_swap = (ao[1] + ao[2]) > (ao[0] + ao[3]);
    let index = #[rustfmt::skip] match (face.sign, isotropy_swap) {
        (false, false) => [0, 3, 1, 0, 2, 3],
        (true, false) =>  [0, 1, 3, 0, 3, 2],
        (false, true) =>  [2, 1, 0, 2, 3, 1],
        (true, true) =>   [2, 0, 1, 2, 1, 3],
    };
    let vertex_offset = match face.sign {
        false => EPos::ZERO,
        true => face.axis.dir().epos(),
    };
    let vertex_geom = #[rustfmt::skip] match face.axis {
        Axis::X => [
            (0, EPos { x: 0.0, y: 0.0, z: 0.0 }, [0, 0]),
            (1, EPos { x: 0.0, y: 1.0, z: 0.0 }, [1, 0]),
            (2, EPos { x: 0.0, y: 0.0, z: 1.0 }, [0, 1]),
            (3, EPos { x: 0.0, y: 1.0, z: 1.0 }, [1, 1]),
        ],
        Axis::Y => [
            (0, EPos { x: 0.0, y: 0.0, z: 0.0 }, [0, 0]),
            (2, EPos { x: 0.0, y: 0.0, z: 1.0 }, [1, 0]),
            (1, EPos { x: 1.0, y: 0.0, z: 0.0 }, [0, 1]),
            (3, EPos { x: 1.0, y: 0.0, z: 1.0 }, [1, 1]),
        ],
        Axis::Z => [
            (0, EPos { x: 0.0, y: 0.0, z: 0.0 }, [0, 0]),
            (1, EPos { x: 1.0, y: 0.0, z: 0.0 }, [1, 0]),
            (2, EPos { x: 0.0, y: 1.0, z: 0.0 }, [0, 1]),
            (3, EPos { x: 1.0, y: 1.0, z: 0.0 }, [1, 1]),
        ]
    };
    let texcoord = texcoord[face.index()].clone();
    let vertex = vertex_geom.map(|(i, p, uv)| Vertex {
        uv: [uv[0] as f32, uv[1] as f32],
        position: (p + vertex_offset).array(),
        atlas: [
            [texcoord.start, texcoord.end][uv[0]].x as f32,
            [texcoord.start, texcoord.end][uv[1]].y as f32,
        ],
        ao: ao[i],
    });

    (vertex, index)
}

pub fn aovalue(s1: bool, c: bool, s2: bool) -> f32 {
    1. - match (s1, c, s2) {
        (true, _, true) => 0.5,                             // both sides
        (true, true, false) | (false, true, true) => 0.3,   // corner + side
        (true, false, false) | (false, false, true) => 0.3, // one side
        (false, true, false) => 0.3,                        // corner
        (false, false, false) => 0.0,                       // none
    }
}

pub fn aoblocks(f: BlockFace) -> [BPos; 8] {
    let off = f.sign();
    #[rustfmt::skip] match f.axis {
        Axis::X => [
            BPos { x: off, y: -1, z: 0 },
            BPos { x: off, y: -1, z: -1 },
            BPos { x: off, y: 0, z: -1 },
            BPos { x: off, y: 1, z: -1 },
            BPos { x: off, y: 1, z: 0 },
            BPos { x: off, y: 1, z: 1 },
            BPos { x: off, y: 0, z: 1 },
            BPos { x: off, y: -1, z: 1 },
        ],
        Axis::Y => [
            BPos { x: -1, y: off, z: 0 },
            BPos { x: -1, y: off, z: -1 },
            BPos { x: 0, y: off, z: -1 },
            BPos { x: 1, y: off, z: -1 },
            BPos { x: 1, y: off, z: 0 },
            BPos { x: 1, y: off, z: 1 },
            BPos { x: 0, y: off, z: 1 },
            BPos { x: -1, y: off, z: 1 },
        ],
        Axis::Z => [
            BPos { x: -1, y: 0, z: off },
            BPos { x: -1, y: -1, z: off },
            BPos { x: 0, y: -1, z: off },
            BPos { x: 1, y: -1, z: off },
            BPos { x: 1, y: 0, z: off },
            BPos { x: 1, y: 1, z: off },
            BPos { x: 0, y: 1, z: off },
            BPos { x: -1, y: 1, z: off },
        ],
    }
}
