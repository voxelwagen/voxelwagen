/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use std::ops::Range;
use voxelwagen_common::{matrix::Mat4, range::RangeExt, vector::Vec4, EMat, EPos, EVec};

#[derive(Debug, Clone)]
pub struct Camera {
    pub position: EPos,
    pub rotation: EMat,

    pub aspect: f32,
    pub fov: f32,
    pub near: f32,
    pub far: f32,
}

impl Camera {
    pub fn view_projection_matrix(&self) -> Mat4<f32> {
        perspective(self.aspect, self.fov, self.near, self.far)
            * Mat4::from_transform_offset(EVec::ZERO, self.rotation)
            * Mat4::from_transform_offset(self.position * -1., EMat::UNIT)
    }

    //? verify if this actually works
    pub fn is_visible(&self, r: Range<EPos>) -> bool {
        let vp = self.view_projection_matrix();
        let screen_corners: [EPos; 8] = r.corners().map(|c| project(&vp, c));

        let min = screen_corners.into_iter().reduce(|a, b| a.min(b)).unwrap();
        let max = screen_corners.into_iter().reduce(|a, b| a.max(b)).unwrap();
        let screen_bounds = EPos {
            x: -1.,
            y: -1.,
            z: 0.,
        }..EPos {
            x: 1.,
            y: 1.,
            z: f32::INFINITY,
        };
        screen_bounds.overlaps(&(min..max))
    }
}

pub fn project(vp: &Mat4<f32>, c: EPos) -> EPos {
    let k = *vp
        * Vec4 {
            x: c.x,
            y: c.y,
            z: c.z,
            w: 1.,
        };
    EPos {
        x: k.x / k.w,
        y: k.y / k.w,
        z: k.z / k.w,
    }
}

fn perspective(aspect: f32, fovy: f32, near: f32, far: f32) -> Mat4<f32> {
    let top = (fovy / 2.).tan() * near;
    let bottom = -top;
    let right = top * aspect;
    let left = -right;
    Mat4 {
        x: Vec4 {
            x: (2. * near) / (right - left),
            y: 0.,
            z: (right + left) / (right - left),
            w: 0.,
        },
        y: Vec4 {
            x: 0.,
            y: (2. * near) / (top - bottom),
            z: (top + bottom) / (top - bottom),
            w: 0.,
        },
        z: Vec4 {
            x: 0.,
            y: 0.,
            z: -(far + near) / (far - near),
            w: -(2. * far * near) / (far - near),
        },
        w: Vec4 {
            x: 0.,
            y: 0.,
            z: -1.,
            w: 0.,
        },
    }
}
