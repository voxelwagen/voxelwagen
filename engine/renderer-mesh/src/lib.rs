/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(stmt_expr_attributes)]
pub(crate) mod atlas;
pub(crate) mod bufferpool;
pub(crate) mod camera;
pub(crate) mod chunk;
pub(crate) mod loader;
pub(crate) mod mesh;
pub(crate) mod meshgen;
pub(crate) mod process;
pub(crate) mod repr;
pub(crate) mod sprite;
pub(crate) mod texture;
pub(crate) mod visual;

pub use crate::atlas::TAtlas;
pub use crate::meshgen::generate_mesh;

use crate::{atlas::Atlas, bufferpool::BufferPool, chunk::Chunk, sprite::Sprite};
use crossbeam_channel::Sender;
use loader::{ChunkSpec, Loader, LoaderEvent};
use log::info;
use std::{
    collections::HashMap,
    fmt::Write,
    ops::Range,
    sync::{Arc, RwLock},
};
use visual::Visual;
use voxelwagen_common::{compat::SendAnyway, BPos, EPos};
use voxelwagen_renderer_common::{Renderer, RendererCallbacks, RendererNew};
use wgpu::{CommandEncoder, Device, Queue, SurfaceConfiguration, TextureView};

pub struct MeshViewer {
    pub visual: SendAnyway<Visual>,
    pub loader_events_send: Sender<LoaderEvent>,
}

#[derive(Clone)]
pub struct SharedState {
    pub atlas: Arc<RwLock<Atlas>>,
    pub sprites: Arc<RwLock<HashMap<u64, Sprite>>>,
    pub camera: Arc<RwLock<EPos>>,
    pub chunk_meshes: Arc<RwLock<HashMap<ChunkSpec, Chunk>>>,
    pub callbacks: Arc<dyn RendererCallbacks>,
    pub buffer_pool: Arc<BufferPool>,
}

impl RendererNew for MeshViewer {
    fn new(
        callbacks: Arc<dyn RendererCallbacks>,
        device: &Arc<Device>,
        queue: &Arc<Queue>,
        surface_config: &SurfaceConfiguration,
    ) -> Self {
        let (atlas, atlas_texture) = Atlas::new(device, 8192);

        let (loader_events_send, loader_events_receive) = crossbeam_channel::unbounded();
        let (buffer_pool, uploads) = BufferPool::new(device.to_owned());

        let shared = SharedState {
            atlas: Arc::new(RwLock::new(atlas)),
            chunk_meshes: Default::default(),
            sprites: Default::default(),
            callbacks,
            buffer_pool,
            camera: Default::default(),
        };

        let visual = Visual::new(
            shared.clone(),
            device,
            queue,
            surface_config,
            uploads,
            atlas_texture,
        );

        let state = Self {
            visual: SendAnyway(visual),
            loader_events_send,
        };

        let mut loader = Loader::new(shared, loader_events_receive);

        std::thread::Builder::new()
            .name("mesh load sched".to_string())
            .spawn(move || loader.chunk_loop())
            .unwrap();

        state
    }
}
impl Renderer for MeshViewer {
    fn invalidate_blocks(&mut self, range: Range<BPos>) {
        info!("invalidate {range:?}");
        self.loader_events_send
            .send(LoaderEvent::DamageBlocks(range))
            .unwrap();
    }

    fn invalidate_sprites(&mut self, _ents: &[u64]) {
        // let mut g = self.sprites.write().unwrap();
        // for id in ents {
        //     if let Some((block, offset, transform)) = self.callbacks.get_sprite(*id) {
        //         g.insert(
        //             *id,
        //             Sprite {
        //                 mesh: self.block_model_mesh(block),
        //                 current_model: block,
        //                 projection: Mat4::from_transform_offset(offset, transform),
        //             },
        //         );
        //     } else {
        //         g.remove(id);
        //     }
        // }
    }

    fn reconfigure(&mut self, surface_config: &SurfaceConfiguration) {
        self.visual.reconfigure(surface_config)
    }

    fn render(&mut self, encoder: &mut CommandEncoder, view: &TextureView) {
        self.visual.render(encoder, view)
    }

    fn pre_submit(&mut self) {
        self.visual.pre_submit()
    }
    fn post_submit(&mut self) {
        self.visual.post_submit()
    }
    fn stats(&mut self, out: &mut String) {
        writeln!(out, "renderer: mesh").unwrap();
        self.visual.0.shared.buffer_pool.stats(out);
    }
}
