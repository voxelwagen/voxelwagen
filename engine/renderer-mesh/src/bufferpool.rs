/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crossbeam_channel::{Receiver, Sender};
use log::debug;
use std::{
    fmt::Write,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc, RwLock,
    },
};
use wgpu::{Buffer, BufferDescriptor, BufferUsages, Device};

pub type Upload = (Arc<Buffer>, Vec<u8>);

pub struct BufferPool {
    device: Arc<Device>,
    upload: Sender<Upload>,
    recycle_recv: Receiver<ReusableBuffer>,
    recycle_sender: Sender<ReusableBuffer>,
    unused: RwLock<Vec<ReusableBuffer>>,
    allocated_size: AtomicUsize,
    allocated_buffers: AtomicUsize,
}

impl BufferPool {
    pub fn new(device: Arc<Device>) -> (Arc<Self>, Receiver<Upload>) {
        let (recycle_sender, recycle_recv) = crossbeam_channel::unbounded();
        let (upload, upload_recv) = crossbeam_channel::unbounded();
        (
            BufferPool {
                upload,
                unused: Default::default(),
                recycle_recv,
                recycle_sender,
                device,
                allocated_size: AtomicUsize::new(0),
                allocated_buffers: AtomicUsize::new(0),
            }
            .into(),
            upload_recv,
        )
    }
    pub fn request(&self, size: usize, kind: BufferKind) -> ReusableBuffer {
        let mut unused = self.unused.write().unwrap();
        for buf in self.recycle_recv.try_iter() {
            debug!("recycled old buffer ({})", buf.size);
            unused.push(buf);
        }
        let reuse_index = unused
            .iter()
            .enumerate()
            .filter(|(_, b)| b.kind == kind && b.size >= size)
            .min_by_key(|(_, b)| b.size)
            .map(|(i, _)| i);
        if let Some(ri) = reuse_index {
            let b = unused.swap_remove(ri);
            drop(unused);
            debug!("reusing buffer ({} >= {size})", b.size);
            b
        } else {
            drop(unused);
            let b = ReusableBuffer::new(
                &self.device,
                self.recycle_sender.clone(),
                size + size / 2, // make it 50% bigger to support resize
                kind,
            );
            self.allocated_size.fetch_add(b.size, Ordering::Relaxed);
            self.allocated_buffers.fetch_add(1, Ordering::Relaxed);
            debug!("allocating new buffer ({} >= {size})", b.size);
            b
        }
    }
    pub fn request_init(&self, data: Vec<u8>, kind: BufferKind) -> ReusableBuffer {
        let buffer = self.request(data.len(), kind);
        self.upload.send((buffer.buffer.clone(), data)).unwrap();
        buffer
    }

    pub fn stats(&self, out: &mut String) {
        let unused = self.unused.read().unwrap();
        let unused_size = unused.iter().map(|b| b.size).sum::<usize>();
        let all_size = self.allocated_size.load(Ordering::Relaxed);
        let all_count = self.allocated_buffers.load(Ordering::Relaxed);
        writeln!(
            out,
            "buffers free: {} ({} MiB)\nbuffers used: {} ({} MiB)\nbuffers all: {} ({} MiB)",
            unused.len(),
            unused_size / (1024 * 1024),
            all_count - unused.len(),
            (all_size - unused_size) / (1024 * 1024),
            all_count,
            all_size / (1024 * 1024)
        )
        .unwrap();
    }
}

impl ReusableBuffer {
    pub fn new(
        device: &Device,
        recycle: Sender<ReusableBuffer>,
        size: usize,
        kind: BufferKind,
    ) -> Self {
        Self {
            recycle,
            kind,
            size,
            buffer: device
                .create_buffer(&BufferDescriptor {
                    label: Some("managed vert/idx"),
                    size: size.try_into().unwrap(),
                    usage: match kind {
                        BufferKind::Index => BufferUsages::INDEX,
                        BufferKind::Vertex => BufferUsages::VERTEX,
                    } | BufferUsages::COPY_DST,
                    mapped_at_creation: false,
                })
                .into(),
        }
    }
    pub fn recycle(self) {
        self.recycle.clone().send(self).unwrap()
    }
}

impl Drop for ReusableBuffer {
    fn drop(&mut self) {
        ReusableBuffer {
            buffer: self.buffer.clone(),
            kind: self.kind,
            recycle: self.recycle.clone(),
            size: self.size,
        }
        .recycle()
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BufferKind {
    Vertex,
    Index,
}

pub struct ReusableBuffer {
    recycle: Sender<ReusableBuffer>,
    kind: BufferKind,
    size: usize,
    pub buffer: Arc<Buffer>,
}
