/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use super::{
    repr::PostProcessUniforms,
    texture::{ColorTexture, DepthTexture},
};
use wgpu::{
    BindGroup, BindGroupEntry, BindGroupLayoutEntry, BindingResource, BindingType, Buffer,
    BufferBindingType, BufferDescriptor, BufferUsages, ColorTargetState, ColorWrites,
    CommandEncoder, Device, LoadOp, PipelineCompilationOptions, RenderPassColorAttachment,
    RenderPipeline, ShaderStages, StoreOp, SurfaceConfiguration, TextureSampleType, TextureView,
    TextureViewDimension,
};

pub(crate) struct PostProcessing {
    pipeline: RenderPipeline,
    bind_group: BindGroup,
    pub uniform_buffer: Buffer,
    pub uniforms: PostProcessUniforms,
}

impl PostProcessing {
    pub fn new(
        device: &Device,
        surface_config: &SurfaceConfiguration,
        color: &ColorTexture,
        depth: &DepthTexture,
        sample_count: u32,
    ) -> Self {
        let multisampled = sample_count != 1;
        let shader_source =
            std::fs::read_to_string("engine/renderer-mesh/src/process.wgsl").unwrap();
        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(&shader_source)),
        });
        // let shader = device.create_shader_module(wgpu::include_wgsl!("process.wgsl"));

        let uniform_buffer = device.create_buffer(&BufferDescriptor {
            label: None,
            size: std::mem::size_of::<PostProcessUniforms>() as u64,
            usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });

        // let sampler = device.create_sampler(&SamplerDescriptor {
        //     label: None,
        //     address_mode_u: AddressMode::Repeat,
        //     address_mode_v: AddressMode::Repeat,
        //     address_mode_w: AddressMode::Repeat,
        //     mag_filter: wgpu::FilterMode::Linear,
        //     min_filter: wgpu::FilterMode::Linear,
        //     mipmap_filter: wgpu::FilterMode::Linear,
        //     lod_min_clamp: 0.,
        //     lod_max_clamp: 1.,
        //     compare: None,
        //     anisotropy_clamp: 1,
        //     border_color: None,
        // });

        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("post processing"),
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStages::FRAGMENT,
                    ty: BindingType::Buffer {
                        ty: BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStages::FRAGMENT,
                    ty: BindingType::Texture {
                        sample_type: TextureSampleType::Float { filterable: false },
                        view_dimension: TextureViewDimension::D2,
                        multisampled,
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 2,
                    visibility: ShaderStages::FRAGMENT,
                    ty: BindingType::Texture {
                        sample_type: TextureSampleType::Depth,
                        view_dimension: TextureViewDimension::D2,
                        multisampled,
                    },
                    count: None,
                },
                // BindGroupLayoutEntry {
                //     binding: 3,
                //     visibility: ShaderStages::FRAGMENT,
                //     ty: BindingType::Sampler(SamplerBindingType::Filtering),
                //     count: None,
                // },
            ],
        });
        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("post processing"),
            layout: &bind_group_layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: uniform_buffer.as_entire_binding(),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::TextureView(&color.view),
                },
                BindGroupEntry {
                    binding: 2,
                    resource: BindingResource::TextureView(&depth.view),
                },
                // BindGroupEntry {
                //     binding: 3,
                //     resource: BindingResource::Sampler(&sampler),
                // },
            ],
        });

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });

        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("post processing"),
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vertex_stage",
                buffers: &[],
                compilation_options: PipelineCompilationOptions::default(),
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                compilation_options: PipelineCompilationOptions::default(),
                entry_point: "fragment_stage",
                targets: &[Some(ColorTargetState {
                    blend: None,
                    format: surface_config.format,
                    write_mask: ColorWrites::ALL,
                })],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                polygon_mode: wgpu::PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState::default(),
            multiview: None,
        });

        Self {
            pipeline,
            bind_group,
            uniform_buffer,
            uniforms: PostProcessUniforms { far: 0., near: 0. },
        }
    }

    pub fn render_pass(&mut self, encoder: &mut CommandEncoder, target: &TextureView) {
        let mut pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("post processing pass"),
            color_attachments: &[Some(RenderPassColorAttachment {
                view: target,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: LoadOp::Clear(wgpu::Color::RED),
                    store: StoreOp::Store,
                },
            })],
            depth_stencil_attachment: None,
            timestamp_writes: None,
            occlusion_query_set: None,
        });
        pass.set_pipeline(&self.pipeline);
        pass.set_bind_group(0, &self.bind_group, &[]);
        pass.draw(0..4, 0..1);
    }
}
