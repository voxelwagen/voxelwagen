//    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
//    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
//    Copyright (C) 2024 metamuffin <metamuffin.org>

struct Uniforms { near: f32, far: f32 };
@group(0) @binding(0) var<uniform> un: Uniforms;
@group(0) @binding(1) var frame_color: texture_2d<f32>;
@group(0) @binding(2) var frame_depth: texture_depth_2d;

struct Vertex {
    @builtin(position) clip: vec4<f32>,
    @location(0) uv: vec2<f32>,
}
@vertex
fn vertex_stage(@builtin(vertex_index) in_vertex_index: u32) -> Vertex {
    let x = f32(i32(in_vertex_index & 1u) * 2 - 1);
    let y = f32(i32(in_vertex_index / 2u) * 2 - 1);
    var v: Vertex;
    v.clip = vec4<f32>(x, y, 0.0, 1.0);
    v.uv = vec2<f32>(x, y) * 0.5 + 0.5;
    v.uv.y *= -1.;
    v.uv.y += 1.;
    return v;
}

@fragment
fn fragment_stage(in: Vertex) -> @location(0) vec4<f32> {
    let uv = in.uv * vec2<f32>(textureDimensions(frame_color));
    var color = textureLoad(frame_color, vec2<i32>(round(uv)), 0).rgb;
    let depth_sample = textureLoad(frame_depth, vec2<i32>(round(uv)), 0);
    let depth = 2.0 * un.near * un.far / (un.far + un.near - (depth_sample * 2.0 - 1.0) * (un.far - un.near)); // return vec4(vec3(log(depth) * 0.1), 1.);    
    color = atmosphere(depth, color);
    return vec4(color, 1.);
    // return vec4(vec3(depth * 0.1), 1.);
}

fn atmosphere(depth: f32, color: vec3<f32>) -> vec3<f32> {
    let f = pow(vec3(2.), -0.0001 * depth * vec3(1., 2., 4.));
    return mix(vec3(0.3), color, f);
}
