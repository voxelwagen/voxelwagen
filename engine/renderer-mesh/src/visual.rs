/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use crate::{
    atlas::AtlasTexture,
    camera::{project, Camera},
    process::PostProcessing,
    repr::Vertex,
    texture::{ColorTexture, DepthTexture},
    SharedState,
};
use crossbeam_channel::Receiver;
use log::trace;
use std::{mem::size_of, num::NonZeroU64, sync::Arc};
use voxelwagen_common::{compat::instant::Instant, matrix::Mat4, EMat, EPos};
use wgpu::{
    util::StagingBelt, BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayoutDescriptor,
    BindGroupLayoutEntry, BindingResource, BindingType, BlendComponent, BlendFactor,
    BlendOperation, BlendState, Buffer, BufferAddress, ColorTargetState, ColorWrites,
    CommandEncoder, CompareFunction, DepthBiasState, DepthStencilState, Device, Face,
    FragmentState, FrontFace, LoadOp, MultisampleState, PipelineCompilationOptions,
    PipelineLayoutDescriptor, PolygonMode, PrimitiveState, PrimitiveTopology, PushConstantRange,
    Queue, RenderPass, RenderPassColorAttachment, RenderPipeline, RenderPipelineDescriptor,
    ShaderStages, StencilState, StoreOp, SurfaceConfiguration, TextureFormat, TextureSampleType,
    TextureView, TextureViewDimension, VertexBufferLayout, VertexState, VertexStepMode,
};

pub struct Visual {
    pub device: Arc<Device>,
    pub queue: Arc<Queue>,
    pub surface_config: SurfaceConfiguration,
    pub staging_belt: StagingBelt,
    pub uploads: Receiver<(Arc<Buffer>, Vec<u8>)>,

    pub pipeline: RenderPipeline,
    pub bind_group: BindGroup,
    pub(crate) post_processing: PostProcessing,

    pub sample_count: u32,
    pub(crate) framebuffer_depth: DepthTexture,
    pub(crate) framebuffer_color: ColorTexture,

    pub(crate) atlas_texture: AtlasTexture,
    pub shared: SharedState,

    pub camera: Camera,
    pub last_frame: Instant,
    pub stats: String,
    pub frame: usize,
    pub do_post_process: bool,
}

impl Visual {
    pub(crate) fn new(
        shared: SharedState,
        device: &Arc<Device>,
        queue: &Arc<Queue>,
        surface_config: &SurfaceConfiguration,
        uploads: Receiver<(Arc<Buffer>, Vec<u8>)>,
        atlas_texture: AtlasTexture,
    ) -> Self {
        let sample_count = 1;

        // let mesh_shader_source =
        //     std::fs::read_to_string("engine/renderer-mesh/src/visual/mesh.wgsl").unwrap();
        // let mesh_shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
        //     label: None,
        //     source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(&mesh_shader_source)),
        // });
        let mesh_shader = device.create_shader_module(wgpu::include_wgsl!("mesh.wgsl"));

        let framebuffer_depth = DepthTexture::new(device, surface_config, sample_count);
        let framebuffer_color = ColorTexture::new(device, surface_config, sample_count);

        let post_processing = PostProcessing::new(
            device,
            surface_config,
            &framebuffer_color,
            &framebuffer_depth,
            sample_count,
        );

        let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            entries: &[BindGroupLayoutEntry {
                binding: 0,
                visibility: ShaderStages::FRAGMENT,
                ty: BindingType::Texture {
                    sample_type: TextureSampleType::Float { filterable: false },
                    view_dimension: TextureViewDimension::D2,
                    multisampled: false,
                },
                count: None,
            }],
            label: None,
        });
        let bind_group = device.create_bind_group(&BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &[BindGroupEntry {
                binding: 0,
                resource: BindingResource::TextureView(&atlas_texture.view),
            }],
            label: None,
        });

        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[PushConstantRange {
                range: 0..(size_of::<f32>() as u32 * 16),
                stages: ShaderStages::VERTEX,
            }],
        });

        let pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex: VertexState {
                compilation_options: PipelineCompilationOptions::default(),
                module: &mesh_shader,
                entry_point: "vertex_stage",
                buffers: &[VertexBufferLayout {
                    array_stride: size_of::<Vertex>() as BufferAddress,
                    step_mode: VertexStepMode::Vertex,
                    attributes: Vertex::attributes(),
                }],
            },
            fragment: Some(FragmentState {
                module: &mesh_shader,
                compilation_options: PipelineCompilationOptions::default(),
                entry_point: "fragment_stage",
                targets: &[Some(ColorTargetState {
                    blend: Some(BlendState {
                        color: BlendComponent {
                            src_factor: BlendFactor::SrcAlpha,
                            dst_factor: BlendFactor::OneMinusSrcAlpha,
                            operation: BlendOperation::Add,
                        },
                        alpha: BlendComponent::OVER,
                    }),
                    format: surface_config.format,
                    write_mask: ColorWrites::ALL,
                })],
            }),
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: FrontFace::Ccw,
                cull_mode: Some(Face::Back),
                polygon_mode: if std::env::var("DEBUG_RENDER_WIREFRAME").is_ok() {
                    PolygonMode::Line
                } else {
                    PolygonMode::Fill
                },
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: Some(DepthStencilState {
                format: TextureFormat::Depth32Float,
                depth_write_enabled: true,
                depth_compare: CompareFunction::Less,
                stencil: StencilState::default(),
                bias: DepthBiasState::default(),
            }),
            multisample: MultisampleState {
                count: sample_count,
                alpha_to_coverage_enabled: false,
                mask: !0,
            },
            multiview: None,
        });

        Self {
            uploads,
            post_processing,
            shared,
            atlas_texture,
            do_post_process: true,
            sample_count,
            framebuffer_depth,
            framebuffer_color,
            bind_group,
            pipeline,
            staging_belt: StagingBelt::new(4096),
            last_frame: Instant::now(),
            camera: Camera {
                position: EPos::ZERO,
                rotation: EMat::UNIT,
                aspect: surface_config.width as f32 / surface_config.height as f32,
                far: 100000.,
                near: 0.05,
                fov: 45.,
            },
            device: device.clone(),
            surface_config: surface_config.clone(),
            queue: queue.clone(),
            stats: String::new(),
            frame: 0,
        }
    }

    pub fn reconfigure(&mut self, config: &SurfaceConfiguration) {
        self.surface_config = config.clone();
        self.camera.aspect = config.width as f32 / config.height as f32;
        self.framebuffer_depth = DepthTexture::new(&self.device, config, self.sample_count);
        self.framebuffer_color = ColorTexture::new(&self.device, config, self.sample_count);
        self.post_processing = PostProcessing::new(
            &self.device,
            &self.surface_config,
            &self.framebuffer_color,
            &self.framebuffer_depth,
            self.sample_count,
        );
    }

    pub fn render(&mut self, encoder: &mut CommandEncoder, target: &TextureView) {
        {
            let now = Instant::now();
            let camera = self
                .shared
                .callbacks
                .get_camera((now - self.last_frame).as_secs_f32());
            *self.shared.camera.write().unwrap() = camera.0;
            self.camera.position = camera.0;
            self.camera.rotation = camera.1;
            self.last_frame = now
        }

        // self.state.update_sprites();
        self.post_processing.uniforms.far = self.camera.far;
        self.post_processing.uniforms.near = self.camera.near;

        self.queue.write_buffer(
            &self.post_processing.uniform_buffer,
            0,
            bytemuck::cast_slice(&[self.post_processing.uniforms]),
        );

        {
            self.uploads.try_iter().for_each(|(buffer, data)| {
                trace!("staged upload of {} bytes", data.len());
                let mut view = self.staging_belt.write_buffer(
                    encoder,
                    &buffer,
                    0,
                    NonZeroU64::new(data.len().try_into().unwrap()).unwrap(),
                    &self.device,
                );
                view.copy_from_slice(&data);
            });
        }
        self.atlas_texture.apply_patches(&self.queue);

        {
            let view_projection = self.camera.view_projection_matrix();

            let chunks = self.shared.chunk_meshes.read().unwrap();
            let sprites = self.shared.sprites.read().unwrap();

            let visible_chunks = chunks
                .iter()
                .filter(|(cs, c)| {
                    (c.mesh.solid.visible() || c.mesh.translucent.visible())
                        && self
                            .camera
                            .is_visible(cs.range.start.epos()..cs.range.end.epos())
                })
                .collect::<Vec<_>>();

            let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("mesh render pass"),
                // color_attachments: &[Some(self.framebuffer_color.as_direct_render_atttachment())],
                color_attachments: &[Some(match (self.sample_count != 1, self.do_post_process) {
                    (false, false) => RenderPassColorAttachment {
                        view: target,
                        resolve_target: None,
                        ops: wgpu::Operations {
                            load: LoadOp::Clear(wgpu::Color::BLACK),
                            store: StoreOp::Store,
                        },
                    },
                    (true, false) => self.framebuffer_color.as_resolve_render_atttachment(target),
                    _ => self.framebuffer_color.as_direct_render_atttachment(),
                })],
                depth_stencil_attachment: Some(self.framebuffer_depth.render_atttachment()),
                timestamp_writes: None,
                occlusion_query_set: None,
            });
            rpass.set_pipeline(&self.pipeline);
            rpass.set_bind_group(0, &self.bind_group, &[]);

            // visible.sort_by_cached_key(|((cc, _), _)| {
            //     (cc.epos()
            //         .clamp(self.controller.camera.position)
            //         .distance(self.controller.camera.position)
            //         * -1024.) as i64
            // });

            rpass.set_view_projection(view_projection);
            for (_, c) in &visible_chunks {
                c.mesh.solid.draw(&mut rpass)
            }
            for (_, s) in sprites.iter() {
                rpass.set_view_projection(view_projection * s.projection);
                s.mesh.solid.draw(&mut rpass)
            }
            rpass.set_view_projection(view_projection);
            for (_, c) in &visible_chunks {
                c.mesh.translucent.draw(&mut rpass)
            }
            for (_, s) in sprites.iter() {
                rpass.set_view_projection(view_projection * s.projection);
                s.mesh.translucent.draw(&mut rpass)
            }
        };

        if self.do_post_process {
            self.post_processing.render_pass(encoder, target)
        }

        self.frame += 1;
    }

    pub fn pre_submit(&mut self) {
        self.staging_belt.finish();
    }
    pub fn post_submit(&mut self) {
        self.staging_belt.recall();
    }
    pub fn project_screenspace(&self, p: EPos) -> (f32, f32) {
        let k = project(&self.camera.view_projection_matrix(), p);
        (
            (k.x * 0.5 + 0.5) * self.surface_config.width as f32,
            (k.y * -0.5 + 0.5) * self.surface_config.height as f32,
        )
    }
}

trait SetProjectionExt {
    fn set_view_projection(&mut self, view_projection: Mat4<f32>);
}
impl SetProjectionExt for RenderPass<'_> {
    fn set_view_projection(&mut self, view_projection: Mat4<f32>) {
        let k: [[f32; 4]; 4] = view_projection.transpose().into();
        self.set_push_constants(ShaderStages::VERTEX, 0, bytemuck::cast_slice(&k));
    }
}
