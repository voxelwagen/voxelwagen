/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::mesh::MeshBundle;
use crate::MeshViewer;
use std::sync::Arc;
use voxelwagen_common::{matrix::Mat4, Block};

pub struct Sprite {
    pub mesh: Arc<MeshBundle>,
    pub current_model: Block,
    pub projection: Mat4<f32>,
}

impl MeshViewer {
    pub fn update_sprites(&self) {
        // let mut g = self.sprites.write().unwrap();
        // for (id, sp) in g.iter_mut() {
        //     if let Some((model, offset, transform)) = self.callbacks.get_sprite(*id) {
        //         if sp.current_model != model {
        //             todo!("model change not yet supported")
        //         }
        //         sp.projection = Mat4::from_transform_offset(offset, transform)
        //     }
        // }
    }

    // pub fn block_model_mesh(&self, block: Block) -> Arc<MeshBundle> {
    //     let mut g = self.block_model_meshes.write().unwrap();

    //     g.entry(block)
    //         .or_insert_with(|| {
    //             // let model = &self.blockdefs[block as usize];
    //             // build_mesh(
    //             //     bpool,
    //             //     BPos::ZERO..BPos::ONE * 16,
    //             //     1,
    //             //     |p| model,
    //             //     |p| model.,
    //             // )
    //             // .into()

    //             let mut mesh = MeshBuilder::default();
    //             let texcoord = blockdata::Block(block).texcoord();

    //             for f in BlockFace::LIST {
    //                 mesh.face(EPos::ZERO, f, texcoord, [1.; 4], 1.);
    //             }

    //             MeshBundle {
    //                 translucent: Mesh::empty(),
    //                 solid: Mesh::new(&self.buffer_pool, mesh.vertex, mesh.index),
    //             }
    //             .into()
    //         })
    //         .clone()
    // }
}
