/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crossbeam_channel::{unbounded, Receiver, Sender};
use log::{debug, error};
use std::{
    collections::{hash_map::DefaultHasher, HashMap},
    hash::{Hash, Hasher},
    ops::Range,
    sync::RwLock,
};
use voxelwagen_common::{vector::Vec2, Material};
use wgpu::{
    Device, Extent3d, ImageCopyTexture, Origin3d, Queue, Texture, TextureDescriptor,
    TextureDimension, TextureFormat, TextureUsages, TextureView, TextureViewDescriptor,
};

pub trait TAtlas {
    fn add_texture(&mut self, size: Vec2<usize>, texture: Vec<Material>) -> Range<Vec2<usize>>;
}

pub struct AtlasPatch {
    offset: Vec2<usize>,
    size: Vec2<usize>,
    data: Vec<u8>,
}

pub struct Atlas {
    pub res: usize,
    row_height: usize,
    cursor: Vec2<usize>,
    patch_send: Sender<AtlasPatch>,
    texture_dedup: HashMap<u64, Range<Vec2<usize>>>,
}
pub(crate) struct AtlasTexture {
    patch_recv: Receiver<AtlasPatch>,
    pub texture: Texture,
    pub view: TextureView,
}

impl Atlas {
    pub(crate) fn new(device: &Device, res: usize) -> (Atlas, AtlasTexture) {
        let size = Extent3d {
            height: res as u32,
            width: res as u32,
            ..Default::default()
        };

        let texture = device.create_texture(&TextureDescriptor {
            label: Some("game texture"),
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8UnormSrgb,
            usage: TextureUsages::TEXTURE_BINDING | TextureUsages::COPY_DST,
            view_formats: &[],
        });

        let (patch_send, patch_recv) = unbounded();

        let view = texture.create_view(&TextureViewDescriptor::default());
        (
            Atlas {
                texture_dedup: HashMap::new(),
                res,
                row_height: 0,
                cursor: Vec2 { x: 0, y: 0 },
                patch_send,
            },
            AtlasTexture {
                texture,
                view,
                patch_recv,
            },
        )
    }
    fn patch(&mut self, offset: Vec2<usize>, size: Vec2<usize>, texture: Vec<Material>) {
        self.patch_send
            .send(AtlasPatch {
                offset,
                size,
                data: texture
                    .into_iter()
                    .flat_map(|m| [m.color[0], m.color[1], m.color[2], m.alpha])
                    .collect(),
            })
            .unwrap();
    }
    fn add_texture(&mut self, size: Vec2<usize>, texture: Vec<Material>) -> Range<Vec2<usize>> {
        let texture_hash = hash_texture(size.x, &texture);
        if let Some(coord) = self.texture_dedup.get(&texture_hash) {
            return coord.to_owned();
        }

        let Vec2 { x: w, y: h } = size;
        if self.cursor.x + w > self.res {
            self.cursor.y += self.row_height;
            self.row_height = 0;
            self.cursor.x = 0;
        }
        if self.cursor.y + h > self.res {
            error!("failed to add {w}x{h} to atlas");
            error!("texture too big or atlas full");
            return Vec2::splat(0)..Vec2::splat(1);
        }
        self.row_height = self.row_height.max(h);
        debug!(
            "adding {w}x{h} texture to atlas at {},{}",
            self.cursor.x, self.cursor.y
        );
        let offset = self.cursor;
        // #[allow(clippy::needless_range_loop)]
        // for x in 0..w {
        //     for y in 0..h {
        //         let mat = texture[x + w * y];
        //         self.image[(self.cursor.x + x) + (self.cursor.y + y) * self.res] =
        //             [mat.color[0], mat.color[1], mat.color[2], mat.alpha];
        //     }
        // }
        self.cursor.x += w;

        self.patch(offset, size, texture);
        let ret = offset..offset + Vec2 { x: w, y: h };
        self.texture_dedup.insert(texture_hash, ret.clone());
        ret
    }
}

impl TAtlas for &RwLock<Atlas> {
    fn add_texture(&mut self, size: Vec2<usize>, texture: Vec<Material>) -> Range<Vec2<usize>> {
        self.write().unwrap().add_texture(size, texture)
    }
}

impl AtlasTexture {
    pub fn apply_patches(&mut self, queue: &Queue) {
        for patch in self.patch_recv.try_iter() {
            queue.write_texture(
                ImageCopyTexture {
                    aspect: wgpu::TextureAspect::All,
                    mip_level: 0,
                    origin: Origin3d {
                        x: patch.offset.x as u32,
                        y: patch.offset.y as u32,
                        z: 0,
                    },
                    texture: &self.texture,
                },
                patch.data.as_slice(),
                wgpu::ImageDataLayout {
                    offset: 0,
                    bytes_per_row: Some(patch.size.x as u32 * 4),
                    rows_per_image: Some(patch.size.y as u32),
                },
                Extent3d {
                    width: patch.size.x as u32,
                    height: patch.size.y as u32,
                    depth_or_array_layers: 1,
                },
            )
        }
    }
}

fn hash_texture(width: usize, texture: &[Material]) -> u64 {
    let mut h = DefaultHasher::new();
    width.hash(&mut h);
    texture.hash(&mut h);
    h.finish()
}
