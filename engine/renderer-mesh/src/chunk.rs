/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::{bufferpool::BufferPool, mesh::MeshBundle};
use crate::{atlas::Atlas, meshgen::build_mesh};
use log::debug;
use std::{
    ops::Range,
    sync::{Arc, RwLock},
};
use voxelwagen_common::{range::RangeExt, BPos};
use voxelwagen_renderer_common::RendererCallbacks;

pub struct Chunk {
    pub mesh: MeshBundle,
}

impl Chunk {
    pub fn new(
        bpool: &BufferPool,
        atlas: &RwLock<Atlas>,
        world: &Arc<dyn RendererCallbacks>,
        range: Range<BPos>,
        step: usize,
    ) -> Self {
        debug!("building chunk {range:?}");
        let world = world.get_chunk(range.expand(step as i64), step);
        Self {
            mesh: build_mesh(bpool, atlas, range, step, &world),
        }
    }
}
