/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{Chunk, SharedState};
use crossbeam_channel::Receiver;
use log::{info, warn};
use rayon::{ThreadPool, ThreadPoolBuilder};
use std::{
    collections::HashSet, num::NonZeroUsize, ops::Range, sync::mpsc::channel,
    thread::available_parallelism,
};
use voxelwagen_common::{
    range::{DistanceRangeExt, RangeExt, RangeIter},
    BPos, EPos,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct ChunkSpec {
    pub range: Range<BPos>,
    pub lod_size: usize,
}

pub struct Loader {
    max_par: usize,
    shared: SharedState,
    thread_pool: ThreadPool,
    sched: LoadScheduler,
    events: Receiver<LoaderEvent>,
}

pub enum LoaderEvent {
    DamageBlocks(Range<BPos>),
}

impl Loader {
    pub fn new(shared: SharedState, events: Receiver<LoaderEvent>) -> Self {
        Self {
            max_par: available_parallelism()
                .unwrap_or_else(|_| {
                    warn!("available_parallelism not available, not using parallel world loading");
                    NonZeroUsize::new(1).unwrap()
                })
                .into(),
            events,
            shared,
            sched: LoadScheduler::new(5),
            thread_pool: ThreadPoolBuilder::new()
                .thread_name(|i| format!("meshgen #{i}"))
                .build()
                .unwrap(),
        }
    }

    pub fn chunk_loop(&mut self) {
        if std::env::var("DEBUG_LOADER_FAST").is_ok() {
            let mut running_tasks = 0;
            let (finish_tx, finish_rx) = channel();
            let mut tasks = RangeIter::new(BPos::splat(-5)..BPos::splat(5), 1)
                .map(|p| ChunkSpec {
                    range: (p * BPos::splat(32))..((p + BPos::ONE) * BPos::splat(32)),
                    lod_size: 1,
                })
                .collect::<Vec<_>>();

            loop {
                for () in finish_rx.try_iter() {
                    running_tasks -= 1;
                }

                while let Some(task) = tasks.pop() {
                    let shared = self.shared.clone();
                    let finish_tx = finish_tx.clone();
                    self.thread_pool.spawn(move || {
                        let chunk = Chunk::new(
                            &shared.buffer_pool,
                            &shared.atlas,
                            &shared.callbacks,
                            task.range.clone(),
                            task.lod_size,
                        );
                        let mut g = shared.chunk_meshes.write().unwrap();
                        g.insert(task, chunk);
                        finish_tx.send(()).unwrap();
                    });
                    running_tasks += 1;
                    if running_tasks > 8 {
                        break;
                    }
                }
                std::thread::sleep(std::time::Duration::from_millis(50))
            }
        }

        let mut running_tasks = 0;
        let (finish_tx, finish_rx) = channel();
        loop {
            let around = self.shared.camera.read().unwrap().to_owned();

            for ev in self.events.try_iter() {
                match ev {
                    LoaderEvent::DamageBlocks(range) => self.sched.damage(range),
                }
            }

            {
                self.sched.create_tasks(around);

                for task in finish_rx.try_iter() {
                    running_tasks -= 1;
                    self.sched.finish_task(task);
                }

                while running_tasks < self.max_par {
                    let Some(t) = self.sched.next_task() else {
                        break;
                    };
                    running_tasks += 1;

                    let shared = self.shared.clone();
                    let finish_tx = finish_tx.clone();
                    self.thread_pool.spawn(move || {
                        info!("start task {t:?}");
                        match t.clone() {
                            LoaderTask::Subdivide(cp) => subdivide(cp, shared),
                            LoaderTask::Merge(cp) => merge(cp, shared),
                            LoaderTask::Reload(cp) => reload(cp, shared),
                        }
                        info!("finish task {t:?}");
                        finish_tx.send(t).unwrap();
                    })
                }
            }
            std::thread::sleep(std::time::Duration::from_millis(100))
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum LoaderTask {
    Subdivide(ChunkSpec),
    Merge(ChunkSpec),
    Reload(ChunkSpec),
}

pub struct LoadScheduler {
    load_fac: f32,
    unload_fac: f32,
    stable: HashSet<ChunkSpec>,
    pending: TaskProrityQueue,
}

#[derive(Default)]
struct TaskProrityQueue {
    set: HashSet<LoaderTask>,
    order: Vec<LoaderTask>,
}

impl Default for LoadScheduler {
    fn default() -> Self {
        Self {
            load_fac: 1.5 * 5. * 10.,
            unload_fac: 6. * 5. * 10.,
            stable: Default::default(),
            pending: Default::default(),
        }
    }
}

impl TaskProrityQueue {
    pub fn enqueue(&mut self, task: LoaderTask) {
        let new = self.set.insert(task.clone());
        if new {
            self.order.push(task)
        }
    }
    pub fn finish(&mut self, task: &LoaderTask) {
        self.set.remove(task);
    }
    pub fn next(&mut self) -> Option<LoaderTask> {
        self.order.pop()
    }
    pub fn sort(&mut self, around: EPos) {
        self.order.sort_by_cached_key(|t| {
            (match t {
                LoaderTask::Subdivide(cp) => cp.range.distance(around) * -1.,
                LoaderTask::Merge(cp) => cp.range.distance(around) * 1.,
                LoaderTask::Reload(cp) => cp.range.distance(around) * -2.,
            } * 100.) as i64
        })
    }
}

impl LoadScheduler {
    pub fn new(_view_distance: i64) -> Self {
        let mut selff = LoadScheduler::default();
        selff.pending.enqueue(LoaderTask::Reload(ChunkSpec::ROOT));
        selff
    }
    pub fn damage(&mut self, range: Range<BPos>) {
        let range = range.expand(1); // blocks at chunk borders need to update the neighbour.
        let inval = self
            .stable
            .iter()
            .filter(|cp| cp.range.overlaps(&range))
            .map(|e| e.to_owned())
            .collect::<Vec<_>>();
        for cp in inval {
            self.pending.enqueue(LoaderTask::Reload(cp));
        }
    }

    fn create_tasks(&mut self, around: EPos) {
        let mut unstablize = Vec::new();
        for cp in &self.stable {
            let d = cp.range.center().epos().distance(around);
            let span = cp.range.size().x as f32;
            if d < span * self.load_fac && cp.lod_size > 1 {
                unstablize.push(cp.to_owned());
                self.pending.enqueue(LoaderTask::Subdivide(cp.to_owned()));
            }
            if d > span * self.unload_fac && cp.lod_size < ChunkSpec::ROOT.lod_size {
                let children = cp.parent().subdivide();
                // if one of the children does not exist (i.e. it is subdivided itself) we cant merge
                if children.iter().all(|c| self.stable.contains(c)) {
                    unstablize.extend(children);
                    self.pending.enqueue(LoaderTask::Merge(cp.parent()));
                }
            }
        }
        for cp in unstablize {
            self.stable.remove(&cp);
        }
        self.pending.sort(around);
    }
    fn next_task(&mut self) -> Option<LoaderTask> {
        self.pending.next()
    }
    fn finish_task(&mut self, task: LoaderTask) {
        self.pending.finish(&task);
        match task {
            LoaderTask::Subdivide(cp) => {
                self.stable.extend(cp.subdivide());
            }
            LoaderTask::Merge(cp) => {
                self.stable.insert(cp);
            }
            LoaderTask::Reload(cp) => {
                self.stable.insert(cp);
            }
        };
    }
}

impl ChunkSpec {
    const ROOT: Self = {
        let mesh_size = 32;
        let root_span = 1 << 16;
        ChunkSpec {
            range: BPos::splat(-(root_span >> 1))..BPos::splat(root_span >> 1),
            lod_size: root_span as usize / mesh_size,
        }
    };
    fn parent(&self) -> ChunkSpec {
        let mut c = ChunkSpec::ROOT;
        while c.lod_size > self.lod_size * 2 {
            c = c
                .subdivide()
                .into_iter()
                .find(|c| c.range.overlaps(&self.range))
                .unwrap();
        }
        c
    }

    fn subdivide(&self) -> Vec<ChunkSpec> {
        (0..2)
            .flat_map(|x| (0..2).map(move |y| (0..2).map(move |z| BPos { x, y, z })))
            .flatten()
            .map(|ver| {
                let lod_size = self.lod_size >> 1;
                let size = self.range.size() / 2;
                let offset = self.range.start + size * ver;
                let range = offset..offset + size;
                ChunkSpec { range, lod_size }
            })
            .collect::<Vec<_>>()
    }
}

fn subdivide(s: ChunkSpec, shared: SharedState) {
    let loaded = s
        .subdivide()
        // .into_par_iter()
        .into_iter()
        .map(|sp| {
            (
                sp.clone(),
                Chunk::new(
                    &shared.buffer_pool,
                    &shared.atlas,
                    &shared.callbacks,
                    sp.range,
                    sp.lod_size,
                ),
            )
        })
        .collect::<Vec<_>>();

    {
        let mut g = shared.chunk_meshes.write().unwrap();
        g.remove(&s);
        for (spec, chunk) in loaded {
            g.insert(spec, chunk);
        }
    }
}
fn merge(s: ChunkSpec, shared: SharedState) {
    let chunk = Chunk::new(
        &shared.buffer_pool,
        &shared.atlas,
        &shared.callbacks,
        s.range.clone(),
        s.lod_size,
    );
    {
        let mut g = shared.chunk_meshes.write().unwrap();
        for cp in s.subdivide() {
            g.remove(&cp);
        }
        g.insert(s, chunk);
    }
}
fn reload(s: ChunkSpec, shared: SharedState) {
    let chunk = Chunk::new(
        &shared.buffer_pool,
        &shared.atlas,
        &shared.callbacks,
        s.range.clone(),
        s.lod_size,
    );
    {
        let mut g = shared.chunk_meshes.write().unwrap();
        drop(g.insert(s, chunk))
    }
}
