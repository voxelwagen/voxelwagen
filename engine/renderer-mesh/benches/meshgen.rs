/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use criterion::{criterion_group, criterion_main, Criterion};
use voxelwagen_common::{
    vector::pack::{v2, v3},
    BPos, Material,
};
use voxelwagen_renderer_mesh::{generate_mesh, TAtlas};
use voxelwagen_world::preload::PreloadedWorld;

struct DummyAtlas;
impl TAtlas for DummyAtlas {
    fn add_texture(
        &mut self,
        _size: voxelwagen_common::vector::Vec2<usize>,
        _texture: Vec<Material>,
    ) -> std::ops::Range<voxelwagen_common::vector::Vec2<usize>> {
        v2(0, 0)..v2(1, 1)
    }
}

pub fn criterion_benchmark(c: &mut Criterion) {
    let range = v3(0, 0, 0)..v3(32, 32, 32);
    let prange = v3(-1, -1, -1)..v3(33, 33, 33);

    {
        let world = PreloadedWorld::new(&|_p: BPos| Material::AIR, prange.clone(), 1);
        c.bench_function("meshgen_empty", |b| {
            b.iter(|| generate_mesh(DummyAtlas, range.clone(), 1, &world))
        });
    }
    {
        let world = PreloadedWorld::new(
            &|BPos { x, y, z }| {
                let k = xs(xs(x as u32 ^ xs(y as u32 ^ xs(z as u32))));
                if k < u32::MAX / 2 {
                    Material::AIR
                } else {
                    Material::DEBUG
                }
            },
            prange.clone(),
            1,
        );
        c.bench_function("meshgen_random", |b| {
            b.iter(|| generate_mesh(DummyAtlas, range.clone(), 1, &world))
        });
    }
    {
        let world = PreloadedWorld::new(
            &|BPos { x, y, z }| {
                let k = xs(xs(x as u32 ^ xs(y as u32 ^ xs(z as u32))));
                if k < u32::MAX / 16 {
                    Material::AIR
                } else {
                    Material::DEBUG
                }
            },
            prange.clone(),
            1,
        );
        c.bench_function("meshgen_holes", |b| {
            b.iter(|| generate_mesh(DummyAtlas, range.clone(), 1, &world))
        });
    }

    // c.bench_function("meshgen_random", |b| {
    //     b.iter(|| {
    //         generate_mesh(
    //             v3(0, 0, 0)..v3(32, 32, 32),
    //             1,
    //             |p| {
    //                 black_box({
    //
    //                     let [x, y, z] = p.array().map(|c| c as i32 as u32);
    //                     let k = xs(xs(x ^ xs(y ^ xs(z))));
    //                     if k < u32::MAX / 2 {
    //                         Solidity::Invisible
    //                     } else {
    //                         Solidity::Solid
    //                     }
    //                 })
    //             },
    //             |_| black_box(Default::default()),
    //         )
    //     })
    // });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);

fn xs(mut x: u32) -> u32 {
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    x
}
