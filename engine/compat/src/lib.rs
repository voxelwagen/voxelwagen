/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use std::ops::{Deref, DerefMut};

pub mod instant;

pub fn init_logging() {
    #[cfg(not(target_arch = "wasm32"))]
    env_logger::init_from_env("LOG");
    #[cfg(target_arch = "wasm32")]
    {
        std::panic::set_hook(Box::new(console_error_panic_hook::hook));
        console_log::init().expect("could not initialize logger");
    }
}

/// Dont use this, it is unsafe. :)
pub struct SendAnyway<T>(pub T);
unsafe impl<T> Send for SendAnyway<T> {}
unsafe impl<T> Sync for SendAnyway<T> {}
impl<T> Deref for SendAnyway<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<T> DerefMut for SendAnyway<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
