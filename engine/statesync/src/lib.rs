/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(associated_type_defaults)]
use bincode::{BorrowDecode, Decode, Encode};
use log::{debug, warn};
use std::{
    collections::{BTreeMap, HashMap, VecDeque},
    fmt::Debug,
    net::SocketAddr,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use voxelwagen_compat::instant::Instant;

pub trait System: Clone + Encode + Decode + Send + Sync + 'static {
    type Input: Clone + Debug + Encode + Decode + for<'a> BorrowDecode<'a>;
    type SideEffect = ();
    fn tick(&mut self) -> (Duration, Vec<Self::SideEffect>);
    fn input(&mut self, input: Self::Input) -> Vec<Self::SideEffect>;
}

#[derive(Debug)]
pub enum ApplyEffect<S> {
    Do(S),
    Undo(S),
}

#[derive(Debug, Clone, Encode, Decode)]
pub struct Instance<S> {
    state: S,
    time: u128,
    next_tick: u128,
}

#[derive(Debug, Clone, Encode, Decode)]
pub enum Packet<S: System> {
    Input(u128, S::Input),
    Time(u128),
    State(Instance<S>, BTreeMap<u128, S::Input>),
}

pub struct StateSync<S: System> {
    startup_buffer: Option<Vec<S::Input>>,
    network_out: VecDeque<Packet<S>>,
    network_time: HashMap<SocketAddr, u128>,
    emit_effects: Vec<ApplyEffect<S::SideEffect>>,

    inputs: BTreeMap<u128, S::Input>,

    head: Instance<S>,
    tail: Instance<S>,
}

impl<S: System> StateSync<S> {
    pub fn create(initial: S) -> Self {
        Self {
            network_time: HashMap::new(),
            startup_buffer: Some(Default::default()),
            network_out: Default::default(),
            head: Instance::new(initial.clone(), Self::now()),
            tail: Instance::new(initial, Self::now()),
            emit_effects: Default::default(),
            inputs: Default::default(),
        }
    }

    pub fn network_out(&mut self) -> Option<Packet<S>> {
        self.network_out.pop_front()
    }
    pub fn network_in(&mut self, address: SocketAddr, packet: Packet<S>) {
        match packet {
            Packet::Input(t, i) => self.remote_input(address, t, i),
            Packet::State(i, ips) => {
                // TODO this is a little more complicated
                self.tail = i;
                self.inputs.extend(ips);
                self.resimulate_head();
                if let Some(buf) = self.startup_buffer.take() {
                    debug!("re-processing startup-delayed inputs...");
                    for i in buf {
                        self.input(i);
                    }
                }
            }
            Packet::Time(t) => {
                let prev = self.network_time.insert(address, t);
                if let Some(prev) = prev {
                    if prev > t {
                        warn!(
                            "{address} is going back in time. (dt={:?})",
                            Duration::from_micros((prev - t) as u64)
                        )
                    }
                }
            }
        }
    }
    pub fn init_peer(&mut self, address: SocketAddr, active: bool) {
        self.network_time.insert(address, Self::now());
        if !active {
            self.network_out
                .push_back(Packet::State(self.tail.clone(), self.inputs.clone()))
        }
    }

    #[inline]
    pub fn head(&self) -> &S {
        &self.head.state
    }

    pub fn extract_side_effects(&mut self, apply: impl FnMut(ApplyEffect<S::SideEffect>)) {
        self.emit_effects.drain(..).for_each(apply)
    }

    pub fn tick(&mut self) {
        let now = Self::now();
        let nt_min = *self.network_time.values().min().unwrap_or(&now);
        debug!(
            "network lag is {:?}",
            Duration::from_micros((now - nt_min) as u64)
        );

        self.head.forward(now, &self.inputs, |x: _| {
            self.emit_effects.push(ApplyEffect::Do(x))
        });
        self.tail.forward(nt_min, &self.inputs, |_| ());

        self.network_out.push_back(Packet::Time(now));
        self.head.time = now;
    }

    pub fn input(&mut self, input: S::Input) {
        if let Some(q) = &mut self.startup_buffer {
            q.push(input.clone());
        }
        let now = Self::now();

        self.network_out
            .push_back(Packet::Input(Self::now(), input.clone()));
        // TODO use actual public ip for consistency
        self.inputs.insert(now, input.clone());

        let se = self.head.state.input(input);
        self.emit_effects
            .extend(se.into_iter().map(ApplyEffect::Do));
    }

    fn remote_input(&mut self, _address: SocketAddr, time: u128, input: S::Input) {
        self.inputs.insert(time, input);
        self.resimulate_head();
    }

    fn resimulate_head(&mut self) {
        debug!(
            "starting head resimulation (tail at {} ~~> head at {})",
            self.tail.time, self.head.time
        );
        debug!("... time machine noises ...");
        let perft = Instant::now();

        let mut new_head = self.tail.clone();
        new_head.forward(self.head.time, &self.inputs, |_| ());

        debug!(
            "resim of {:?} done within {:?}",
            Duration::from_micros((self.head.time - self.tail.time) as u64),
            perft.elapsed()
        );

        self.head = new_head;
    }

    fn now() -> u128 {
        SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_micros()
    }
}

impl<S: System> Instance<S> {
    pub fn new(state: S, now: u128) -> Self {
        Instance {
            state,
            time: now,
            next_tick: now,
        }
    }
    pub fn forward(
        &mut self,
        target: u128,
        inputs: &BTreeMap<u128, S::Input>,
        mut se: impl FnMut(S::SideEffect),
    ) {
        assert!(target >= self.time, "cant go back in time");

        while self.next_tick < target {
            for (_, i) in inputs.range(self.time..self.next_tick) {
                self.state.input(i.to_owned());
            }
            let (next_tick_off, ses) = self.state.tick();
            ses.into_iter().for_each(&mut se);
            self.time = self.next_tick;
            self.next_tick += next_tick_off.as_micros();
        }
        for (_, i) in inputs.range(self.time..target) {
            self.state.input(i.to_owned());
        }
        self.time = target;
    }
}
