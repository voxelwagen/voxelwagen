/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use std::{
    any::{Any, TypeId},
    collections::HashMap,
};
use voxelwagen_common::compat::instant::Instant;

pub struct AnimationState {
    pub time_reference: Instant,
    pub state: HashMap<(u32, TypeId), Box<dyn Any + Send + Sync>>,
}

impl Default for AnimationState {
    fn default() -> Self {
        Self {
            state: Default::default(),
            time_reference: Instant::now(),
        }
    }
}

impl AnimationState {
    pub fn time(&self) -> f32 {
        self.time_reference.elapsed().as_secs_f32() // TODO: 32-bit is not that much, either wrap manualle or use f64
    }
}

#[derive(Default)]
pub struct SmoothFloat(f32);
impl SmoothFloat {
    pub fn next(&mut self, target: f32, dt: f32) -> f32 {
        self.0 += (target - self.0) * dt * 0.1;
        self.0
    }
}
