/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{animation::SmoothFloat, Ui, Widget};
use voxelwagen_common::{range::RangeExt, UVec};

pub fn wheel<'a, T, F, U>(
    index: usize,
    approx_elem_height: f32,
    elements: &'a [T],
    mut show: F,
) -> impl Widget<()> + 'a
where
    F: FnMut(f32, &T) -> U + 'a,
    U: Widget<()>,
{
    move |ui: &mut Ui| {
        let scroll = ui.astate::<SmoothFloat>().next(index as f32, 1.) * -approx_elem_height
            + ui.bounds.size().y / 2.;
        let ob = ui.bounds.clone();
        let mut yoff = 0f32;
        let mut xmax = 0f32;
        for (i, e) in elements.iter().enumerate() {
            ui.bounds = ob.start
                + UVec {
                    x: 0.,
                    y: yoff + scroll,
                }..ob.end;
            ui.push_id();
            let active = ui
                .astate::<SmoothFloat>()
                .next(if i == index { 1. } else { 0. }, 1.);
            let (al, _) = show(active, e).ui(ui);
            xmax = xmax.max(al.x);
            yoff += al.y;
        }
        ui.bounds = ob;
        (ui.bounds.size(), ())
    }
}
