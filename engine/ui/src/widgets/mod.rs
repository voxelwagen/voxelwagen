/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
pub mod wheel;

use crate::{Ui, Widget};
use log::warn;
use voxelwagen_canvas::{primitives::SurfacePrimitives, ShapeColor};
use voxelwagen_common::{color::Color, range::RangeExt, UVec};
use voxelwagen_renderer_common::MouseButton;

pub fn label(text: impl AsRef<str>, size: f32, color: Color) -> impl Widget<()> {
    move |ui: &mut Ui| {
        let size = ui.theme.font.text(
            ShapeColor(color, &mut ui.canvas),
            ui.bounds.start,
            size,
            0.1,
            text.as_ref(),
        );
        (size, ())
    }
}

pub fn fill_rect(color: Color) -> impl Widget<()> {
    move |ui: &mut Ui| {
        ui.canvas.rect(ui.bounds.start, ui.bounds.end, color);
        (ui.bounds.size(), ())
    }
}

pub fn rect<T>(color: Color, w: impl Widget<T>) -> impl Widget<T> {
    move |ui: &mut Ui| {
        ui.push_id();
        let size_prediction = *ui.astate::<UVec>();
        ui.canvas.rect(ui.bounds.start, size_prediction, color);
        let (size, ret) = w.ui(ui);
        let size_prediction = ui.astate::<UVec>();
        *size_prediction = size;
        (size, ret)
    }
}

pub fn clickable<T>(w: impl Widget<T>) -> impl Widget<bool> {
    move |ui: &mut Ui| {
        let (size, _) = w.ui(ui);
        let c = ui.bounds.start..ui.bounds.start + size;
        let c = c.has(&ui.input.mouse_location)
            && ui
                .input
                .mouse_buttons
                .just_pressed
                .contains(&MouseButton::Left);
        (size, c)
    }
}

pub fn pad<T>(d: f32, w: impl Widget<T>) -> impl Widget<T> {
    move |ui: &mut Ui| {
        let ob = ui.bounds.clone();
        let pad = UVec::ONE * d;
        ui.bounds = (ui.bounds.start + pad)..(ui.bounds.end - pad);
        let (size, t) = w.ui(ui);
        ui.bounds = ob;
        (size + pad * 2., t)
    }
}

pub fn fixed<T>(size: UVec, w: impl Widget<T>) -> impl Widget<T> {
    move |ui: &mut Ui| {
        if ui.bounds.size() < size {
            warn!(
                "fixed ui doesnt fit (we need to fit {:?} into {:?})",
                size,
                ui.bounds.size()
            )
        }
        let ob = ui.bounds.clone();
        ui.bounds = ui.bounds.start..ui.bounds.start + size;
        let k = w.ui(ui);
        ui.bounds = ob;
        (size, k.1)
    }
}
