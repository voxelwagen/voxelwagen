/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{animation::AnimationState, input::InputState, theme::Theme, Ui, Widget};
use log::warn;
use voxelwagen_canvas::{Canvas, TransformSurface};
use voxelwagen_common::{UPos, UVec};
use voxelwagen_dyndraw::Vertex;
use voxelwagen_renderer_common::InputEvent;

pub struct UiState {
    pub input: InputState,
    pub canvas: Canvas,
    animation: AnimationState,
    theme: Theme,
    resolution: UVec,
}

impl UiState {
    pub fn new(theme: Theme) -> Self {
        Self {
            animation: AnimationState::default(),
            resolution: UVec::ONE,
            input: InputState::default(),
            canvas: Canvas::default(),
            theme,
        }
    }
    pub fn draw<T>(&mut self, w: impl Widget<T>) {
        self.canvas.clear();
        let mut ui = Ui {
            animation_key: 0xdeadbeef,
            animation: &mut self.animation,
            theme: &mut self.theme,
            input: &self.input,
            canvas: TransformSurface::new_from_to(
                &mut self.canvas,
                UVec::ZERO..self.resolution,
                UVec { x: -1., y: 1. }..UVec { x: 1., y: -1. },
            ),
            bounds: UPos::ZERO..self.resolution,
        };
        let (size, _) = w.ui(&mut ui);
        if size > self.resolution {
            warn!("ui does not fit; overflows by {:?}", size - self.resolution)
        }

        self.input.tick();
    }
    pub fn finish(&mut self) -> (Vec<Vertex>, Vec<u32>) {
        self.canvas.clone().finish()
    }
    pub fn input_event(&mut self, event: &InputEvent) {
        if let InputEvent::Resize { width, height } = event {
            self.resolution = UVec {
                x: *width as f32,
                y: *height as f32,
            }
        }
        self.input.handle(event);
    }
}
