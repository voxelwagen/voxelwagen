/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{Container, Ui, Widget};
use voxelwagen_common::{UAxis, UVec};

pub fn flow<T>(axis: UAxis, mut w: impl FnMut(&mut Flow) -> T) -> impl Widget<T> {
    move |ui: &mut Ui| {
        let mut f = Flow::new(ui, axis);
        let v = w(&mut f);
        (f.finish(), v)
    }
}

pub struct Flow<'a, 'b> {
    pub ui: &'a mut Ui<'b>,
    direction: UAxis,
    cross_max: f32,
    main_offset: f32,
}

impl<'a, 'b> Flow<'a, 'b> {
    pub fn new(ui: &'a mut Ui<'b>, direction: UAxis) -> Self {
        Self {
            ui,
            cross_max: 0.,
            main_offset: 0.,
            direction,
        }
    }
}
impl Container for Flow<'_, '_> {
    fn add<T>(&mut self, w: impl Widget<T>) -> T {
        let k;
        {
            let (size, r) = w.ui(self.ui);
            k = r;
            match self.direction {
                UAxis::Horizontal => {
                    self.main_offset += size.x;
                    self.ui.bounds.start.x += size.x;
                    self.cross_max = self.cross_max.max(size.y)
                }
                UAxis::Vertial => {
                    self.main_offset += size.y;
                    self.ui.bounds.start.y += size.y;
                    self.cross_max = self.cross_max.max(size.x)
                }
            }
        }
        k
    }
    fn finish(&mut self) -> UVec {
        match self.direction {
            UAxis::Horizontal => {
                self.ui.bounds.start.x -= self.main_offset;
                UVec {
                    x: self.main_offset,
                    y: self.cross_max,
                }
            }
            UAxis::Vertial => {
                self.ui.bounds.start.y -= self.main_offset;
                UVec {
                    x: self.cross_max,
                    y: self.main_offset,
                }
            }
        }
    }
}
