use std::collections::HashSet;
use std::hash::Hash;
use voxelwagen_common::UVec;
use voxelwagen_renderer_common::{InputEvent, Key, MouseButton, PhysicalKey, SmolStr};

pub struct ElementStates<T> {
    pub pressed: HashSet<T>, // TODO maybe use something that is faster for hashset
    pub just_pressed: HashSet<T>,
    pub just_released: HashSet<T>,
}

pub struct InputState {
    pub physical_keys: ElementStates<PhysicalKey>,
    pub semantic_keys: ElementStates<Key<SmolStr>>,
    pub mouse_buttons: ElementStates<MouseButton>,
    pub mouse_location: UVec,
}

impl InputState {
    pub fn handle(&mut self, event: &InputEvent) {
        match event {
            InputEvent::Key {
                physical,
                semantic,
                down,
            } => {
                self.physical_keys.event(*physical, *down);
                self.semantic_keys.event(semantic.clone(), *down);
            }
            InputEvent::MouseButton { down, button, .. } => {
                self.mouse_buttons.event(*button, *down)
            }
            InputEvent::MousePosition { position } => self.mouse_location = *position,
            _ => (),
        }
    }
    pub fn tick(&mut self) {
        self.mouse_buttons.tick();
        self.physical_keys.tick();
        self.semantic_keys.tick();
    }
}
impl Default for InputState {
    fn default() -> Self {
        Self {
            mouse_location: UVec::ZERO,
            physical_keys: ElementStates::default(),
            semantic_keys: ElementStates::default(),
            mouse_buttons: ElementStates::default(),
        }
    }
}
impl<T: Clone + Eq + Hash> ElementStates<T> {
    pub fn event(&mut self, k: T, state: bool) {
        if state {
            self.just_pressed.insert(k.clone());
            self.pressed.insert(k);
        } else {
            self.pressed.remove(&k);
            self.just_released.insert(k);
        }
    }
    pub fn tick(&mut self) {
        self.just_pressed.clear();
        self.just_released.clear();
    }
}

impl<T: Clone + Eq + Hash> Default for ElementStates<T> {
    fn default() -> Self {
        Self {
            just_pressed: HashSet::new(),
            just_released: HashSet::new(),
            pressed: HashSet::new(),
        }
    }
}
