/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use voxelwagen_canvas::font::Font;
use voxelwagen_common::color::Color;

pub struct Theme {
    pub font: Font,
    pub text_color: Color,
}

impl Default for Theme {
    fn default() -> Self {
        Self {
            font: Font::load(include_bytes!("/usr/share/fonts/TTF/Roboto-Regular.ttf")),
            text_color: Color::WHITE,
        }
    }
}
