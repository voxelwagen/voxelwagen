/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(stmt_expr_attributes)]

pub mod animation;
pub mod input;
pub mod layout;
pub mod state;
pub mod theme;
pub mod widgets;

use animation::AnimationState;
use input::InputState;
use std::{
    any::{Any, TypeId},
    ops::Range,
};
use theme::Theme;
use voxelwagen_canvas::{Canvas, TransformSurface};
use voxelwagen_common::{UPos, UVec};

pub struct Ui<'a> {
    pub canvas: TransformSurface<&'a mut Canvas>,
    pub theme: &'a mut Theme,
    pub animation: &'a mut AnimationState,
    pub input: &'a InputState,
    pub bounds: Range<UPos>,
    pub animation_key: u32,
}

pub trait Widget<T> {
    fn ui(self, ui: &mut Ui) -> (UVec, T);
}
pub trait Container {
    fn add<T>(&mut self, w: impl Widget<T>) -> T;
    fn finish(&mut self) -> UVec;
}

impl<U, T: FnOnce(&mut Ui) -> (UVec, U)> Widget<U> for T {
    fn ui(self, ui: &mut Ui) -> (UVec, U) {
        self(ui)
    }
}

impl Ui<'_> {
    fn astate<T: Any + Send + Sync + Default>(&mut self) -> &mut T {
        self.animation
            .state
            .entry((self.animation_key, TypeId::of::<T>()))
            .or_insert_with(|| Box::new(T::default()))
            .as_mut()
            .downcast_mut()
            .unwrap()
    }
    fn push_id(&mut self) {
        self.animation_key += 1; // TODO effient hash?! key every widget?! idk
    }
}
