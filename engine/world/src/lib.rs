/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(trait_alias)]

pub mod cache;
pub mod preload;

use std::ops::Range;

use preload::PreloadedWorld;
use voxelwagen_common::BPos;

pub trait World<V> {
    fn get(&self, p: BPos) -> V;
    fn get_lod(&self, p: BPos, _lod: usize) -> V {
        self.get(p)
    }
    fn get_chunk(&self, range: Range<BPos>, lod: usize) -> PreloadedWorld<V> {
        PreloadedWorld::new(self, range, lod)
    }
}
pub trait WorldMut<V>: World<V> {
    fn set(&mut self, p: BPos, b: V);
}

pub trait SparseWorld<V> = World<Option<V>>;
pub trait SparseWorldMut<V> = WorldMut<Option<V>>;

impl<V, T: Fn(BPos) -> V> World<V> for T {
    fn get(&self, p: BPos) -> V {
        self(p)
    }
}
