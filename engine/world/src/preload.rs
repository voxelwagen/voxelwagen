/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::World;
use rayon::prelude::{IntoParallelIterator, ParallelIterator};
use std::ops::{Index, IndexMut, Range};
use voxelwagen_common::{
    range::{ForEachExt, RangeExt, RangeIter},
    BPos,
};

pub struct PreloadedWorld<B> {
    data: Vec<B>,
    stride: [usize; 2],
    offset: BPos,
    step_bits: i64,
}

impl<B> PreloadedWorld<B> {
    #[inline(always)]
    fn index(&self, pos: BPos) -> usize {
        let pos = (pos - self.offset) >> self.step_bits;
        pos.x as usize + pos.y as usize * self.stride[0] + pos.z as usize * self.stride[1]
    }
    pub fn new_alloc(range: Range<BPos>, step: usize) -> Self {
        PreloadedWorld {
            data: Vec::with_capacity(range.volume() as usize / step.pow(3)),
            offset: range.start,
            stride: [
                (range.end.x - range.start.x) as usize / step,
                (range.end.x - range.start.x) as usize * (range.end.y - range.start.y) as usize
                    / step.pow(2),
            ],
            step_bits: step.ilog2() as i64,
        }
    }
    pub fn new<W: World<B> + ?Sized>(world: &W, range: Range<BPos>, step: usize) -> Self {
        let mut s = Self::new_alloc(range.clone(), step);
        range
            .clone()
            .for_each_step(step, |p| s.data.push(world.get(p)));
        s
    }
}
impl<B: Default> PreloadedWorld<B> {
    pub fn new_empty(range: Range<BPos>, step: usize) -> Self {
        PreloadedWorld {
            data: (0..(range.volume() as usize / step.pow(3)))
                .map(|_| B::default())
                .collect(),
            offset: range.start,
            stride: [
                (range.end.x - range.start.x) as usize / step,
                (range.end.x - range.start.x) as usize * (range.end.y - range.start.y) as usize
                    / step.pow(2),
            ],
            step_bits: step.ilog2() as i64,
        }
    }
}
impl<B: Send> PreloadedWorld<B> {
    pub fn new_par<W: World<B> + Send + Sync>(world: W, range: Range<BPos>, step: usize) -> Self {
        PreloadedWorld {
            data: RangeIter::new(range.clone(), step.try_into().unwrap())
                .collect::<Vec<_>>()
                .into_par_iter()
                .map(|p| world.get(p))
                .collect(),
            offset: range.start,
            stride: [
                (range.end.x - range.start.x) as usize / step,
                (range.end.x - range.start.x) as usize * (range.end.y - range.start.y) as usize
                    / step.pow(2),
            ],
            step_bits: step.ilog2() as i64,
        }
    }
}
impl<B: Clone> World<B> for PreloadedWorld<B> {
    #[inline(always)]
    fn get(&self, pos: BPos) -> B {
        self.data[self.index(pos)].clone()
    }
}
impl<B> Index<BPos> for PreloadedWorld<B> {
    type Output = B;
    #[inline]
    fn index(&self, index: BPos) -> &Self::Output {
        &self.data[self.index(index)]
    }
}
impl<B> IndexMut<BPos> for PreloadedWorld<B> {
    #[inline]
    fn index_mut(&mut self, index: BPos) -> &mut Self::Output {
        let i = self.index(index);
        &mut self.data[i]
    }
}
