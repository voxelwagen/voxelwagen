use crate::{World, WorldMut};
use bincode::{BorrowDecode, Decode, Encode};
use parking_lot::Mutex;
use voxelwagen_common::BPos;

const CACHE_SIZE: usize = 64;
const RESERVED_BPOS: BPos = BPos::splat(i64::MIN);

type Entries<V> = Mutex<([(BPos, Option<V>); CACHE_SIZE], u64)>;

#[derive(Debug)]
pub struct WorldCache<V, Inner> {
    pub inner: Inner,
    pub entries: Entries<V>,
}

impl<Inner: Clone + World<V>, V: Clone> Clone for WorldCache<V, Inner> {
    fn clone(&self) -> Self {
        self.inner.clone().into()
    }
}

impl<V: Clone, I: World<V>> World<V> for WorldCache<V, I> {
    fn get(&self, p: BPos) -> V {
        {
            let mut x = self.entries.lock();
            x.1 += 1;
            for (k, v) in &mut x.0 {
                if *k == p {
                    let v = v.clone().unwrap();
                    return v;
                }
            }
        }
        let b = self.inner.get(p);
        // TODO LRU?
        {
            let mut x = self.entries.lock();
            let slot = x.1 as usize % CACHE_SIZE;
            let (k, v) = &mut x.0[slot];
            *k = p;
            *v = Some(b.clone());
        }
        b
    }
}
impl<V: Clone, I: WorldMut<V>> WorldMut<V> for WorldCache<V, I> {
    fn set(&mut self, p: BPos, b: V) {
        self.inner.set(p, b)
    }
}
impl<V, I: World<V>> From<I> for WorldCache<V, I> {
    fn from(inner: I) -> Self {
        Self {
            inner,
            entries: Mutex::new(([(); CACHE_SIZE].map(|_| (RESERVED_BPOS, None)), 0)),
        }
    }
}
impl<V, I: World<V>> WorldCache<V, I> {}

impl<V, I: WorldMut<V> + Encode> Encode for WorldCache<V, I> {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        self.inner.encode(encoder)
    }
}

impl<'a, V, I: WorldMut<V> + BorrowDecode<'a>> BorrowDecode<'a> for WorldCache<V, I> {
    fn borrow_decode<D: bincode::de::BorrowDecoder<'a>>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        Ok(I::borrow_decode(decoder)?.into())
    }
}
impl<V, I: WorldMut<V> + Decode> Decode for WorldCache<V, I> {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        Ok(I::decode(decoder)?.into())
    }
}

// #[derive(Debug, Clone, Default)]
// pub struct WorldCache<V, Inner> {
//     pub inner: Inner,
//     cache: CHashMap<BPos, V>,
// }

// impl<V: Clone, I: World<V>> World<V> for WorldCache<V, I> {
//     fn get(&self, p: BPos) -> V {
//         if let Some(b) = self.cache.get(&p) {
//             return b.to_owned();
//         }
//         let b = self.inner.get(p);
//         self.cache.insert(p, b.clone());
//         b
//     }
// }
// impl<V: Clone, I: WorldMut<V>> WorldMut<V> for WorldCache<V, I> {
//     fn set(&mut self, p: BPos, b: V) {
//         self.cache.remove(&p);
//         self.inner.set(p, b)
//     }
// }
// impl<V, I: World<V>> From<I> for WorldCache<V, I> {
//     fn from(inner: I) -> Self {
//         Self {
//             inner,
//             cache: CHashMap::new(),
//         }
//     }
// }
// impl<V, I: World<V>> WorldCache<V, I> {}

// impl<V, I: WorldMut<V> + Encode> Encode for WorldCache<V, I> {
//     fn encode<E: bincode::enc::Encoder>(
//         &self,
//         encoder: &mut E,
//     ) -> Result<(), bincode::error::EncodeError> {
//         self.inner.encode(encoder)
//     }
// }

// impl<'a, V, I: WorldMut<V> + BorrowDecode<'a>> BorrowDecode<'a> for WorldCache<V, I> {
//     fn borrow_decode<D: bincode::de::BorrowDecoder<'a>>(
//         decoder: &mut D,
//     ) -> Result<Self, bincode::error::DecodeError> {
//         Ok(I::borrow_decode(decoder)?.into())
//     }
// }
// impl<V, I: WorldMut<V> + Decode> Decode for WorldCache<V, I> {
//     fn decode<D: bincode::de::Decoder>(
//         decoder: &mut D,
//     ) -> Result<Self, bincode::error::DecodeError> {
//         Ok(I::decode(decoder)?.into())
//     }
// }
