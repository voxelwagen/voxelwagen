/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(stmt_expr_attributes)]
pub mod pipeline;
pub mod stagedbuf;

use bytemuck::{Pod, Zeroable};

#[repr(C)]
#[derive(Debug, Clone, Copy, Pod, Zeroable)]
pub struct Vertex {
    pub position: [f32; 2],
    pub color: [f32; 4],
}
