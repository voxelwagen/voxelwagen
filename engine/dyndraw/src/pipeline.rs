/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::Vertex;
use crate::stagedbuf::StagedBuffer;
use log::trace;
use std::mem::size_of;
use wgpu::{
    include_wgsl, BlendState, BufferAddress, BufferUsages, ColorTargetState, ColorWrites,
    CommandEncoder, Device, FragmentState, FrontFace, LoadOp, MultisampleState,
    PipelineCompilationOptions, PipelineLayoutDescriptor, PolygonMode, PrimitiveState,
    PrimitiveTopology, Queue, RenderPass, RenderPipeline, RenderPipelineDescriptor, StoreOp,
    TextureFormat, TextureView, VertexAttribute, VertexBufferLayout, VertexFormat, VertexState,
    VertexStepMode,
};

pub struct DynamicRenderingPipeline {
    vertex: StagedBuffer,
    index: StagedBuffer,
    loaded_index_count: u64,

    pipeline: RenderPipeline,
    // bind_group: BindGroup,
}

impl DynamicRenderingPipeline {
    pub fn new(device: &Device, surface_format: TextureFormat) -> Self {
        let index = StagedBuffer::new(device, BufferUsages::INDEX);
        let vertex = StagedBuffer::new(device, BufferUsages::VERTEX);

        let module = device.create_shader_module(include_wgsl!("render.wgsl"));
        // let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
        //     label: None,
        //     entries: &[],
        // });
        // let bind_group = device.create_bind_group(&BindGroupDescriptor {
        //     label: None,
        //     layout: &bind_group_layout,
        //     entries: &[],
        // });
        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[],
            ..Default::default()
        });
        let pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex: VertexState {
                module: &module,
                compilation_options: PipelineCompilationOptions::default(),
                entry_point: "vertex_stage",
                buffers: &[VertexBufferLayout {
                    array_stride: size_of::<Vertex>() as BufferAddress,
                    step_mode: VertexStepMode::Vertex,
                    attributes: &[
                        VertexAttribute {
                            offset: 0,
                            shader_location: 0,
                            format: VertexFormat::Float32x2,
                        },
                        VertexAttribute {
                            offset: size_of::<[f32; 2]>() as BufferAddress,
                            shader_location: 1,
                            format: VertexFormat::Float32x4,
                        },
                    ],
                }],
            },
            fragment: Some(FragmentState {
                module: &module,
                compilation_options: PipelineCompilationOptions::default(),
                entry_point: "fragment_stage",
                targets: &[Some(ColorTargetState {
                    blend: Some(BlendState::PREMULTIPLIED_ALPHA_BLENDING),
                    format: surface_format,
                    write_mask: ColorWrites::all(),
                })],
            }),
            primitive: PrimitiveState {
                conservative: false,
                topology: PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: FrontFace::Ccw,
                // cull_mode: Some(Face::Back),
                cull_mode: None, // TODO: debug, please cull
                unclipped_depth: false,
                polygon_mode: PolygonMode::Fill,
            },
            depth_stencil: None,
            multisample: MultisampleState::default(),
            multiview: None,
        });

        Self {
            index,
            vertex,
            loaded_index_count: 0,
            // bind_group,
            pipeline,
        }
    }

    pub fn prepare(
        &mut self,
        device: &Device,
        queue: &Queue,
        encoder: &mut CommandEncoder,
        vba: &[Vertex],
        iba: &[u32],
    ) {
        trace!(
            "preparing {} triangles with {} vertecies",
            iba.len() / 3,
            vba.len()
        );
        self.index
            .upload(device, queue, encoder, bytemuck::cast_slice(iba));
        self.vertex
            .upload(device, queue, encoder, bytemuck::cast_slice(vba));
        self.loaded_index_count = iba.len() as u64;
    }

    pub fn render<'a>(&'a mut self, rpass: &mut RenderPass<'a>) {
        rpass.set_pipeline(&self.pipeline);
        rpass.set_vertex_buffer(0, self.vertex.primary.slice(..));
        rpass.set_index_buffer(self.index.primary.slice(..), wgpu::IndexFormat::Uint32);
        rpass.draw_indexed(0..self.loaded_index_count as u32, 0, 0..1);
    }

    pub fn renderpass(&mut self, encoder: &mut CommandEncoder, view: &TextureView) {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: LoadOp::Load,
                    store: StoreOp::Store,
                },
            })],
            depth_stencil_attachment: None,
            occlusion_query_set: None,
            timestamp_writes: None,
        });
        self.render(&mut rpass);
    }
}
