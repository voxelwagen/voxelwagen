use log::debug;
/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use wgpu::{Buffer, BufferDescriptor, BufferUsages, CommandEncoder, Device, Queue};

pub(crate) struct StagedBuffer {
    size: u64,
    usage: BufferUsages,
    pub primary: Buffer,
    staging: Buffer,
}
impl StagedBuffer {
    pub fn new(device: &Device, usage: BufferUsages) -> Self {
        let initial_size = 1024;
        let (staging, primary) = Self::create_buffers(initial_size, device, usage);
        Self {
            primary,
            staging,
            usage,
            size: initial_size,
        }
    }
    fn create_buffers(size: u64, device: &Device, usage: BufferUsages) -> (Buffer, Buffer) {
        let primary = device.create_buffer(&BufferDescriptor {
            label: None,
            size,
            usage: usage | BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });
        let staging = device.create_buffer(&BufferDescriptor {
            label: None,
            size,
            usage: BufferUsages::COPY_SRC | BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });
        (staging, primary)
    }
    pub fn upload(
        &mut self,
        device: &Device,
        queue: &Queue,
        encoder: &mut CommandEncoder,
        data: &[u8],
    ) {
        while data.len() as u64 > self.size {
            debug!(
                "staged buffer needs to grow ({} -> {})",
                self.size,
                self.size * 2
            );
            self.size *= 2;
            let (staging, primary) = Self::create_buffers(self.size, device, self.usage);
            self.staging = staging;
            self.primary = primary;
        }
        queue.write_buffer(&self.staging, 0, data);
        encoder.copy_buffer_to_buffer(&self.staging, 0, &self.primary, 0, data.len() as u64);
    }
}
