#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl Color {
    pub const fn rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self {
            r: r as f32 / 255.,
            g: g as f32 / 255.,
            b: b as f32 / 255.,
            a: a as f32 / 255.,
        }
    }
    #[inline]
    pub fn array(self) -> [f32; 4] {
        [self.r, self.g, self.b, self.a]
    }
    #[inline]
    pub fn mix(f: f32, a: Color, b: Color) -> Color {
        let g = 1. - f;
        Color {
            r: a.r * g + b.r * f,
            g: a.g * g + b.g * f,
            b: a.b * g + b.b * f,
            a: a.a * g + b.a * f,
        }
    }
}

impl Color {
    pub const TRANSPARENT: Self = Self::rgba(0, 0, 0, 0);
    pub const WHITE: Self = Self::rgba(255, 255, 255, 255);
    pub const BLACK: Self = Self::rgba(0, 0, 0, 255);
    pub const RED: Self = Self::rgba(255, 0, 0, 255);
    pub const GREEN: Self = Self::rgba(0, 255, 0, 255);
    pub const BLUE: Self = Self::rgba(0, 0, 255, 255);
    pub const YELLOW: Self = Self::rgba(255, 255, 0, 255);
    pub const CYAN: Self = Self::rgba(0, 255, 255, 255);
    pub const MAGENTA: Self = Self::rgba(255, 0, 255, 255);
    pub const fn grey(v: u8) -> Self {
        Self::rgba(v, v, v, 255)
    }
}
