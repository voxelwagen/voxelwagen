/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use std::{
    fmt::Write,
    time::{Duration, Instant},
};

const MAX_CPS: usize = 32;
const N_ITERS: usize = 60;

#[derive(Default)]
pub struct Profiler {
    active: Option<(Instant, &'static str)>,
    cps: [(Duration, &'static str); MAX_CPS],
    ncp: usize,
    iters: usize,
    pub last_stat: String,
}
impl Profiler {
    pub fn finish(&mut self) {
        if let Some((ins, label)) = self.active.take() {
            self.cps[self.ncp].0 += ins.elapsed();
            self.cps[self.ncp].1 = label;
            self.ncp += 1;
        }
    }
    pub fn reset(&mut self) {
        if self.iters == N_ITERS {
            self.last_stat = String::new();
            for i in 0..self.ncp {
                writeln!(
                    self.last_stat,
                    "{}: {:?}",
                    self.cps[i].1,
                    self.cps[i].0 / self.iters as u32
                )
                .unwrap();
                self.cps[i] = Default::default();
            }
            self.iters = 0;
        }
        self.ncp = 0;
        self.iters += 1;
    }
    pub fn section(&mut self, label: &'static str) {
        self.finish();
        self.active = Some((Instant::now(), label))
    }
}
