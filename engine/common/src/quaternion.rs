/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{matrix::Mat3, vector::Vec3, EMat, EVec};
use std::ops::Mul;

#[derive(Debug, Clone, Copy)]
pub struct Quaternion {
    pub a: f32,
    pub b: f32,
    pub c: f32,
    pub d: f32,
}

// rotate :: Vec3 -> Float -> Quaternion -> Quaternion
// as_matrix :: Quaternion -> Matrix3x3

impl Quaternion {
    pub const IDENTITY: Quaternion = Quaternion {
        a: 1.,
        b: 0.,
        c: 0.,
        d: 0.,
    };

    pub fn rotate(self, axis: EVec, angle: f32) -> Self {
        let ah = angle / 2.;
        let q = Quaternion {
            a: ah.cos(),
            b: axis.x * ah.sin(),
            c: axis.y * ah.sin(),
            d: axis.z * ah.sin(),
        };
        q * self * q.inverse()
    }
    /// Straight from wikipedia <https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Quaternion-derived_rotation_matrix>
    pub fn as_matrix(self) -> EMat {
        Mat3 {
            x: Vec3 {
                x: 1. - 2. * (self.c.powi(2) + self.d.powi(2)),
                y: 2. * (self.b * self.c - self.d * self.a),
                z: 2. * (self.b * self.d + self.c * self.a),
            },
            y: Vec3 {
                x: 2. * (self.b * self.c + self.d * self.a),
                y: 1. - 2. * (self.b.powi(2) + self.d.powi(2)),
                z: 2. * (self.c * self.d - self.b * self.a),
            },
            z: Vec3 {
                x: 2. * (self.b * self.d - self.c * self.a),
                y: 2. * (self.c * self.d + self.b * self.a),
                z: 1. - 2. * (self.b.powi(2) + self.c.powi(2)),
            },
        }
    }

    pub fn inverse(self) -> Quaternion {
        Quaternion {
            a: self.a,
            b: -self.b,
            c: -self.c,
            d: -self.d,
        }
    }
}

impl Mul for Quaternion {
    type Output = Quaternion;
    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            a: self.a * rhs.a - self.b * rhs.b - self.c * rhs.c - self.d * rhs.d,
            b: self.a * rhs.b + self.b * rhs.a + self.c + rhs.d - self.d * rhs.c,
            c: self.a * rhs.c - self.b * rhs.d + self.c * rhs.a + self.d * rhs.b,
            d: self.a * rhs.d + self.b * rhs.c - self.c * rhs.b + self.d * rhs.a,
        }
    }
}
