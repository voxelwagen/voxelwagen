/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{vector::Vec3, BPos, EPos, UVec};
use std::{iter::Step, ops::Range};

pub trait RangeExt<T, U> {
    fn overlaps(&self, rhs: &Self) -> bool;
    fn includes(&self, rhs: &Self) -> bool;
    fn has(&self, rhs: &T) -> bool;
    fn expand(&self, amount: U) -> Self;
    fn sort(&self) -> Self;
    fn size(&self) -> T;
    fn center(&self) -> T;
    fn volume(&self) -> U;
    fn corners(&self) -> [T; 8];
    fn clamp(&self, rhs: T) -> T;
}
impl RangeExt<BPos, i64> for Range<BPos> {
    fn size(&self) -> BPos {
        self.end - self.start
    }
    fn clamp(&self, rhs: BPos) -> BPos {
        BPos {
            x: rhs.x.clamp(self.start.x, self.end.x),
            y: rhs.y.clamp(self.start.y, self.end.y),
            z: rhs.z.clamp(self.start.z, self.end.z),
        }
    }
    fn volume(&self) -> i64 {
        (self.end - self.start).product()
    }
    fn has(&self, _rhs: &BPos) -> bool {
        unimplemented!()
    }
    fn expand(&self, amount: i64) -> Self {
        Self {
            start: self.start - BPos::ONE * amount,
            end: self.end + BPos::ONE * amount,
        }
    }
    fn overlaps(&self, rhs: &Self) -> bool {
        self.end.x > rhs.start.x
            && rhs.end.x > self.start.x
            && self.end.y > rhs.start.y
            && rhs.end.y > self.start.y
            && self.end.z > rhs.start.z
            && rhs.end.z > self.start.z
    }
    fn includes(&self, _rhs: &Self) -> bool {
        todo!()
    }
    fn center(&self) -> BPos {
        (self.start + self.end) / 2
    }
    fn corners(&self) -> [BPos; 8]
    where
        Self: Sized,
    {
        #[rustfmt::skip] [
            BPos { x: self.start.x, y: self.start.y, z: self.start.z },
            BPos { x: self.start.x, y: self.start.y, z: self.end.z },
            BPos { x: self.start.x, y: self.end.y, z: self.start.z },
            BPos { x: self.start.x, y: self.end.y, z: self.end.z },
            BPos { x: self.end.x, y: self.start.y, z: self.start.z },
            BPos { x: self.end.x, y: self.start.y, z: self.end.z },
            BPos { x: self.end.x, y: self.end.y, z: self.start.z },
            BPos { x: self.end.x, y: self.end.y, z: self.end.z },
        ]
    }

    fn sort(&self) -> Self {
        Range {
            start: self.start.component_min(self.end),
            end: self.start.component_max(self.end),
        }
    }
}

impl RangeExt<EPos, f32> for Range<EPos> {
    fn size(&self) -> EPos {
        self.end - self.start
    }
    fn has(&self, _rhs: &EPos) -> bool {
        unimplemented!()
    }
    fn center(&self) -> EPos {
        (self.start + self.end) / 2.
    }
    fn clamp(&self, rhs: EPos) -> EPos {
        EPos {
            x: rhs.x.clamp(self.start.x, self.end.x),
            y: rhs.y.clamp(self.start.y, self.end.y),
            z: rhs.z.clamp(self.start.z, self.end.z),
        }
    }
    fn includes(&self, _rhs: &Self) -> bool {
        unimplemented!()
    }
    fn expand(&self, amount: f32) -> Self {
        Self {
            start: self.start - EPos::ONE * amount,
            end: self.end + EPos::ONE * amount,
        }
    }
    fn overlaps(&self, rhs: &Self) -> bool {
        rhs.start.x < self.end.x
            && self.start.x < rhs.end.x
            && rhs.start.y < self.end.y
            && self.start.y < rhs.end.y
            && rhs.start.z < self.end.z
            && self.start.z < rhs.end.z
    }
    fn volume(&self) -> f32 {
        (self.end - self.start).product()
    }
    fn corners(&self) -> [EPos; 8]
    where
        Self: Sized,
    {
        #[rustfmt::skip] [
            EPos { x: self.start.x, y: self.start.y, z: self.start.z },
            EPos { x: self.start.x, y: self.start.y, z: self.end.z },
            EPos { x: self.start.x, y: self.end.y, z: self.start.z },
            EPos { x: self.start.x, y: self.end.y, z: self.end.z },
            EPos { x: self.end.x, y: self.start.y, z: self.start.z },
            EPos { x: self.end.x, y: self.start.y, z: self.end.z },
            EPos { x: self.end.x, y: self.end.y, z: self.start.z },
            EPos { x: self.end.x, y: self.end.y, z: self.end.z },
        ]
    }

    fn sort(&self) -> Self {
        todo!()
    }
}

impl RangeExt<UVec, f32> for Range<UVec> {
    fn size(&self) -> UVec {
        self.end - self.start
    }
    fn center(&self) -> UVec {
        (self.start + self.end) / 2.
    }
    fn has(&self, rhs: &UVec) -> bool {
        self.start.x <= rhs.x && self.end.x > rhs.x && self.start.y <= rhs.y && self.end.y > rhs.y
    }
    fn clamp(&self, rhs: UVec) -> UVec {
        UVec {
            x: rhs.x.clamp(self.start.x, self.end.x),
            y: rhs.y.clamp(self.start.y, self.end.y),
        }
    }
    fn volume(&self) -> f32 {
        (self.end - self.start).product()
    }
    fn expand(&self, amount: f32) -> Self {
        Self {
            start: self.start - UVec::ONE * amount,
            end: self.end + UVec::ONE * amount,
        }
    }
    fn overlaps(&self, rhs: &Self) -> bool {
        self.end.x > rhs.start.x
            && rhs.end.x > self.start.x
            && self.end.y > rhs.start.y
            && rhs.end.y > self.start.y
    }
    fn includes(&self, _rhs: &Self) -> bool {
        todo!()
    }
    fn corners(&self) -> [UVec; 8]
    where
        Self: Sized,
    {
        todo!()
    }

    fn sort(&self) -> Self {
        todo!()
    }
}

pub trait ForEachExt<T> {
    fn for_each(self, f: impl FnMut(T));
    fn for_each_step(self, step: usize, f: impl FnMut(T));
}
impl<T: Copy + Step> ForEachExt<Vec3<T>> for Range<Vec3<T>> {
    #[inline]
    fn for_each(self, mut f: impl FnMut(Vec3<T>)) {
        let Range { end: e, start: s } = self;
        for z in s.z..e.z {
            for y in s.y..e.y {
                for x in s.x..e.x {
                    f(Vec3 { x, y, z })
                }
            }
        }
    }
    #[inline]
    fn for_each_step(self, step: usize, mut f: impl FnMut(Vec3<T>)) {
        let Range { end: e, start: s } = self;
        for z in (s.z..e.z).step_by(step) {
            for y in (s.y..e.y).step_by(step) {
                for x in (s.x..e.x).step_by(step) {
                    f(Vec3 { x, y, z })
                }
            }
        }
    }
}

pub struct RangeIter<V> {
    r: Range<Vec3<V>>,
    c: Vec3<V>,
    step: i64,
    fin: bool,
}
impl RangeIter<i64> {
    pub fn new(r: Range<Vec3<i64>>, step: i64) -> Self {
        Self {
            step,
            c: r.start,
            r,
            fin: false,
        }
    }
}
impl Iterator for RangeIter<i64> {
    type Item = Vec3<i64>;
    fn next(&mut self) -> Option<Self::Item> {
        let Self {
            c,
            step,
            fin,
            r: Range { start, end },
        } = self;
        if *fin {
            return None;
        }

        let ret = *c;

        c.x += *step;
        if c.x >= end.x {
            c.x = start.x;
            c.y += *step;
            if c.y >= end.y {
                c.y = start.y;
                c.z += *step;
                if c.z >= end.z {
                    *fin = true;
                }
            }
        }

        Some(ret)
    }
}

pub trait DistanceRangeExt {
    fn distance(&self, p: EPos) -> f32;
}
impl DistanceRangeExt for Range<BPos> {
    fn distance(&self, p: EPos) -> f32 {
        let pc = EPos {
            x: p.x.clamp(self.start.x as f32, self.end.x as f32),
            y: p.y.clamp(self.start.y as f32, self.end.y as f32),
            z: p.z.clamp(self.start.z as f32, self.end.z as f32),
        };
        pc.distance(p)
    }
}
