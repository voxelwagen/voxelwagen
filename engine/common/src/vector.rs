/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{BPos, EPos};
use std::{
    fmt::{Debug, Display},
    hash::Hash,
    ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Shr, ShrAssign, Sub, SubAssign},
};

macro_rules! impl_vec {
    ($t:tt, $ncomp:expr, [$($comp:ident),*]) => {
        #[derive(Clone, Copy, PartialEq, PartialOrd, Default, bincode::Encode, bincode::Decode)]
        pub struct $t<T> { $(pub $comp: T),+ }

        impl_vec_lift_bop!($t, [$($comp),*], (Add, AddAssign), (add, add_assign), (+,+=));
        impl_vec_lift_bop!($t, [$($comp),*], (Sub, SubAssign), (sub, sub_assign), (-,-=));
        impl_vec_lift_bop!($t, [$($comp),*], (Mul, MulAssign), (mul, mul_assign), (*,*=));
        impl_vec_lift_bop!($t, [$($comp),*], (Div, DivAssign), (div, div_assign), (/,/=));

        impl_vec_lift_asym_bop!($t, [$($comp),*], (Mul, MulAssign), (mul, mul_assign), (*,*=));
        impl_vec_lift_asym_bop!($t, [$($comp),*], (Div, DivAssign), (div, div_assign), (/,/=));
        impl_vec_lift_asym_bop!($t, [$($comp),*], (Shr, ShrAssign), (shr, shr_assign), (>>,>>=));

        impl_vec_const!($t<f32>, [$($comp),*], 0., 1.);
        impl_vec_const!($t<i64>, [$($comp),*], 0, 1);
        impl_vec_const!($t<usize>, [$($comp),*], 0, 1);

        impl_vec_lift_ufn!($t<f64>, [$($comp),*], sin);
        impl_vec_lift_ufn!($t<f64>, [$($comp),*], fract);
        impl_vec_lift_ufn!($t<f64>, [$($comp),*], floor);
        impl_vec_lift_ufn!($t<f32>, [$($comp),*], round);
        impl_vec_lift_ufn!($t<f32>, [$($comp),*], fract);
        impl_vec_lift_ufn!($t<f32>, [$($comp),*], floor);
        impl_vec_lift_ufn!($t<f32>, [$($comp),*], signum);
        impl_vec_lift_ufn!($t<f32>, [$($comp),*], abs);
        impl_vec_lift_ufn!($t<f32>, [$($comp),*], sqrt);

        impl<T: Add<Output = T>> $t<T> { #[inline] pub fn sum(self) -> T { strip_plus!($(+ self.$comp)+) } }
        impl<T: Mul<Output = T>> $t<T> { #[inline] pub fn product(self) -> T { strip_star!($(* self.$comp)+) } }
        impl<T: Copy> $t<T> { #[inline] pub const fn splat(v: T) -> Self { Self { $($comp: v),+ } } }
        // impl<T> $t<T> { #[inline] pub fn map<F: FnMut(T) -> U, U>(self, mut f: F) -> $t<U> { Self { $($comp: f(self.$comp)),* } } }
        impl<T> $t<T> { #[inline] pub fn array(self) -> [T; $ncomp] { [$(self.$comp),+] } }
        impl<T> $t<T> { #[inline] pub fn from_array(a: [T; $ncomp]) -> Self { let [$($comp),+] = a; Self { $($comp),+ } } }
        impl<T> From<[T; $ncomp]> for $t<T> { #[inline] fn from(v: [T; $ncomp]) -> Self { Self::from_array(v) } }
        impl<T> From<$t<T>> for [T; $ncomp] { #[inline] fn from(v: $t<T>) -> [T; $ncomp] { v.array() } }
        impl<T: Ord> $t<T> { #[inline] pub fn component_max(self, rhs: Self) -> Self { Self { $($comp: self.$comp.max(rhs.$comp)),+ } } }
        impl<T: Ord> $t<T> { #[inline] pub fn component_min(self, rhs: Self) -> Self { Self { $($comp: self.$comp.min(rhs.$comp)),+ } } }

    }
}

macro_rules! impl_vec_lift_ufn {
    ($t:ty, [$($comp:ident),*], $f:ident) => {
        impl $t { #[inline] pub fn $f(self) -> Self { Self { $($comp: self.$comp.$f()),+ } } }
    };
}

macro_rules! impl_vec_lift_bop {
    ($t:tt, [$($comp:ident),*], ($trait:ident, $traita:ident), ($fun:ident, $funa:ident), ($op:tt, $opa:tt)) => {
        impl<T: $trait<Output = T>> $trait for $t<T> {
            type Output = Self;
            #[inline]
            fn $fun(self, rhs: Self) -> Self::Output {
                Self { $($comp: self.$comp $op rhs.$comp),+ }
            }
        }
        impl<T: $traita> $traita for $t<T> {
            #[inline]
            fn $funa(&mut self, rhs: Self) {
                $(self.$comp $opa rhs.$comp);+
            }
        }
    };
}
macro_rules! impl_vec_lift_asym_bop {
    ($t:tt, [$($comp:ident),*], ($trait:ident, $traita:ident), ($fun:ident, $funa:ident), ($op:tt, $opa:tt)) => {
        impl<T: $trait<T, Output = T> + Copy> $trait<T> for $t<T> {
            type Output = Self;
            #[inline]
            fn $fun(self, rhs: T) -> Self::Output {
                Self { $($comp: self.$comp $op rhs),+ }
            }
        }
        impl<T: $traita + Copy> $traita<T> for $t<T> {
            #[inline]
            fn $funa(&mut self, rhs: T) {
                $(self.$comp $opa rhs);+
            }
        }
    };
}
macro_rules! impl_vec_const {
    ($t:ty, [$($comp:ident),*], $zero:expr, $one:expr) => {
        impl $t {
            pub const ZERO: Self = Self { $($comp: $zero),+ };
            pub const ONE: Self = Self { $($comp: $one),+ };
        }
    };
}
macro_rules! strip_plus { {+ $($rest:tt)* } => { $($rest)* } }
macro_rules! strip_star { {* $($rest:tt)* } => { $($rest)* } }

impl<T: Debug> Debug for Vec2<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("({:.02?} {:.02?})", self.x, self.y))
    }
}
impl<T: Debug> Debug for Vec3<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "({:.02?} {:.02?} {:.02?})",
            self.x, self.y, self.z
        ))
    }
}
impl<T: Debug> Debug for Vec4<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "({:.02?} {:.02?} {:.02?} {:.02?})",
            self.x, self.y, self.z, self.w
        ))
    }
}

impl<T: Display> Display for Vec3<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "({:.02} {:.02} {:.02})",
            self.x, self.y, self.z
        ))
    }
}

impl Vec3<f32> {
    #[inline]
    pub fn bpos(self) -> BPos {
        BPos {
            x: self.x.floor() as i64,
            y: self.y.floor() as i64,
            z: self.z.floor() as i64,
        }
    }
}
impl BPos {
    #[inline]
    pub fn epos(self) -> EPos {
        EPos {
            x: self.x as f32,
            y: self.y as f32,
            z: self.z as f32,
        }
    }
    #[inline]
    pub fn f64(self) -> Vec3<f64> {
        Vec3 {
            x: self.x as f64,
            y: self.y as f64,
            z: self.z as f64,
        }
    }
}

impl Hash for EPos {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        ((self.x * 4069.) as i64).hash(state);
        ((self.y * 4069.) as i64).hash(state);
        ((self.z * 4069.) as i64).hash(state);
    }
}
impl Hash for BPos {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        self.y.hash(state);
        self.z.hash(state);
    }
}

impl Eq for EPos {
    fn assert_receiver_is_total_eq(&self) {}
}
impl Eq for BPos {
    fn assert_receiver_is_total_eq(&self) {}
}

impl Ord for BPos {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

mod length {
    use std::ops::{Add, Div, Mul, Sub};

    #[rustfmt::skip] pub trait Sqrt { fn sqrt(self) -> Self; const ZERO: Self; }
    #[rustfmt::skip] impl Sqrt for f32 { #[inline(always)] fn sqrt(self) -> Self { self.sqrt() } const ZERO: Self = 0.; }
    #[rustfmt::skip] impl Sqrt for f64 { #[inline(always)] fn sqrt(self) -> Self { self.sqrt() } const ZERO: Self = 0.;}
    #[rustfmt::skip] impl Sqrt for i64 { #[inline(always)] fn sqrt(self) -> Self { unimplemented!() } const ZERO: Self = 0;}

    impl<
            T: Mul<Output = T>
                + Add<Output = T>
                + Sub<Output = T>
                + Copy
                + Div<T, Output = T>
                + Sqrt
                + PartialEq,
        > super::Vec3<T>
    {
        #[inline]
        pub fn sq_length(self) -> T {
            self.x * self.x + self.y * self.y + self.z * self.z
        }
        #[inline]
        pub fn length(self) -> T {
            self.sq_length().sqrt()
        }
        #[inline]
        pub fn normalize(self) -> Self {
            self / self.length()
        }
        #[inline]
        pub fn normalize_or_zero(self) -> Self {
            let l = self.length();
            if l == T::ZERO {
                super::Vec3::splat(T::ZERO)
            } else {
                self / l
            }
        }
        #[inline]
        pub fn distance(self, rhs: Self) -> T {
            (self - rhs).length()
        }
    }
    impl<
            T: Mul<Output = T>
                + Add<Output = T>
                + Sub<Output = T>
                + Copy
                + Div<T, Output = T>
                + Sqrt
                + PartialEq,
        > super::Vec2<T>
    {
        #[inline]
        pub fn length(self) -> T {
            (self.x * self.x + self.y * self.y).sqrt()
        }
        #[inline]
        pub fn normalize(self) -> Self {
            self / self.length()
        }
        #[inline]
        pub fn normalize_or_zero(self) -> Self {
            let l = self.length();
            if l == T::ZERO {
                super::Vec2::splat(T::ZERO)
            } else {
                self / l
            }
        }
        #[inline]
        pub fn distance(self, rhs: Self) -> T {
            (self - rhs).length()
        }
    }
}

impl Vec3<i64> {
    #[inline]
    pub fn rem_euclid(self, k: i64) -> Self {
        Self {
            x: self.x.rem_euclid(k),
            y: self.y.rem_euclid(k),
            z: self.z.rem_euclid(k),
        }
    }
    #[inline]
    pub fn div_euclid(self, k: i64) -> Self {
        Self {
            x: self.x.div_euclid(k),
            y: self.y.div_euclid(k),
            z: self.z.div_euclid(k),
        }
    }
}
impl Vec3<f32> {
    pub fn min(self, rhs: Self) -> Self {
        Self {
            x: self.x.min(rhs.x),
            y: self.y.min(rhs.y),
            z: self.z.min(rhs.z),
        }
    }
    pub fn max(self, rhs: Self) -> Self {
        Self {
            x: self.x.max(rhs.x),
            y: self.y.max(rhs.y),
            z: self.z.max(rhs.z),
        }
    }
}

#[rustfmt::skip] pub mod pack {
    use super::{Vec2, Vec3, Vec4};
    pub const fn v2<T>(x: T, y: T) -> Vec2<T> { Vec2 { x, y } }
    pub const fn v3<T>(x: T, y: T, z: T) -> Vec3<T> { Vec3 { x, y, z } }
    pub const fn v4<T>(x: T, y: T, z: T, w: T) -> Vec4<T> { Vec4 { x, y, z, w } }
    impl<T> Vec3<T> { pub fn xy(self) -> Vec2<T> { v2(self.x, self.y) } }
    impl<T> Vec3<T> { pub fn xz(self) -> Vec2<T> { v2(self.x, self.z) } }
    impl<T> Vec3<T> { pub fn yz(self) -> Vec2<T> { v2(self.y, self.z) } }
}

impl_vec!(Vec2, 2, [x, y]);
impl_vec!(Vec3, 3, [x, y, z]);
impl_vec!(Vec4, 4, [x, y, z, w]);
