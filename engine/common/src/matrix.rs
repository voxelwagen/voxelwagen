/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{
    vector::{Vec2, Vec3, Vec4},
    EVec,
};
use std::{
    fmt::Debug,
    iter::Sum,
    ops::{Add, Mul},
};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Mat2<T> {
    pub x: Vec2<T>,
    pub y: Vec2<T>,
}

impl<T: Mul<Output = T> + Add<Output = T> + Copy + Sum> Mul<Vec2<T>> for Mat2<T> {
    type Output = Vec2<T>;
    #[inline]
    fn mul(self, rhs: Vec2<T>) -> Self::Output {
        Vec2 {
            x: (self.x * rhs).sum(),
            y: (self.y * rhs).sum(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Mat3<T> {
    pub x: Vec3<T>,
    pub y: Vec3<T>,
    pub z: Vec3<T>,
}

impl<T: Copy> From<[[T; 3]; 3]> for Mat3<T> {
    #[inline]
    fn from(a: [[T; 3]; 3]) -> Self {
        Self {
            x: a[0].into(),
            y: a[1].into(),
            z: a[2].into(),
        }
    }
}

impl<T: Mul<Output = T> + Add<Output = T> + Copy + Sum> Mul<Vec3<T>> for Mat3<T> {
    type Output = Vec3<T>;
    #[inline]
    fn mul(self, rhs: Vec3<T>) -> Self::Output {
        Vec3 {
            x: (self.x * rhs).sum(),
            y: (self.y * rhs).sum(),
            z: (self.z * rhs).sum(),
        }
    }
}

impl Mat3<f32> {
    pub const UNIT: Mat3<f32> = Mat3 {
        x: EVec {
            x: 1.,
            y: 0.,
            z: 0.,
        },
        y: EVec {
            x: 0.,
            y: 1.,
            z: 0.,
        },
        z: EVec {
            x: 0.,
            y: 0.,
            z: 1.,
        },
    };

    pub fn scale(f: f32) -> Mat3<f32> {
        Mat3 {
            x: Vec3 { x: f, y: 0., z: 0. },
            y: Vec3 { x: 0., y: f, z: 0. },
            z: Vec3 { x: 0., y: 0., z: f },
        }
    }

    pub fn rotation_x(a: f32) -> Mat3<f32> {
        let (s, c) = (a.sin(), a.cos());
        Mat3 {
            x: EVec {
                x: 1.,
                y: 0.,
                z: 0.,
            },
            y: EVec { x: 0., y: c, z: -s },
            z: EVec { x: 0., y: s, z: c },
        }
    }
    pub fn rotation_y(a: f32) -> Mat3<f32> {
        let (s, c) = (a.sin(), a.cos());
        Mat3 {
            x: EVec { x: c, y: 0., z: s },
            y: EVec {
                x: 0.,
                y: 1.,
                z: 0.,
            },
            z: EVec { x: -s, y: 0., z: c },
        }
    }
    pub fn rotation_z(a: f32) -> Mat3<f32> {
        let (s, c) = (a.sin(), a.cos());
        Mat3 {
            x: EVec { x: c, y: -s, z: 0. },
            y: EVec { x: s, y: c, z: 0. },
            z: EVec {
                x: 0.,
                y: 0.,
                z: 1.,
            },
        }
    }
}

impl<T> Mat3<T> {
    pub fn array(self) -> [[T; 3]; 3] {
        [self.x.array(), self.y.array(), self.z.array()]
    }
    pub fn transpose(self) -> Self {
        Self {
            x: Vec3 {
                x: self.x.x,
                y: self.y.x,
                z: self.z.x,
            },
            y: Vec3 {
                x: self.x.y,
                y: self.y.y,
                z: self.z.y,
            },
            z: Vec3 {
                x: self.x.z,
                y: self.y.z,
                z: self.z.z,
            },
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Mat4<T> {
    pub x: Vec4<T>,
    pub y: Vec4<T>,
    pub z: Vec4<T>,
    pub w: Vec4<T>,
}

impl<T: Copy> From<[[T; 4]; 4]> for Mat4<T> {
    #[inline]
    fn from(a: [[T; 4]; 4]) -> Self {
        Self {
            x: a[0].into(),
            y: a[1].into(),
            z: a[2].into(),
            w: a[3].into(),
        }
    }
}

impl<T: Mul<Output = T> + Add<Output = T> + Copy + Sum + Debug> Mul<Vec4<T>> for Mat4<T> {
    type Output = Vec4<T>;
    #[inline]
    fn mul(self, rhs: Vec4<T>) -> Self::Output {
        Vec4 {
            x: (self.x * rhs).sum(),
            y: (self.y * rhs).sum(),
            z: (self.z * rhs).sum(),
            w: (self.w * rhs).sum(),
        }
    }
}

impl<T: Mul<Output = T> + Add<Output = T> + Copy + Sum> Mul<Mat4<T>> for Mat4<T> {
    type Output = Mat4<T>;
    fn mul(self, rhs: Mat4<T>) -> Self::Output {
        let rt = rhs.transpose();
        Mat4 {
            x: Vec4 {
                x: (self.x * rt.x).sum(),
                y: (self.x * rt.y).sum(),
                z: (self.x * rt.z).sum(),
                w: (self.x * rt.w).sum(),
            },
            y: Vec4 {
                x: (self.y * rt.x).sum(),
                y: (self.y * rt.y).sum(),
                z: (self.y * rt.z).sum(),
                w: (self.y * rt.w).sum(),
            },
            z: Vec4 {
                x: (self.z * rt.x).sum(),
                y: (self.z * rt.y).sum(),
                z: (self.z * rt.z).sum(),
                w: (self.z * rt.w).sum(),
            },
            w: Vec4 {
                x: (self.w * rt.x).sum(),
                y: (self.w * rt.y).sum(),
                z: (self.w * rt.z).sum(),
                w: (self.w * rt.w).sum(),
            },
        }
    }
}
impl<T: Mul<Output = T> + Add<Output = T> + Copy + Sum> Mul<Mat3<T>> for Mat3<T> {
    type Output = Mat3<T>;
    fn mul(self, rhs: Mat3<T>) -> Self::Output {
        let rt = rhs.transpose();
        Mat3 {
            x: Vec3 {
                x: (self.x * rt.x).sum(),
                y: (self.x * rt.y).sum(),
                z: (self.x * rt.z).sum(),
            },
            y: Vec3 {
                x: (self.y * rt.x).sum(),
                y: (self.y * rt.y).sum(),
                z: (self.y * rt.z).sum(),
            },
            z: Vec3 {
                x: (self.z * rt.x).sum(),
                y: (self.z * rt.y).sum(),
                z: (self.z * rt.z).sum(),
            },
        }
    }
}

impl Mat4<f32> {
    pub const UNIT: Mat4<f32> = Mat4 {
        x: Vec4 {
            x: 1.,
            y: 0.,
            z: 0.,
            w: 0.,
        },
        y: Vec4 {
            x: 0.,
            y: 1.,
            z: 0.,
            w: 0.,
        },
        z: Vec4 {
            x: 0.,
            y: 0.,
            z: 1.,
            w: 0.,
        },
        w: Vec4 {
            x: 0.,
            y: 0.,
            z: 0.,
            w: 1.,
        },
    };
}

impl<T> Mat4<T> {
    pub fn transpose(self) -> Self {
        Self {
            x: Vec4 {
                x: self.x.x,
                y: self.y.x,
                z: self.z.x,
                w: self.w.x,
            },
            y: Vec4 {
                x: self.x.y,
                y: self.y.y,
                z: self.z.y,
                w: self.w.y,
            },
            z: Vec4 {
                x: self.x.z,
                y: self.y.z,
                z: self.z.z,
                w: self.w.z,
            },
            w: Vec4 {
                x: self.x.w,
                y: self.y.w,
                z: self.z.w,
                w: self.w.w,
            },
        }
    }
}

impl<T> From<Mat4<T>> for [[T; 4]; 4] {
    #[inline]
    fn from(v: Mat4<T>) -> [[T; 4]; 4] {
        [v.x.array(), v.y.array(), v.z.array(), v.w.array()]
    }
}

impl Mat4<f32> {
    pub fn from_transform_offset(offset: Vec3<f32>, transform: Mat3<f32>) -> Mat4<f32> {
        Mat4 {
            x: Vec4 {
                x: transform.x.x,
                y: transform.y.x,
                z: transform.z.x,
                w: offset.x,
            },
            y: Vec4 {
                x: transform.x.y,
                y: transform.y.y,
                z: transform.z.y,
                w: offset.y,
            },
            z: Vec4 {
                x: transform.x.z,
                y: transform.y.z,
                z: transform.z.z,
                w: offset.z,
            },
            w: Vec4 {
                x: 0.,
                y: 0.,
                z: 0.,
                w: 1.,
            },
        }
    }
}

#[rustfmt::skip] pub mod pack {
    use super::{Mat2, Mat3};
    use crate::vector::pack::{v2, v3};
    pub const fn mat2<T>(a: T, b: T, c: T, d: T) -> Mat2<T> { Mat2 { x: v2(a, b), y: v2(c, d) } }
    #[allow(clippy::too_many_arguments)]
    pub const fn mat3<T>(a: T, b: T, c: T, d: T, e: T, f: T, g: T, h: T, i: T) -> Mat3<T> { Mat3 { x: v3(a, b,c), y: v3(d, e,f), z: v3(g,h,i) } }
}
