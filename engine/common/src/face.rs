/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{vector::Vec3, BPos, EPos};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct BlockFace {
    pub axis: Axis,
    /// true => pos, false => neg
    pub sign: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Axis {
    X,
    Y,
    Z,
}

impl Axis {
    pub const LIST: [Axis; 3] = [Axis::X, Axis::Y, Axis::Z];
    #[inline]
    pub fn dir(self) -> BPos {
        match self {
            Axis::X => BPos { x: 1, y: 0, z: 0 },
            Axis::Y => BPos { x: 0, y: 1, z: 0 },
            Axis::Z => BPos { x: 0, y: 0, z: 1 },
        }
    }
    #[inline]
    #[rustfmt::skip]
    pub fn edir(self) -> EPos {
        match self {
            Axis::X => EPos { x: 1., y: 0., z: 0. },
            Axis::Y => EPos { x: 0., y: 1., z: 0. },
            Axis::Z => EPos { x: 0., y: 0., z: 1. },
        }
    }
    #[inline]
    pub fn unit(self, v: i64) -> Vec3<i64> {
        match self {
            Axis::X => Vec3 { x: v, y: 0, z: 0 },
            Axis::Y => Vec3 { x: 0, y: v, z: 0 },
            Axis::Z => Vec3 { x: 0, y: 0, z: v },
        }
    }
    #[inline]
    pub fn extract_from<T: Copy>(self, v: Vec3<T>) -> T {
        match self {
            Axis::X => v.x,
            Axis::Y => v.y,
            Axis::Z => v.z,
        }
    }
    #[inline]
    pub fn extract_from_mut<T>(self, v: &mut Vec3<T>) -> &mut T {
        match self {
            Axis::X => &mut v.x,
            Axis::Y => &mut v.y,
            Axis::Z => &mut v.z,
        }
    }
}

impl BlockFace {
    #[rustfmt::skip]
    pub const LIST: [BlockFace; 6] = [
        BlockFace { axis: Axis::X, sign: false },
        BlockFace { axis: Axis::X, sign: true },
        BlockFace { axis: Axis::Y, sign: false },
        BlockFace { axis: Axis::Y, sign: true },
        BlockFace { axis: Axis::Z, sign: false },
        BlockFace { axis: Axis::Z, sign: true },
    ];
    #[inline]
    pub fn dir(self) -> BPos {
        self.axis.unit(self.sign())
    }
    #[inline]
    pub fn sign(self) -> i64 {
        if self.sign {
            1
        } else {
            -1
        }
    }
    #[inline]
    pub fn opposite(mut self) -> Self {
        self.sign = !self.sign;
        self
    }
    #[inline]
    pub fn index(self) -> usize {
        match (self.axis, self.sign) {
            (Axis::X, false) => 0,
            (Axis::X, true) => 1,
            (Axis::Y, false) => 2,
            (Axis::Y, true) => 3,
            (Axis::Z, false) => 4,
            (Axis::Z, true) => 5,
        }
    }
}
