/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(stmt_expr_attributes)]
#![feature(step_trait)]
#![feature(const_fn_floating_point_arithmetic)]

use bincode::{Decode, Encode};
use matrix::Mat3;
use model::Model;
use vector::{Vec2, Vec3};

pub use voxelwagen_compat as compat;

pub mod color;
pub mod face;
pub mod matrix;
pub mod model;
pub mod profiling;
pub mod quaternion;
pub mod range;
pub mod vector;

pub type UPos = Vec2<f32>; // for ui
pub type UVec = Vec2<f32>; // for ui
pub type EPos = Vec3<f32>; // for entities
pub type EVec = Vec3<f32>; // for entities
pub type EMat = Mat3<f32>; // for entities
pub type BPos = Vec3<i64>; // for blocks
pub type BVec = Vec3<i64>; // for blocks

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum UAxis {
    Horizontal,
    Vertial,
}

pub type Block = u32;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Solidity {
    Invisible,
    Translucent,
    Fluid,
    Solid,
}

#[repr(C)]
#[derive(Debug)]
pub enum Event {
    Init {
        num_blocks: usize,
        blocks: usize, // pointer to array of BlockDef
    },
    InvalidateBlocks {
        xmin: i64,
        xmax: i64,
        ymin: i64,
        ymax: i64,
        zmin: i64,
        zmax: i64,
    },
    CreateSprite {
        id: u32,
        block: u32,
    },
    DeleteSprite {
        id: u32,
    },
}

#[repr(C)]
#[derive(Debug, Clone, Encode, Decode)]
pub struct BlockDefinition {
    pub name: String,
    pub model: Model,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Encode, Decode)]
pub struct Material {
    pub color: [u8; 3],
    pub alpha: u8,
    pub roughness: u8,
    pub emission: u8,
    pub flags: u8,
    pub _pad: u8,
}

impl Material {
    pub const FLAG_FLUID: u8 = 0b00000001;
    pub const AIR: Material = Material {
        color: [255, 255, 255],
        alpha: 0,
        _pad: 0,
        emission: 0,
        flags: 0,
        roughness: 0,
    };
    pub const DEBUG: Material = Material {
        color: [255, 100, 255],
        alpha: 255,
        _pad: 0,
        emission: 0,
        flags: 0,
        roughness: 0,
    };
    #[inline]
    pub fn fluid(&self) -> bool {
        (self.flags & Material::FLAG_FLUID) != 0
    }
    #[inline]
    pub fn is_opaque(&self) -> bool {
        self.alpha == 255
    }
    #[inline]
    pub fn is_invisible(&self) -> bool {
        self.alpha == 0
    }
}
impl Default for Material {
    fn default() -> Self {
        Material::AIR
    }
}
