/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{
    face::{Axis, BlockFace},
    range::ForEachExt,
    vector::Vec3,
    BPos, Material, Solidity,
};
use bincode::{Decode, Encode};
use std::{
    collections::{HashSet, VecDeque},
    ops::{Index, IndexMut, Range},
};

#[derive(Default, Debug, Clone, Encode, Decode)]
pub struct Model(pub [[[Material; 16]; 16]; 16]);

impl Model {
    pub fn range_identical(&self, r: Range<Vec3<usize>>) -> bool {
        // TODO probably broken (14 15 15)..(15 16 16) false
        let cmp = self.0[r.start.x][r.start.x][r.start.x];
        let mut is_eq = true;
        r.for_each(|o| is_eq &= self.0[o.x][o.y][o.z] == cmp);
        is_eq
    }

    pub fn from_material(mat: Material) -> Self {
        Model([[[mat; 16]; 16]; 16])
    }

    pub fn from_textures(
        sides: &[[[[u8; 4]; 16]; 16]; 6],
        fluid: bool,
        roughness: u8,
        emission: u8,
    ) -> Self {
        let mut m = Self::default();
        for face in BlockFace::LIST {
            let texture = sides[face.index()];
            #[allow(clippy::needless_range_loop)]
            for x in 0..16 {
                for y in 0..16 {
                    let k = if face.sign { 15 } else { 0 };
                    let mat = match face.axis {
                        Axis::X => &mut m.0[k][x][y],
                        Axis::Y => &mut m.0[y][k][x],
                        Axis::Z => &mut m.0[x][y][k],
                    };
                    mat.color[0] = texture[x][y][0];
                    mat.color[1] = texture[x][y][1];
                    mat.color[2] = texture[x][y][2];
                    mat.alpha = texture[x][y][3];
                    mat.emission = emission;
                    mat.roughness = roughness;
                    if fluid {
                        mat.flags |= Material::FLAG_FLUID
                    }
                }
            }
        }
        m
    }

    pub fn to_textures(&self) -> [[[[u8; 4]; 16]; 16]; 6] {
        let mut sides: [[[[u8; 4]; 16]; 16]; 6] = Default::default();
        for face in BlockFace::LIST {
            let texture = &mut sides[face.index()];
            #[allow(clippy::needless_range_loop)]
            for x in 0..16 {
                for y in 0..16 {
                    let k = if face.sign { 15 } else { 0 };
                    let mat = match face.axis {
                        Axis::X => &self.0[k][x][y],
                        Axis::Y => &self.0[y][k][x],
                        Axis::Z => &self.0[x][y][k],
                    };
                    texture[x][y][0] = mat.color[0];
                    texture[x][y][1] = mat.color[1];
                    texture[x][y][2] = mat.color[2];
                    texture[x][y][3] = mat.alpha;
                }
            }
        }
        sides
    }
    pub fn global_solidity_estimate(&self) -> Solidity {
        let mut solid = true;
        let mut invisible = true;
        let mut fluid = false;
        for face in BlockFace::LIST {
            for x in 0..16 {
                for y in 0..16 {
                    let k = if face.sign { 15 } else { 0 };
                    let mat = match face.axis {
                        Axis::X => self.0[k][x][y],
                        Axis::Y => self.0[y][k][x],
                        Axis::Z => self.0[x][y][k],
                    };
                    solid &= mat.alpha == 255;
                    fluid |= mat.fluid()
                }
            }
        }
        for x in 0..16 {
            for y in 0..16 {
                for z in 0..16 {
                    let mat = self.0[x][y][z];
                    invisible &= mat.alpha == 0;
                }
            }
        }
        match (solid, invisible) {
            (true, true) => unreachable!(),
            (true, false) => Solidity::Solid,
            (false, true) => Solidity::Invisible,
            (false, false) if fluid => Solidity::Fluid,
            (false, false) => Solidity::Translucent,
        }
    }

    // TODO this is horribly slow...
    pub fn fill_internal_spaces(&mut self) -> usize {
        let mut total_filled = 0;
        (BPos::splat(0)..BPos::splat(16)).for_each(|p| {
            if let Some(hole) = self.find_side_floodfill(p) {
                for p in hole {
                    self[p] = self.ray_up(p);
                    total_filled += 1;
                }
            }
        });
        total_filled
    }
    fn ray_up(&self, p: BPos) -> Material {
        let m = self[p];
        if m.is_opaque() {
            m
        } else {
            self.ray_up(p + BPos { x: 0, y: 1, z: 0 })
        }
    }
    fn find_side_floodfill(&self, p: BPos) -> Option<HashSet<BPos>> {
        let mut open_set = VecDeque::new();
        let mut checked = HashSet::new();
        open_set.push_back(p);
        while let Some(p) = open_set.pop_front() {
            if checked.contains(&p) {
                continue;
            }
            checked.insert(p);

            if self[p].is_opaque() {
                continue;
            }
            if p.x == 0 || p.y == 0 || p.z == 0 || p.x == 15 || p.y == 15 || p.z == 15 {
                return None;
            }
            for face in BlockFace::LIST {
                open_set.push_back(p + face.dir())
            }
        }
        Some(checked)
    }
}

impl Index<BPos> for Model {
    type Output = Material;
    fn index(&self, index: BPos) -> &Self::Output {
        &self.0[index.x as usize][index.y as usize][index.z as usize]
    }
}
impl IndexMut<BPos> for Model {
    fn index_mut(&mut self, index: BPos) -> &mut Self::Output {
        &mut self.0[index.x as usize][index.y as usize][index.z as usize]
    }
}
