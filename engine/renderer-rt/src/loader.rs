/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{world::World, Patch, PatchSet, SharedState};
use crossbeam_channel::Sender;
use log::debug;
use std::{collections::HashMap, ops::DerefMut, sync::RwLock, thread};

pub struct Loader {
    pub shared: SharedState,
    pub world: RwLock<World>,
    // pub loaded_groups: RwLock<HashSet<Range<BPos>>>,
    pub uploads_send: Sender<PatchSet>,
}

impl Loader {
    pub fn loader_loop(&self) {
        let mut visible_index = HashMap::new();
        loop {
            thread::sleep(std::time::Duration::from_millis(50));
            // let mut loaded = self.loaded_groups.write().unwrap();
            // let around = *self.shared.camera.read().unwrap();
            // Self::should_load(around, |g| {
            //     if !loaded.contains(&g) {
            //         self.update_region(g.clone());
            //         self.upload();
            //         loaded.insert(g);
            //     }
            // });
            self.shared.hit_feedback.update_loader(&mut visible_index);
            {
                let mut world = self.world.write().unwrap();
                for k in visible_index.keys() {
                    world.subdivide(*k, |pos| self.shared.callbacks.get(pos));
                }
                eprintln!(
                    "damage(len={} max={}) visible(len={})",
                    world.index_damaged.len(),
                    world
                        .index_damaged
                        .iter()
                        .max()
                        .copied()
                        .unwrap_or_default(),
                    visible_index.len(),
                );
            }

            self.upload();
        }
    }

    // fn should_load(around: EPos, mut cb: impl FnMut(Range<BPos>)) {
    //     let group_size = 16;
    //     let cc = around.bpos() / group_size;
    //     ((BPos::ONE * -4)..(BPos::ONE * 4)).for_each(|c| {
    //         let co = c + cc;
    //         let bounds = (co * group_size)..((co + BPos::ONE) * group_size);
    //         cb(bounds)
    //     })
    // }

    // pub fn update_region(&self, range: Range<BPos>) {
    //     info!("updating {range:?}");
    //     // TODO par?
    //     let blocks = PreloadedWorld::new(self.shared.callbacks.as_ref(), range.clone(), 1);
    //     let mut g = self.world.write().unwrap();
    //     range.for_each(|p| {
    //         let block_id = blocks.get(p);
    //         g.set(p, Node::Leaf(block_id), 0);
    //     });
    // }

    pub fn upload(&self) {
        let mut g = self.world.write().unwrap();
        let crate::World {
            index_damaged,
            nodes,
            root,
            ..
        } = g.deref_mut();

        if !index_damaged.is_empty() {
            debug!("uploading damaged nodes ({})", index_damaged.len());
            self.uploads_send
                .send(PatchSet {
                    root: *root,
                    patches: index_damaged
                        .drain()
                        .map(|index| Patch {
                            index,
                            data: nodes[index as usize].0,
                        })
                        .collect(),
                })
                .unwrap();
        }
    }
}
