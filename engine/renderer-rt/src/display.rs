/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{
    feedback::HitFeedback,
    pipelines::{render::Render, update::Update, WorldStorage},
    PatchSet, SharedState,
};
use crossbeam_channel::Receiver;
use std::sync::Arc;
use voxelwagen_common::compat::instant::Instant;
use wgpu::{util::StagingBelt, CommandEncoder, Device, Queue, SurfaceConfiguration, TextureView};

pub struct Visual {
    pub shared: SharedState,
    pub device: Arc<Device>,
    pub queue: Arc<Queue>,
    pub surface_config: SurfaceConfiguration,
    pub staging_belt: StagingBelt,
    pub uploads_receive: Receiver<PatchSet>,

    pub _stats: String,
    pub frame: usize,
    pub last_frame: Instant,

    pub render_pipeline: Render,
    pub update_pipeline: Update,
    pub world: WorldStorage,
    pub hit_feedback: HitFeedback,
    pub start_instant: Instant,
}

impl Visual {
    pub fn new(
        shared: SharedState,
        uploads_receive: Receiver<PatchSet>,
        hit_feedback: HitFeedback,
        device: &Arc<Device>,
        queue: &Arc<Queue>,
        surface_config: &SurfaceConfiguration,
    ) -> Self {
        let world = WorldStorage::new(device);
        let render_pipeline = Render::new(device, &world, &hit_feedback, surface_config.format);
        let update_pipeline = Update::new(device, &world);

        Self {
            render_pipeline,
            uploads_receive,
            update_pipeline,
            shared,
            hit_feedback,
            last_frame: Instant::now(),
            world,
            start_instant: Instant::now(),
            staging_belt: StagingBelt::new(4096),
            surface_config: surface_config.clone(),
            device: device.clone(),
            queue: queue.clone(),
            _stats: String::new(),
            frame: 0,
        }
    }

    pub fn render(&mut self, encoder: &mut CommandEncoder, target: &TextureView) {
        let now = Instant::now();
        let dt = (now - self.last_frame).as_secs_f32();
        self.last_frame = now;

        let (camera_position, camera_transform) = self.shared.callbacks.get_camera(dt);

        // TODO remove this factor or explain its reason
        self.render_pipeline.uniforms.camera_position = (camera_position * 2.).array();
        self.render_pipeline.uniforms.camera_transform = [
            [
                camera_transform.x.x,
                camera_transform.y.x,
                camera_transform.z.x,
                0.,
            ],
            [
                camera_transform.x.y,
                camera_transform.y.y,
                camera_transform.z.y,
                0.,
            ],
            [
                camera_transform.x.z,
                camera_transform.y.z,
                camera_transform.z.z,
                0.,
            ],
        ];
        self.render_pipeline.uniforms.aspect =
            self.surface_config.width as f32 / self.surface_config.height as f32;
        self.render_pipeline.uniforms.time = self.start_instant.elapsed().as_secs_f32();
        self.render_pipeline.uniforms.root = self.update_pipeline.current_root;
        self.render_pipeline.write_uniforms(&self.queue);

        *self.shared.camera.write().unwrap() = camera_position;

        self.update_pipeline.tick(
            &self.device,
            encoder,
            &mut self.staging_belt,
            &self.world,
            &self.uploads_receive,
        );

        self.render_pipeline.pass(encoder, target);
        self.hit_feedback.update_render(encoder);

        self.frame += 1;
    }
    pub fn pre_submit(&mut self) {
        self.staging_belt.finish();
    }
    pub fn post_submit(&mut self) {
        self.staging_belt.recall();
    }
}
