/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use std::{
    collections::HashMap,
    mem::size_of,
    ops::Deref,
    sync::{
        atomic::{AtomicU8, Ordering},
        Arc,
    },
};
use voxelwagen_common::BPos;
use wgpu::{
    BindGroupLayoutEntry, BindingResource, Buffer, BufferDescriptor, BufferUsages, CommandEncoder,
    Device, ShaderStages,
};

const STATE_IDLE: u8 = 0;
const STATE_UNMAPPED: u8 = 1;
const STATE_MAPPING: u8 = 2;
const STATE_MAPPED: u8 = 3;
const STATE_READING: u8 = 4;

#[derive(Clone)]
pub struct HitFeedback {
    buffer: Arc<Buffer>,
    staging: Arc<Buffer>,
    state: Arc<AtomicU8>,
}
impl HitFeedback {
    pub fn new(device: &Device) -> Self {
        let size = 1024 * 1024 * 4 * size_of::<u32>() as u64;
        let buffer = Arc::new(device.create_buffer(&BufferDescriptor {
            label: Some("hit feedback"),
            size,
            usage: BufferUsages::STORAGE | BufferUsages::COPY_SRC,
            mapped_at_creation: false,
        }));
        let staging = Arc::new(device.create_buffer(&BufferDescriptor {
            label: Some("hit feedback download"),
            size,
            usage: BufferUsages::COPY_DST | BufferUsages::MAP_READ,
            mapped_at_creation: false,
        }));
        Self {
            buffer,
            state: Arc::new(AtomicU8::new(STATE_UNMAPPED)),
            staging,
        }
    }
    pub fn layout_entry(binding: u32, ro: bool, stage: ShaderStages) -> BindGroupLayoutEntry {
        BindGroupLayoutEntry {
            binding,
            visibility: stage,
            ty: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Storage { read_only: ro },
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        }
    }
    pub fn update_render(&self, encoder: &mut CommandEncoder) {
        if self
            .state
            .compare_exchange(
                STATE_IDLE,
                STATE_UNMAPPED,
                Ordering::Relaxed,
                Ordering::Relaxed,
            )
            .is_ok()
        {
            encoder.copy_buffer_to_buffer(&self.buffer, 0, &self.staging, 0, self.buffer.size());
        }
    }
    pub fn update_post_submit(&self) {
        if self
            .state
            .compare_exchange(
                STATE_UNMAPPED,
                STATE_MAPPING,
                Ordering::Relaxed,
                Ordering::Relaxed,
            )
            .is_ok()
        {
            let state = self.state.clone();
            self.staging
                .slice(..)
                .map_async(wgpu::MapMode::Read, move |res| {
                    res.unwrap();
                    state.store(STATE_MAPPED, Ordering::Relaxed);
                });
        }
    }
    pub fn update_loader(&self, seen: &mut HashMap<BPos, usize>) {
        if self
            .state
            .compare_exchange(
                STATE_MAPPED,
                STATE_READING,
                Ordering::Relaxed,
                Ordering::Relaxed,
            )
            .is_ok()
        {
            let buf = self.staging.clone();
            {
                let mapped = buf.slice(..).get_mapped_range();
                let values = bytemuck::cast_slice::<_, [u32; 4]>(mapped.deref());

                seen.clear();
                for &v in values {
                    *seen
                        .entry(BPos {
                            x: v[0] as i32 as i64,
                            y: v[1] as i32 as i64,
                            z: v[2] as i32 as i64,
                        })
                        .or_default() += 1
                }
                drop(mapped)
            }
            buf.unmap();
            self.state.store(STATE_IDLE, Ordering::Relaxed);
        }
    }
    pub fn entry(&self) -> BindingResource {
        self.buffer.as_entire_binding()
    }
}
