/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(stmt_expr_attributes)]
pub(crate) mod display;
pub mod feedback;
pub(crate) mod loader;
pub(crate) mod pipelines;
pub mod sprite;
pub(crate) mod world;

use display::Visual;
use feedback::HitFeedback;
use loader::Loader;
use std::{
    collections::HashMap,
    ops::Range,
    sync::{Arc, RwLock},
    thread,
};
use voxelwagen_common::{compat::SendAnyway, BPos, EPos};
use voxelwagen_renderer_common::{Renderer, RendererCallbacks, RendererNew};
use wgpu::{CommandEncoder, TextureView};
use world::World;

#[derive(Debug)]
pub struct Patch {
    pub index: u32,
    pub data: [u32; 8],
}
pub struct PatchSet {
    pub root: u32,
    pub patches: Vec<Patch>,
}

#[derive(Default)]
pub struct Sprite {
    pub set: bool,
    pub origin: BPos,
    pub active: bool,
}

pub struct RtViewer {
    pub sprites: RwLock<HashMap<u64, Sprite>>,
    visual: SendAnyway<Visual>,
    pub shared: SharedState,
}

#[derive(Clone)]
pub struct SharedState {
    pub hit_feedback: HitFeedback,
    pub camera: Arc<RwLock<EPos>>,
    pub callbacks: Arc<dyn RendererCallbacks>,
}

impl RendererNew for RtViewer {
    fn new(
        callbacks: Arc<dyn voxelwagen_renderer_common::RendererCallbacks>,
        device: &Arc<wgpu::Device>,
        queue: &Arc<wgpu::Queue>,
        surface_config: &wgpu::SurfaceConfiguration,
    ) -> Self {
        let (uploads_send, uploads_receive) = crossbeam_channel::unbounded();
        let hit_feedback = HitFeedback::new(device);

        let shared = SharedState {
            hit_feedback: hit_feedback.clone(),
            camera: Arc::new(RwLock::new(EPos::ZERO)),
            callbacks,
        };

        let visual = Visual::new(
            shared.clone(),
            uploads_receive,
            hit_feedback,
            device,
            queue,
            surface_config,
        );

        let state = RtViewer {
            sprites: Default::default(),
            visual: SendAnyway(visual),
            shared: shared.clone(),
        };

        let loader = Loader {
            shared,
            uploads_send,
            world: World::default().into(),
        };

        thread::Builder::new()
            .name("loader loop".to_string())
            .spawn(move || loader.loader_loop())
            .unwrap();

        // {
        //     let state = state.clone();
        //     thread::Builder::new()
        //         .name("sprite loop".to_string())
        //         .spawn(move || state.sprite_loop())
        //         .unwrap();
        // }

        state
    }
}

impl Renderer for RtViewer {
    fn invalidate_blocks(&mut self, _range: Range<BPos>) {
        // let mut g = self.world.write().unwrap();
        // range.for_each(|p| {
        //     let block = self.callbacks.get_block(p);
        //     g.set_block(p, block)
        // })
    }
    fn invalidate_sprites(&mut self, _ents: &[u64]) {
        // let mut g = self.sprites.write().unwrap();
        // for id in ents {
        //     if let Some((_block, _offset, _transform)) = self.callbacks.get_sprite(*id) {
        //         g.entry(*id).or_default();
        //     } else {
        //         g.remove(id);
        //     }
        // }
    }

    fn reconfigure(&mut self, surface_config: &wgpu::SurfaceConfiguration) {
        surface_config.clone_into(&mut self.visual.surface_config);
    }

    fn render(&mut self, encoder: &mut CommandEncoder, view: &TextureView) {
        self.visual.render(encoder, view)
    }

    fn pre_submit(&mut self) {
        self.visual.pre_submit()
    }
    fn post_submit(&mut self) {
        self.visual.post_submit();
        self.shared.hit_feedback.update_post_submit();
    }

    fn stats(&mut self, _out: &mut String) {
        // let wg = self.world.read().unwrap();
        // writeln!(out, "node registration: {}", wg.nk.len()).unwrap();
        // writeln!(
        //     out,
        //     "node buffer: {} (free={}, damaged={})",
        //     wg.nodes.len(),
        //     wg.index_free.len(),
        //     wg.index_damaged.len()
        // )
        // .unwrap();
        // writeln!(out, "current root: {}", wg.root).unwrap();
        // writeln!(
        //     out,
        //     "ops: add={} remove={} merge={}",
        //     wg.stat_add, wg.stat_remove, wg.stat_merge
        // )
        // .unwrap();
    }
}
