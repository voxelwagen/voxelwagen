/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::WorldStorage;
use crate::feedback::HitFeedback;
use bytemuck::{Pod, Zeroable};
use wgpu::{
    BindGroup, Buffer, BufferDescriptor, BufferUsages, ColorTargetState, ColorWrites,
    CommandEncoder, Device, LoadOp, MultisampleState, PipelineCompilationOptions, PrimitiveState,
    Queue, RenderPassColorAttachment, RenderPassDescriptor, RenderPipeline, ShaderStages, StoreOp,
    TextureFormat, TextureView,
};

pub struct Render {
    pipeline: RenderPipeline,
    bind_group: BindGroup,
    pub uniforms: Uniforms,
    uniforms_buffer: Buffer,
}

#[repr(C, align(16))]
#[derive(Debug, Copy, Clone, Default, Pod, Zeroable)]
pub struct Uniforms {
    pub camera_position: [f32; 3],
    pub _pad2: f32,
    pub camera_transform: [[f32; 4]; 3],
    pub aspect: f32,
    pub time: f32,
    pub root: u32,
    pub _pad4: f32,
}

impl Render {
    pub fn new(
        device: &Device,
        wost: &WorldStorage,
        hit_feedback: &HitFeedback,
        swapchain_format: TextureFormat,
    ) -> Self {
        let uniforms = Uniforms {
            ..Default::default()
        };
        let uniforms_buffer = device.create_buffer(&BufferDescriptor {
            label: None,
            size: std::mem::size_of::<Uniforms>() as u64,
            usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });

        // TODO include_wgsl! if in production
        let shader_source =
            std::fs::read_to_string("../voxelwagen/engine/renderer-rt/src/pipelines/render.wgsl")
                .unwrap();
        let module = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(&shader_source)),
        });
        // let module = device.create_shader_module(wgpu::include_wgsl!("render.wgsl"));

        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
                WorldStorage::layout_entry(1, true, ShaderStages::FRAGMENT),
                HitFeedback::layout_entry(2, false, ShaderStages::FRAGMENT),
            ],
            label: None,
        });
        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: uniforms_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wost.entry(),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: hit_feedback.entry(),
                },
            ],
            label: None,
        });

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });

        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &module,
                compilation_options: PipelineCompilationOptions::default(),
                entry_point: "vertex_stage",
                buffers: &[],
            },
            fragment: Some(wgpu::FragmentState {
                compilation_options: PipelineCompilationOptions::default(),
                module: &module,
                entry_point: "fragment_stage",
                targets: &[Some(ColorTargetState {
                    format: swapchain_format,
                    blend: None,
                    write_mask: ColorWrites::ALL,
                })],
            }),
            depth_stencil: None,
            multisample: MultisampleState::default(),
            primitive: PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                conservative: false,
                cull_mode: None,
                front_face: wgpu::FrontFace::Ccw,
                polygon_mode: wgpu::PolygonMode::Fill,
                strip_index_format: None,
                unclipped_depth: false,
            },
            multiview: None,
        });

        Render {
            bind_group,
            pipeline,
            uniforms,
            uniforms_buffer,
        }
    }

    pub fn write_uniforms(&self, queue: &Queue) {
        queue.write_buffer(
            &self.uniforms_buffer,
            0,
            bytemuck::cast_slice(&[self.uniforms]),
        );
    }

    pub fn pass(&self, encoder: &mut CommandEncoder, target: &TextureView) {
        let mut rpass = encoder.begin_render_pass(&RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(RenderPassColorAttachment {
                ops: wgpu::Operations {
                    load: LoadOp::Clear(wgpu::Color::BLACK),
                    store: StoreOp::Store,
                },
                resolve_target: None,
                view: target,
            })],
            depth_stencil_attachment: None,
            timestamp_writes: None,
            occlusion_query_set: None,
        });
        rpass.set_pipeline(&self.pipeline);
        rpass.set_bind_group(0, &self.bind_group, &[]);
        rpass.draw(0..4, 0..1);
    }
}
