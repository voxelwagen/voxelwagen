//    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
//    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
//    Copyright (C) 2024 metamuffin <metamuffin.org>


struct Uniforms { camera_position: vec3<f32>, camera_transform: mat3x3<f32>, aspect: f32, time: f32, root: u32 };
struct Leaf { roughness: f32, metallic: f32, emission: f32, albedo: vec3<f32>, alpha: f32 }
struct Vertex { @builtin(position) clip: vec4<f32>, @location(0) uv: vec2<f32> }
struct TraceR { exit: u32, steps: u32, distance: f32, normal: vec3<f32>, material: Leaf }
struct LookupR { index: u32, depth: u32 };
struct MapR { d: f32, rawnorm: vec3<f32>, leaf: Leaf };

const I_TAG = 0; const I_FLAGS = 1; const I_ROUGHNESS = 2; const I_EMISSION = 3; const I_R = 4; const I_G = 5; const I_B = 6; const I_ALPHA = 7;
const EXIT_HIT = 0u; const EXIT_SKY = 1u; const EXIT_FAIL = 2u;
const HBITS: u32 = 31u;
const HMASK: u32 = 2147483648u;
const MAX_DIST: f32 = 32768.;
const MAX_STEPS: u32 = 256u;
const MAX_BOUNCES: u32 = 3u;
const SAMPLE_COUNT: u32 = 2u;

@group(0) @binding(0) var<uniform> un: Uniforms;
@group(0) @binding(1) var<storage, read> world: array<array<u32, 8>>;
@group(0) @binding(2) var<storage, read_write> tree_hits: array<vec3<u32>>;

@vertex
fn vertex_stage(@builtin(vertex_index) in_vertex_index: u32) -> Vertex {
    let x = f32(i32(in_vertex_index & 1u) * 2 - 1);
    let y = f32(i32(in_vertex_index / 2u) * 2 - 1);
    var v: Vertex;
    v.clip = vec4<f32>(x, y, 0.0, 1.0);
    v.uv = vec2<f32>(x, y);
    return v;
}

@fragment
fn fragment_stage(in: Vertex) -> @location(0) vec4<f32> {
    RNG_STATE = srand(in.uv.x + srand(in.uv.y + srand(un.time)));
    let out = image(in.uv);
    return vec4<f32>(out, 1.0);
}

////////////////////////// path tracing functions

fn image(uv: vec2<f32>) -> vec3<f32> {
    var sample_acc = vec4(0.);
    for (var i = 0u; i < SAMPLE_COUNT; i += 1u) {
        let ray = un.camera_transform * normalize(vec3(0., 0., -2.) + vec3(uv.x * un.aspect, uv.y, 0.));
        sample_acc += sample(un.camera_position, ray);
    }
    var out = sample_acc.rgb / sample_acc.a;
    // out = atmosphere(distance, out);
    out = tonemap(out);
    return out;
}

fn sample(position1: vec3<f32>, dir1: vec3<f32>) -> vec4<f32> {
    var position = position1; var dir = dir1;
    var filt = vec3(1.);
    var out = vec3(0.);
    for (var steps = 0u; steps < MAX_STEPS; steps += 5u) {
        let sa = trace(position, dir, MAX_STEPS);
        if sa.exit == EXIT_SKY { out += sky(dir) * filt; break; }
        if sa.exit == EXIT_FAIL { return vec4(1., 0., 1., 1.); }
        out = sa.material.albedo + abs(sa.normal) * 0.1;
        break;
        // filt *= sa.material.albedo;
        // out += sa.material.emission * filt;
        // let bounce = scatter(sa.normal, dir, sa.material.roughness);
        // position += sa.distance * dir;
        // dir = bounce;
        // steps += sa.steps;
    }
    return vec4(out, 1.);
}

fn scatter(normal: vec3<f32>, dir: vec3<f32>, roughness: f32) -> vec3<f32> {
    return sample_hemisphere_cos(normal);
}

fn trace(position: vec3<f32>, raydir: vec3<f32>, max_steps: u32) -> TraceR {
    var dt = 0.;
    for (var i = 0u; i < max_steps && dt < MAX_DIST; i += 1u) {
        let k = map(position + raydir * dt, raydir);
        if k.leaf.alpha > 0. { return TraceR(EXIT_HIT, i, dt - 0.002, unraw_normal(k.rawnorm), k.leaf); } // TODO why must this epsilon be so big?
        dt += k.d + 0.001;
    }
    var r: TraceR;
    if dt >= MAX_DIST { r.exit = EXIT_SKY; r.distance = 30000.; } else { r.exit = EXIT_FAIL; }
    return r;
}

////////////////////////// sky and atmosphere

fn sun_position() -> vec3<f32> {
    let time = un.time * 0.01;
    let sun = vec3(sin(time), cos(time), 0.);
    return sun;
}
fn sky_global() -> vec3<f32> {
    return vec3(147., 185., 255.) * 0.01 * (0.1 + max(0., sun_position().y));
}
fn sky(dir: vec3<f32>) -> vec3<f32> {
    // return vec3(dir.y*2.);
    let sun = sun_position();
    let sun_ray = dot(dir, sun);
    var scatter = exp2(-(dir.y - 0.5 * sun_ray) / vec3(.4, .5, .6)) * sqrt(max(0.001, sun.y + 0.2)) * 2.5;
    let disk = vec3(50.0, 20.0, 1.) * smoothstep(0.999, 1.0, max(0.0, sun_ray));
    return (scatter + disk) * sqrt(max(0., dir.y));
}

fn atmosphere(depth: f32, color: vec3<f32>) -> vec3<f32> {
    let f = pow(vec3(2.), -0.0001 * depth * vec3(1., 2., 4.));
    return mix(0.3 * sky_global(), color, f);
}

////////////////////////// tonemapping functions

// based off MIT licenced code from BakingLab; original from Stephen Hill
// https://github.com/TheRealMJP/BakingLab/blob/6677ddbd5f90a0548e74647dd37753b2858f376d/BakingLab/ACES.hlsl
fn tonemap(x1: vec3<f32>) -> vec3<f32> {
    var x = x1;
    // sRGB => XYZ => D65_2_D60 => AP1 => RRT_SAT
    let aces_input_map: mat3x3<f32> = mat3x3(0.59719, 0.35458, 0.04823,
        0.07600, 0.90834, 0.01566,
        0.02840, 0.13383, 0.83777);
    // ODT_SAT => XYZ => D60_2_D65 => sRGB
    let aces_output_map: mat3x3<f32> = mat3x3(1.60475, -0.53108, -0.07367,
        -0.10208, 1.10813, -0.00605,
        -0.00327, -0.07276, 1.07602);
    // x = pow(x, vec3(2.2));
    // if un.time % 2. < 1. {
    // x = aces_input_map * x;
    // x = rrt_and_odt_fit(x);
    // x = aces_output_map * x;
    // x = saturate(x);
    // }
    // x = pow(x, vec3(1./2.2));
    return x;
}
fn rrt_and_odt_fit(v: vec3<f32>) -> vec3<f32> {
    let a = v * (v + 0.0245786) - 0.000090537;
    let b = v * (0.983729 * v + 0.4329510) + 0.238081;
    return a / b;
}

////////////////////////// octtree traversal functions

fn lookup(x1: u32, y1: u32, z1: u32) -> LookupR {
    var x = x1; var y = y1; var z = z1;
    var index = un.root;
    var depth = 0u;
    for (depth = 0u; depth <= HBITS; depth++) {
        if world[index][I_TAG] == 0u { break; }
        let ki = ((x & HMASK) >> (HBITS - u32(0))) + ((y & HMASK) >> (HBITS - u32(1))) + ((z & HMASK) >> (HBITS - u32(2)));
        index = world[index][ki];
        x <<= 1u; y <<= 1u; z <<= 1u;
    }
    return LookupR(index, depth);
}

fn cube_exit_rayd(pos: vec3<f32>, dir: vec3<f32>) -> f32 {
    let k = (step(vec3(0.), dir) - pos) / dir;
    return min(k.x, min(k.y, k.z));
}
fn unraw_normal(nrel: vec3<f32>) -> vec3<f32> {
    let n = nrel - 0.5;
    let nabs = abs(n);
    if nabs.x > max(nabs.y, nabs.z) { return vec3(sign(n.x), 0., 0.); } else if nabs.y > nabs.z { return vec3(0., sign(n.y), 0.); } else { return vec3(0., 0., sign(n.z)); }
}

fn map(pos: vec3<f32>, dir: vec3<f32>) -> MapR {
    let upos = vec3<u32>(vec3<i32>(floor(pos)));
    let lookup = lookup(upos.x, upos.y, upos.z);
    let cube_scale = f32(1u << (HBITS - lookup.depth + 1u));
    let cube_local = fract(pos / cube_scale);
    var r: MapR;
    r.d = cube_exit_rayd(cube_local, dir) * cube_scale;
    r.rawnorm = cube_local;

    let k = rand();
    // if k < 0.01 {
    let index = u32(k * 100000.) % arrayLength(&tree_hits);
    tree_hits[index] = upos;
    // }

    let leafdata = world[lookup.index];
    r.leaf.albedo = bitcast<vec3<f32>>(vec3(leafdata[I_R], leafdata[I_G], leafdata[I_B]));
    r.leaf.alpha = bitcast<f32>(leafdata[I_ALPHA]);
    r.leaf.emission = bitcast<f32>(leafdata[I_EMISSION]);
    r.leaf.roughness = bitcast<f32>(leafdata[I_ROUGHNESS]);
    return r;
}

////////////////////////// helper functions

const PI: f32 = 3.141592654;
const TWO_PI: f32 = 6.283185307;

fn rotateX(p: vec3<f32>, ang: f32) -> vec3<f32> { let s = sin(ang); let c = cos(ang); return vec3(p.x, c * p.y + s * p.z, -s * p.y + c * p.z); }
fn rotateY(p: vec3<f32>, ang: f32) -> vec3<f32> { let s = sin(ang); let c = cos(ang); return vec3(c * p.x + s * p.z, p.y, -s * p.x + c * p.z); }
fn rotateZ(p: vec3<f32>, ang: f32) -> vec3<f32> { let s = sin(ang); let c = cos(ang); return vec3(c * p.x + s * p.y, -s * p.x + c * p.y, p.z); }

var<private> RNG_STATE: f32 = 0.;
fn rand() -> f32 { RNG_STATE = srand(RNG_STATE); return RNG_STATE; }
fn srand(s: f32) -> f32 { return fract(sin(s * 2136.1321477) * 72483.2436435); }

// fn sample_hemisphere_cos_approx2(normal: vec3<f32>) -> vec3<f32> {
//     return normalize(normal + sample_sphere_uniform());
// }
// fn sample_hemisphere_cos_approx() -> vec3<f32> {
//     let z = rand();
//     let phi = TWO_PI * rand();
//     let r = sqrt(max(0.0, 1.0 - z * z));
//     return vec3(r * cos(phi), r * sin(phi), z);
// }
fn sample_hemisphere_cos(normal: vec3<f32>) -> vec3<f32> {
    return normalize(sample_sphere_uniform() + normal);
}
fn sample_sphere_uniform() -> vec3<f32> {
    let z = acos(rand() * 2. * -1.) / PI * 2. - 1.;
    let phi = rand() * TWO_PI;
    let r = sqrt(max(0., 1.0 - z * z));
    return vec3(
        r * cos(phi),
        r * sin(phi),
        z
    );
}
fn sample_hemisphere_uniform(normal: vec3<f32>) -> vec3<f32> {
    let v = sample_sphere_uniform();
    if dot(v, normal) < 0. { return -v; } else { return v; }
}
