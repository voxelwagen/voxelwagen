/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::WorldStorage;
use crate::PatchSet;
use crossbeam_channel::Receiver;
use log::debug;
use std::{mem::size_of, num::NonZeroU64};
use wgpu::{
    include_wgsl, util::StagingBelt, BindGroup, BindGroupDescriptor, BindGroupEntry,
    BindGroupLayoutEntry, Buffer, BufferDescriptor, BufferUsages, CommandEncoder,
    ComputePassDescriptor, ComputePipeline, Device, PipelineCompilationOptions, ShaderStages,
};

pub const MAX_COMPUTE_DISPATCH: usize = 65535;

pub struct Update {
    pub current_root: u32,
    bind_group: BindGroup,
    patch_buffer: Buffer,
    pipeline: ComputePipeline,
}
impl Update {
    pub fn new(device: &Device, wost: &WorldStorage) -> Self {
        let patch_buffer = device.create_buffer(&BufferDescriptor {
            label: Some("world patch buffer"),
            size: size_of::<u32>() as u64 * 9 * MAX_COMPUTE_DISPATCH as u64,
            usage: BufferUsages::COPY_DST | BufferUsages::STORAGE,
            mapped_at_creation: false,
        });

        let module = device.create_shader_module(include_wgsl!("update.wgsl"));

        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("world update layoout"),
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    count: None,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Storage { read_only: true },
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    visibility: ShaderStages::COMPUTE,
                },
                WorldStorage::layout_entry(1, false, ShaderStages::COMPUTE),
            ],
        });
        let bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: Some("world update group"),
            layout: &bind_group_layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: patch_buffer.as_entire_binding(),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: wost.entry(),
                },
            ],
        });

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("world update pipeline layout"),
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });
        let pipeline = device.create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
            label: Some("world update pipeline"),
            layout: Some(&pipeline_layout),
            module: &module,
            entry_point: "main",
            compilation_options: PipelineCompilationOptions::default(),
        });

        Self {
            bind_group,
            patch_buffer,
            current_root: 0,
            pipeline,
        }
    }

    pub fn tick(
        &mut self,
        device: &Device,
        encoder: &mut CommandEncoder,
        staging_belt: &mut StagingBelt,
        _wost: &WorldStorage,
        recv: &Receiver<PatchSet>,
    ) {
        if let Ok(mut ps) = recv.try_recv() {
            self.current_root = ps.root;

            // for p in &ps.patches {
            //     let mut view = staging_belt.write_buffer(
            //         encoder,
            //         &wost.buffer,
            //         8 * size_of::<u32>() as u64 * p.index as u64,
            //         NonZeroU64::new(8 * size_of::<u32>() as u64).unwrap(),
            //         &device,
            //     );
            //     view.copy_from_slice(
            //         &p.data
            //             .into_iter()
            //             .flat_map(u32::to_ne_bytes)
            //             .collect::<Vec<u8>>(),
            //     );
            // }
            loop {
                if ps.patches.is_empty() {
                    return;
                }
                debug!("applying {} patches", ps.patches.len());

                let mut data = Vec::new();
                let mut count = 0;
                while let Some(p) = ps.patches.pop() {
                    data.extend(p.index.to_ne_bytes());
                    data.extend(p.data.into_iter().flat_map(u32::to_ne_bytes));
                    count += 1;
                    if count >= MAX_COMPUTE_DISPATCH {
                        break;
                    }
                }

                {
                    let mut view = staging_belt.write_buffer(
                        encoder,
                        &self.patch_buffer,
                        0,
                        NonZeroU64::new(data.len().try_into().unwrap()).unwrap(),
                        device,
                    );
                    view.copy_from_slice(&data);
                }
                {
                    let mut cpass = encoder.begin_compute_pass(&ComputePassDescriptor {
                        label: Some("update world"),
                        timestamp_writes: None,
                    });
                    cpass.set_pipeline(&self.pipeline);
                    cpass.set_bind_group(0, &self.bind_group, &[]);
                    cpass.dispatch_workgroups(count.try_into().unwrap(), 1, 1);
                }
            }
        }
    }
}
