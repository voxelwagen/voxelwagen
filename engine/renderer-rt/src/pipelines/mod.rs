/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use wgpu::{
    BindGroupLayoutEntry, BindingResource, Buffer, BufferDescriptor, BufferUsages, Device,
    ShaderStages,
};

pub mod render;
pub mod update;

pub struct WorldStorage {
    buffer: Buffer,
}

impl WorldStorage {
    pub fn new(device: &Device) -> Self {
        let buffer = device.create_buffer(&BufferDescriptor {
            label: Some("world storage"),
            size: 1024
                * 1024
                * std::env::var("WORLD_BUF_SIZE")
                    .map(|e| e.parse().unwrap())
                    .unwrap_or(1),
            usage: BufferUsages::STORAGE | BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });

        Self { buffer }
    }
    pub fn layout_entry(binding: u32, ro: bool, stage: ShaderStages) -> BindGroupLayoutEntry {
        BindGroupLayoutEntry {
            binding,
            visibility: stage,
            ty: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Storage { read_only: ro },
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        }
    }
    pub fn entry(&self) -> BindingResource {
        self.buffer.as_entire_binding()
    }
}
