//    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
//    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
//    Copyright (C) 2024 metamuffin <metamuffin.org>

struct Patch {
    index: u32,
    data: array<u32, 8>,
};

@group(0) @binding(0) var<storage, read> patches: array<Patch>;
@group(0) @binding(1) var<storage, read_write> world: array<array<u32, 8>>;

@compute @workgroup_size(1)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let p = patches[global_id.x];
    world[p.index] = p.data;
}
