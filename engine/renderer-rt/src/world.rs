/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use rand::{thread_rng, Rng};
use std::{
    collections::HashSet,
    ops::{Index, IndexMut, Range},
};
use voxelwagen_common::{
    range::RangeExt,
    vector::{pack::v3, Vec3},
    BPos, Material,
};

const HBITS: u32 = 31;
// const HBITS: u32 = 32;
const _HMASK_GPU: u32 = 1 << HBITS;
const HMASK: u32 = 1 << (HBITS - 1);

pub struct World {
    pub root: u32,
    pub nodes: Vec<Node>,
    pub index_damaged: HashSet<u32>,
}

impl Default for World {
    fn default() -> Self {
        Self {
            root: 0,
            nodes: vec![Node::new_leaf(Material::DEBUG)],
            index_damaged: HashSet::from_iter([0]),
        }
    }
}

impl World {
    pub fn lookup(&self, pos: BPos) -> (Node, u32, u32) {
        let [mut x, mut y, mut z] = pos.array().map(|e| e as u32);
        let mut index = self.root;
        let mut depth = 0;
        loop {
            let c = self[index];
            if c.is_leaf() {
                break (c, index, depth);
            }
            let branch_index = (((x & HMASK) >> (HBITS - 1))
                | ((y & HMASK) >> (HBITS - 2))
                | ((z & HMASK) >> (HBITS - 3))) as usize;
            index = c.0[branch_index];

            x <<= 1;
            y <<= 1;
            z <<= 1;
            depth += 1;
        }
    }
    pub fn add(&mut self, node: Node) -> u32 {
        let index = self.nodes.len() as u32;
        self.nodes.push(node);
        self.index_damaged.insert(index);
        index
    }

    pub fn subdivide(&mut self, mut pos: BPos, query: impl Fn(BPos) -> Material) {
        let (current, index, depth) = self.lookup(pos);
        if depth >= HBITS {
            return;
        }

        let dmask = (!0) << (HBITS - depth);
        pos.x &= dmask;
        pos.y &= dmask;
        pos.z &= dmask;

        let ns = 2i64.pow(HBITS - depth);
        let parent = pos..pos + BPos::splat(ns);
        let children = split_range(parent).map(|pos| Node::new_leaf(query(sample_range(pos))));

        if children[0] == children[1]
            && children[0] == children[2]
            && children[0] == children[3]
            && children[0] == children[4]
            && children[0] == children[5]
            && children[0] == children[6]
            && children[0] == children[7]
        {
            if children[0] == current {
                // nothing changed
            } else {
                self[index] = children[0];
            }
        } else {
            self[index] = Node(children.map(|node| self.add(node)));
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Node(pub [u32; 8]);

impl Node {
    pub fn is_leaf(self) -> bool {
        self.0[0] == 0
    }
    pub fn new_leaf(mat: Material) -> Self {
        Self([
            0,
            mat.flags as u32,
            (mat.roughness as f32 / 255.).to_bits(),
            (mat.emission as f32 / 20.).to_bits(),
            (mat.color[0] as f32 / 255.).powf(2.2).to_bits(),
            (mat.color[1] as f32 / 255.).powf(2.2).to_bits(),
            (mat.color[2] as f32 / 255.).powf(2.2).to_bits(),
            (mat.alpha as f32 / 255.).to_bits(),
        ])
    }
}

impl Index<u32> for World {
    type Output = Node;
    #[inline]
    fn index(&self, index: u32) -> &Self::Output {
        &self.nodes[index as usize]
    }
}
impl IndexMut<u32> for World {
    #[inline]
    fn index_mut(&mut self, index: u32) -> &mut Self::Output {
        self.index_damaged.insert(index);
        &mut self.nodes[index as usize]
    }
}

fn split_range(r: Range<BPos>) -> [Range<BPos>; 8] {
    let ns = r.size().x / 2;
    let Vec3 { x, y, z } = r.start;
    [
        v3(x, y, z),
        v3(x + ns, y, z),
        v3(x, y + ns, z),
        v3(x + ns, y + ns, z),
        v3(x, y, z + ns),
        v3(x + ns, y, z + ns),
        v3(x, y + ns, z + ns),
        v3(x + ns, y + ns, z + ns),
    ]
    .map(|p| p..p + BPos::splat(ns))
}
fn sample_range(r: Range<BPos>) -> BPos {
    BPos {
        x: thread_rng().gen_range(r.start.x..r.end.x),
        y: thread_rng().gen_range(r.start.y..r.end.y),
        z: thread_rng().gen_range(r.start.z..r.end.z),
    }
}
