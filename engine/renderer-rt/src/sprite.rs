/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

// impl RtViewer {
//     pub fn sprite_loop(&self) {
//         loop {
//             self.update_sprites();
//             self.upload();
//             thread::sleep(std::time::Duration::from_millis(1000 / 30));
//         }
//     }
//     fn update_sprites(&self) {
//         let mut s = self.sprites.write().unwrap();
//         let mut w = self.world.write().unwrap();

//         for (sid, so) in s.iter_mut() {
//             if let Some((bid, pos, _mat)) = self.callbacks.get_sprite(*sid) {
//                 let origin = (pos * 16.).round().bpos();
//                 if origin != so.origin {
//                     if so.set {
//                         (so.origin..so.origin + v3(16, 16, 16))
//                             .clone()
//                             .for_each(|p| w.set(p, Node::Leaf(Material::AIR), 0));
//                     }
//                     let model = &self.blockdefs[bid as usize].model.0;
//                     for x in 0..16 {
//                         for y in 0..16 {
//                             for z in 0..16 {
//                                 w.set(
//                                     origin + v3(x as i64, y as i64, z as i64),
//                                     Node::Leaf(model[x][y][z]),
//                                     0,
//                                 )
//                             }
//                         }
//                     }
//                 }
//                 so.set = true;
//                 so.origin = origin;
//             }
//         }
//     }
// }
