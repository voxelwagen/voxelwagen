/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use std::{ops::Range, sync::Arc};
use voxelwagen_common::{BPos, Block, EMat, EPos, Material, UVec};
use voxelwagen_world::World;
use wgpu::{CommandEncoder, Device, Queue, SurfaceConfiguration, TextureView};

pub trait RendererNew {
    fn new(
        callbacks: Arc<dyn RendererCallbacks>,
        device: &Arc<Device>,
        queue: &Arc<Queue>,
        surface_config: &SurfaceConfiguration,
    ) -> Self;
}

pub trait Renderer: Send + Sync + 'static {
    fn reconfigure(&mut self, surface_config: &SurfaceConfiguration);
    fn render(&mut self, encoder: &mut CommandEncoder, target: &TextureView);
    fn pre_submit(&mut self);
    fn post_submit(&mut self);

    fn invalidate_blocks(&mut self, range: Range<BPos>);
    fn invalidate_sprites(&mut self, ents: &[u64]);

    fn stats(&mut self, out: &mut String);
}

pub trait RendererCallbacks: World<Material> + Send + Sync + 'static {
    fn get_sprite(&self, id: u64) -> Option<(Block, EPos, EMat)>;
    fn get_camera(&self, delta: f32) -> (EPos, EMat);
}

pub use winit::{
    event::MouseButton,
    keyboard::{Key, KeyCode, PhysicalKey, SmolStr},
};

#[derive(Debug)]
pub enum InputEvent {
    Key {
        physical: PhysicalKey,
        semantic: Key<SmolStr>,
        down: bool,
    },
    Resize {
        width: u32,
        height: u32,
    },
    MouseMotion {
        delta: UVec,
    },
    MousePosition {
        position: UVec,
    },
    MouseButton {
        button: MouseButton,
        down: bool,
    },
    MouseWheel(UVec),
    SetFocus(bool),
}
