/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(stmt_expr_attributes)]
pub mod blocks;
pub mod worldgen;

use blocks::{block_defs, Block};
use std::{
    collections::{HashMap, HashSet},
    ops::Range,
    sync::{Arc, RwLock},
};
use voxelwagen_engine::{
    common::{
        color::Color,
        compat::{init_logging, instant::Instant},
        BPos, BlockDefinition, EMat, EPos, EVec, Material, UAxis, UVec,
    },
    graphics::{
        defs::{InputEvent, KeyCode, MouseButton, PhysicalKey},
        dyndraw::Vertex,
        GameOutEvents, Graphics, GraphicsCallbacks,
    },
    ui::{
        layout::flow,
        state::UiState,
        theme::Theme,
        widgets::{self, fixed, label},
        Container,
    },
    world::World,
};
use worldgen::Worldgen;

struct State {
    _world: Arc<GameWorld>,
}

struct GameWorld {
    start: Instant,
    controller: RwLock<Controller>,
    ui_state: RwLock<UiState>,
    worldgen: Worldgen,
    block_override: RwLock<HashMap<BPos, Block>>,
    invalid: RwLock<HashSet<BPos>>,
    block_defs: Vec<BlockDefinition>,
}

pub struct Controller {
    position: EPos,
    pitch: f32,
    yaw: f32,
    speed: EVec,
    last_facing: EVec,
    keys_down: HashSet<PhysicalKey>,
}

impl World<Material> for GameWorld {
    fn get(&self, p: BPos) -> Material {
        let pworld = p.array().map(|c| c / 16).into();
        let pblock = p.array().map(|c| c.rem_euclid(16)).into();
        let id = self
            .block_override
            .read()
            .unwrap()
            .get(&pworld)
            .map(|b| b.to_owned())
            .unwrap_or_else(|| self.worldgen.generate(pblock));
        self.block_defs[id as usize].model.0[pblock.x as usize][pblock.y as usize]
            [pblock.z as usize]
    }
}

impl GraphicsCallbacks for GameWorld {
    fn get_sprite(&self, id: u64) -> Option<(u32, EPos, EMat)> {
        if id == 0 {
            Some((
                Block::Dirt as u32,
                EPos {
                    x: self.start.elapsed().as_secs_f32().sin() * 10.,
                    y: 0.,
                    z: 0.,
                },
                EMat::scale(1.2),
            ))
        } else {
            None
        }
    }
    fn get_camera(&self, delta: f32) -> (EPos, EMat) {
        let mut c = self.controller.write().unwrap();

        let mut speed = EPos::ZERO;
        for k in &c.keys_down {
            match *k {
                PhysicalKey::Code(KeyCode::KeyW) => speed.z += -1.0,
                PhysicalKey::Code(KeyCode::KeyS) => speed.z += 1.0,
                PhysicalKey::Code(KeyCode::KeyA) => speed.x += -1.0,
                PhysicalKey::Code(KeyCode::KeyD) => speed.x += 1.0,
                PhysicalKey::Code(KeyCode::ControlLeft) => speed.y += -1.0,
                PhysicalKey::Code(KeyCode::ShiftLeft) => speed.y += 1.0,
                _ => (),
            }
        }

        let rotation = EMat::rotation_y(c.pitch) * EMat::rotation_x(c.yaw);
        if speed != EPos::ZERO {
            c.speed += (rotation * speed.normalize() * 40.0) * delta;
        }
        c.last_facing = rotation
            * EVec {
                x: 0.,
                y: 0.,
                z: -1.,
            };

        let s = c.speed;
        c.position += s * delta;
        c.speed *= (0.1f32).powf(delta);
        (c.position * 16., rotation)
    }

    fn input_event(&self, input: InputEvent) {
        let mut c = self.controller.write().unwrap();
        self.ui_state.write().unwrap().input_event(&input);
        match input {
            InputEvent::Key {
                physical: scancode,
                down,
                ..
            } => {
                if down {
                    c.keys_down.insert(scancode);
                } else {
                    c.keys_down.remove(&scancode);
                }
            }
            InputEvent::MouseMotion { delta } => {
                c.pitch += delta.x * -0.001;
                c.yaw += delta.y * -0.001;
            }
            InputEvent::MouseButton { button, down: true } => {
                if let Some((before, after)) = self.raycast(c.position, c.last_facing, 20.) {
                    match button {
                        MouseButton::Left => {
                            self.set_block(after, Block::Air);
                        }
                        MouseButton::Right => {
                            self.set_block(before, Block::Stone);
                        }
                        _ => (),
                    }
                }
            }
            _ => (),
        }
    }

    fn update_ui(&self, _out: &mut GameOutEvents) -> (Vec<Vertex>, Vec<u32>) {
        let mut ui_state = self.ui_state.write().unwrap();
        ui_state.draw(flow(UAxis::Vertial, |ui| {
            ui.add(label("Testing font rendering", 100.0, Color::WHITE));
            ui.add(fixed(UVec::ONE * 10., widgets::fill_rect(Color::GREEN)));
        }));
        ui_state.finish()
    }
    fn mainloop_tick(
        &self,
        _invalid: &mut Vec<Range<BPos>>,
        _alive: &mut Vec<u64>,
        out: &mut GameOutEvents,
    ) {
        out.cursor_grab = true
    }
}

impl GameWorld {
    pub fn raycast(&self, origin: EPos, direction: EPos, max_d: f32) -> Option<(BPos, BPos)> {
        let mut d = 0.;
        let step = 0.1;
        while d < max_d {
            let p = origin + direction * d;
            let block = self.get(p.bpos());

            if block.alpha > 127 {
                return Some(((p - direction * step).bpos(), p.bpos()));
            }

            d += step;
        }
        None
    }
    pub fn set_block(&self, p: BPos, block: Block) {
        self.block_override.write().unwrap().insert(p, block);
        self.invalid.write().unwrap().insert(p);
    }
}

impl State {
    pub fn new() -> Arc<Self> {
        let world = Arc::new(GameWorld {
            worldgen: Worldgen::new(),
            start: Instant::now(),
            block_defs: block_defs(),
            ui_state: UiState::new(Theme::default()).into(),
            block_override: Default::default(),
            invalid: Default::default(),
            controller: Controller {
                position: EPos::ONE,
                pitch: 0.,
                yaw: 0.,
                last_facing: EVec::ONE,
                keys_down: Default::default(),
                speed: EVec::ZERO,
            }
            .into(),
        });
        Arc::new(Self { _world: world })
    }
}

fn main() {
    init_logging();
    let state = State::new();
    Graphics::run(state._world.clone());
}
