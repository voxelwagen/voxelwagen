/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::blocks::Block;
use noise::{Fbm, MultiFractal, NoiseFn, Perlin, Turbulence};
use voxelwagen_engine::common::BPos;

pub(crate) struct Worldgen {
    height: Fbm<Perlin>,
    tree: Random,
    cave: Turbulence<Perlin, Perlin>,
}
impl Worldgen {
    pub fn new() -> Self {
        Self {
            height: Fbm::new(0)
                .set_frequency(0.05)
                .set_octaves(4)
                .set_sources(vec![
                    Perlin::new(0),
                    Perlin::new(1),
                    Perlin::new(2),
                    Perlin::new(3),
                ]),
            cave: Turbulence::new(Perlin::new(0)),
            tree: Random::new(0),
        }
    }
    pub fn generate(&self, p: BPos) -> Block {
        use Block::*;
        let pf = p.f64();

        let height = (self.height.get([pf.x / 10., pf.z / 10.]) * 20. - 5.) as i64;
        let cave = self.cave.get((pf * 0.01).array()) > 0.5;
        let tree = self.tree.get(p.x, p.z, 0) < 0.002;
        let sea = height < 0;

        match p.y - height {
            0.. if sea && p.y < 0 => Water,
            ..=10 if cave => Air,
            ..=-2 => Stone,
            -1 => Dirt,
            0 if !sea => Grass,
            1..=5 if tree && !sea => Log,
            6..=7 if tree && !sea => Leaves,
            _ => Air,
        }
    }
}

struct Random(u32);
impl Random {
    pub fn new(seed: u32) -> Self {
        Random(seed)
    }
    pub fn get(&self, x: i64, y: i64, z: i64) -> f32 {
        pub fn xorshift(mut x: u32) -> u32 {
            x ^= x << 13;
            x ^= x >> 17;
            x ^= x << 5;
            x
        }
        let k = xorshift(self.0 ^ xorshift(x as u32 ^ xorshift(y as u32 ^ xorshift(z as u32))));
        k as f32 / u32::MAX as f32
    }
}
