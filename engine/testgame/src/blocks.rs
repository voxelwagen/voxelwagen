/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use voxelwagen_engine::common::{model::Model, BlockDefinition, Material};

pub fn block_defs() -> Vec<BlockDefinition> {
    // let atlas = image::open("assets/atlas.png").unwrap().into_rgba8();
    let atlas = image::load_from_memory(include_bytes!("../assets/atlas.png"))
        .unwrap()
        .into_rgba8();
    BLOCK_DEFS
        .iter()
        .map(|e| {
            let model = match e {
                BD::Texture(e) => {
                    let mut tex: [[[u8; 4]; 16]; 16] = Default::default();
                    #[allow(clippy::needless_range_loop)]
                    for x in 0..16 {
                        for y in 0..16 {
                            tex[x][y] = atlas
                                .get_pixel(x as u32 + e[0] * 16, y as u32 + e[1] * 16)
                                .0
                        }
                    }
                    let mut model =
                        Model::from_textures(&[tex, tex, tex, tex, tex, tex], true, 0, 0);
                    model.fill_internal_spaces();
                    model
                }
                BD::Mat(e) => Model([[[*e; 16]; 16]; 16]),
            };
            BlockDefinition {
                name: String::new(),
                model,
            }
        })
        .collect()
}

enum BD {
    Texture([u32; 2]),
    Mat(Material),
}

#[derive(Debug, Clone, Copy)]
pub enum Block {
    Air = 0,
    Stone = 1,
    Dirt = 2,
    Grass = 3,
    Log = 4,
    Leaves = 5,
    Water = 6,
}

const BLOCK_DEFS: &[BD] = &[
    BD::Mat(Material::AIR), // air
    BD::Texture([0, 0]),    // stone
    BD::Texture([1, 0]),    // dirt
    BD::Texture([2, 0]),    // grass
    BD::Texture([4, 0]),    // log
    BD::Texture([5, 0]),    // leaves
    BD::Mat(Material {
        // water
        color: [0, 0, 30],
        alpha: 180,
        roughness: 1,
        flags: Material::FLAG_FLUID,
        emission: 0,
        _pad: 0,
    }),
];
