/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
pub use voxelwagen_dyndraw as dyndraw;
pub use voxelwagen_renderer_common as defs;

use defs::{InputEvent, Renderer};
use log::debug;
use pollster::FutureExt;
use std::{
    ops::Range,
    sync::{Arc, RwLock},
};
use voxelwagen_common::{BPos, Block, EMat, EPos, Material, UVec};
use voxelwagen_dyndraw::{pipeline::DynamicRenderingPipeline, Vertex};
use voxelwagen_renderer_common::{RendererCallbacks, RendererNew};
use voxelwagen_renderer_mesh::MeshViewer;
use voxelwagen_renderer_rt::RtViewer;
use voxelwagen_world::World;
use wgpu::{Device, Features, InstanceDescriptor, Limits, Queue, Surface, SurfaceConfiguration};
use winit::{
    application::ApplicationHandler,
    dpi::{PhysicalPosition, PhysicalSize},
    event::{DeviceEvent, ElementState, WindowEvent},
    event_loop::{ActiveEventLoop, EventLoop},
    window::{CursorGrabMode, Window},
};

pub trait GraphicsCallbacks: World<Material> + Send + Sync + 'static {
    fn get_sprite(&self, id: u64) -> Option<(Block, EPos, EMat)>;
    fn get_camera(&self, delta: f32) -> (EPos, EMat);
    fn update_ui(&self, out: &mut GameOutEvents) -> (Vec<Vertex>, Vec<u32>);
    fn input_event(&self, input: InputEvent);
    fn mainloop_tick(
        &self,
        invalid_blocks: &mut Vec<Range<BPos>>,
        invalid_sprites: &mut Vec<u64>,
        out: &mut GameOutEvents,
    );
}

pub struct GameOutEvents {
    pub cursor_grab: bool,
    pub renderer_stats: String,
}

pub struct Graphics {
    inner: Option<GraphicsInner>,
    callbacks: Arc<dyn GraphicsCallbacks>,
}

pub struct GraphicsInner {
    device: Arc<Device>,
    queue: Arc<Queue>,
    window: Window,
    surface: Surface<'static>,
    surface_config: RwLock<SurfaceConfiguration>,

    callbacks: Arc<dyn GraphicsCallbacks>,

    renderer: Box<dyn Renderer>,
    overlays: DynamicRenderingPipeline,
    impl_out: GameOutEvents,
    invalid_entities: Vec<u64>,
}
impl Graphics {
    pub fn run(callbacks: Arc<dyn GraphicsCallbacks>) {
        let event_loop = EventLoop::new().unwrap();
        event_loop
            .run_app(&mut Self {
                callbacks,
                inner: None,
            })
            .unwrap()
    }
}

impl GraphicsInner {
    pub fn new(window: Window, callbacks: Arc<dyn GraphicsCallbacks>) -> Self {
        #[cfg(target_arch = "wasm32")]
        {
            use winit::platform::web::WindowExtWebSys;
            web_sys::window()
                .and_then(|win| win.document())
                .and_then(|doc| doc.body())
                .and_then(|body| {
                    body.append_child(&web_sys::Element::from(window.canvas()))
                        .ok()
                })
                .expect("couldn't append canvas to document body");
        }

        let size = window.inner_size();

        let backends = wgpu::util::backend_bits_from_env().unwrap_or_else(wgpu::Backends::all);
        let instance = wgpu::Instance::new(InstanceDescriptor {
            backends,
            ..Default::default()
        });
        let surface = instance
            .create_surface(unsafe { std::mem::transmute::<&Window, &'static Window>(&window) })
            .unwrap();
        let adapter = wgpu::util::initialize_adapter_from_env_or_default(&instance, Some(&surface))
            .block_on()
            .expect("Failed to find an appropriate adapter");

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: None,
                    required_features: Features::PUSH_CONSTANTS
                        | Features::POLYGON_MODE_LINE
                        | Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
                        | Features::MAPPABLE_PRIMARY_BUFFERS,
                    #[cfg(target_arch = "wasm32")]
                    required_limits: Limits::downlevel_webgl2_defaults(),
                    #[cfg(not(target_arch = "wasm32"))]
                    required_limits: Limits {
                        max_push_constant_size: 64,
                        max_buffer_size: 1024 * 1024 * 1024 * 2 - 2,
                        max_storage_buffer_binding_size: 1024 * 1024 * 1024 * 2 - 2,
                        ..Default::default()
                    },
                    // limits: wgpu::Limits::downlevel_webgl2_defaults()
                    //     .using_resolution(adapter.limits()),
                },
                None,
            )
            .block_on()
            .expect("Failed to create device");
        let device = Arc::new(device);
        let queue = Arc::new(queue);

        let swapchain_capabilities = surface.get_capabilities(&adapter);
        let swapchain_format = swapchain_capabilities.formats[0];

        let surface_config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: swapchain_format,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
            alpha_mode: swapchain_capabilities.alpha_modes[0],
            view_formats: vec![],
            desired_maximum_frame_latency: 2,
        };

        let rcbs = Arc::new(ConvertCallbacks(callbacks.clone())) as Arc<dyn RendererCallbacks>;
        let mut renderer = match std::env::var("RENDERER")
            .unwrap_or("mesh".to_string())
            .as_str()
        {
            "mesh" => Box::new(MeshViewer::new(rcbs, &device, &queue, &surface_config))
                as Box<dyn Renderer>,
            "rt" => {
                Box::new(RtViewer::new(rcbs, &device, &queue, &surface_config)) as Box<dyn Renderer>
            }
            _ => panic!("unknown renderer"),
        };
        let overlays = DynamicRenderingPipeline::new(&device, surface_config.format);

        surface.configure(&device, &surface_config);
        renderer.reconfigure(&surface_config);
        callbacks.input_event(InputEvent::Resize {
            width: surface_config.width,
            height: surface_config.width,
        });

        Self {
            invalid_entities: vec![],
            renderer,
            callbacks,
            impl_out: GameOutEvents {
                cursor_grab: false,
                renderer_stats: String::new(),
            },
            overlays,
            window,
            surface,
            surface_config: surface_config.into(),
            device,
            queue,
            // event_loop: RwLock::new(Some(SendAnyway(event_loop))),
        }
    }

    fn resize(&mut self, size: PhysicalSize<u32>) {
        if size.width > 0 && size.height > 0 {
            debug!("resize {size:?}");
            let mut c = self.surface_config.write().unwrap();
            c.width = size.width;
            c.height = size.height;
            self.surface.configure(&self.device, &c);
            self.renderer.reconfigure(&c);
            self.callbacks.input_event(InputEvent::Resize {
                width: c.width,
                height: c.height,
            });
        }
    }

    pub fn set_focus(&self, state: bool) {
        let _ = self.window.set_cursor_grab(if state {
            CursorGrabMode::Locked
        } else {
            CursorGrabMode::None
        });
        self.window.set_cursor_visible(!state);
    }

    fn render(&mut self) {
        let frame = match self.surface.get_current_texture() {
            Ok(f) => f,
            Err(_e) => {
                // warn!("{e}");
                return;
            }
        };
        let view = frame
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

        {
            let (vba, iba) = self.callbacks.update_ui(&mut self.impl_out);
            self.overlays
                .prepare(&self.device, &self.queue, &mut encoder, &vba, &iba);
        }

        self.renderer.render(&mut encoder, &view);
        self.overlays.renderpass(&mut encoder, &view);

        self.renderer.pre_submit();
        self.queue.submit(Some(encoder.finish()));
        frame.present();
        self.renderer.post_submit();
        self.impl_out.renderer_stats.clear();
        self.renderer.stats(&mut self.impl_out.renderer_stats)
    }
}

impl ApplicationHandler for Graphics {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        let window_attributes = Window::default_attributes().with_title("A fantastic window!");
        self.inner = Some(GraphicsInner::new(
            event_loop.create_window(window_attributes).unwrap(),
            self.callbacks.clone(),
        ));
    }
    fn device_event(
        &mut self,
        _event_loop: &ActiveEventLoop,
        _device_id: winit::event::DeviceId,
        event: DeviceEvent,
    ) {
        if let DeviceEvent::MouseMotion { delta: (x, y) } = event {
            self.callbacks.input_event(InputEvent::MouseMotion {
                delta: UVec {
                    x: x as f32,
                    y: y as f32,
                },
            })
        }
    }
    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        window_id: winit::window::WindowId,
        event: WindowEvent,
    ) {
        let d = self.inner.as_mut().unwrap();
        if window_id != d.window.id() {
            return;
        }
        match event {
            WindowEvent::CloseRequested => event_loop.exit(),
            WindowEvent::Resized(size) => d.resize(size),
            WindowEvent::Focused(true) | WindowEvent::CursorEntered { .. } => {
                d.set_focus(d.impl_out.cursor_grab)
            }
            WindowEvent::Focused(false) | WindowEvent::CursorLeft { .. } => d.set_focus(false),
            WindowEvent::KeyboardInput { event, .. } => {
                self.callbacks.input_event(InputEvent::Key {
                    physical: event.physical_key,
                    semantic: event.logical_key.clone(),
                    down: event.state == ElementState::Pressed,
                });
            }
            WindowEvent::MouseInput { state, button, .. } => {
                self.callbacks.input_event(InputEvent::MouseButton {
                    button,
                    down: state == ElementState::Pressed,
                });
            }
            WindowEvent::CursorMoved { position, .. } => {
                self.callbacks.input_event(InputEvent::MousePosition {
                    position: UVec {
                        x: position.x as f32,
                        y: position.y as f32,
                    },
                });
            }
            WindowEvent::MouseWheel { delta, .. } => match delta {
                winit::event::MouseScrollDelta::LineDelta(x, y) => self
                    .callbacks
                    .input_event(InputEvent::MouseWheel(UVec { x, y })),
                winit::event::MouseScrollDelta::PixelDelta(PhysicalPosition { x, y }) => {
                    self.callbacks.input_event(InputEvent::MouseWheel(UVec {
                        x: x as f32,
                        y: y as f32,
                    }))
                }
            },
            WindowEvent::RedrawRequested => d.render(),
            _ => {}
        }
    }
    fn about_to_wait(&mut self, _event_loop: &ActiveEventLoop) {
        let d = self.inner.as_mut().unwrap();
        d.window.request_redraw();

        let mut i = Vec::new();
        let old_grab = d.impl_out.cursor_grab;
        d.callbacks
            .mainloop_tick(&mut i, &mut d.invalid_entities, &mut d.impl_out);
        if d.impl_out.cursor_grab != old_grab {
            d.set_focus(d.impl_out.cursor_grab);
        }
        i.into_iter().for_each(|r| d.renderer.invalidate_blocks(r));
        d.renderer.invalidate_sprites(&d.invalid_entities);
        d.invalid_entities.clear();
    }
}

pub struct ConvertCallbacks(Arc<dyn GraphicsCallbacks>);
impl RendererCallbacks for ConvertCallbacks {
    #[inline]
    fn get_sprite(&self, id: u64) -> Option<(Block, EPos, EMat)> {
        self.0.get_sprite(id)
    }
    #[inline]
    fn get_camera(&self, delta: f32) -> (EPos, EMat) {
        self.0.get_camera(delta)
    }
}
impl World<Material> for ConvertCallbacks {
    fn get(&self, p: BPos) -> Material {
        self.0.get(p)
    }
    fn get_lod(&self, p: BPos, lod: usize) -> Material {
        self.0.get_lod(p, lod)
    }
    fn get_chunk(
        &self,
        range: Range<BPos>,
        lod: usize,
    ) -> voxelwagen_world::preload::PreloadedWorld<Material> {
        self.0.get_chunk(range, lod)
    }
}
