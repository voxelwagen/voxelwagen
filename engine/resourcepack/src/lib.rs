/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(iterator_try_collect)]
use anyhow::{anyhow, bail};
use bincode::{Decode, Encode};
use log::info;
use std::{
    collections::HashMap,
    fs::File,
    io::{BufReader, BufWriter, Read, Write},
    path::Path,
};
use voxelwagen_common::BlockDefinition;

const RESOURCEPACK_MAGIC: [u8; 8] = [0xAD, 0x04, 0x95, 0x6B, b'V', b'W', b'R', b'P'];
const RESOURCEPACK_VERSION: u32 = 1;

#[derive(Clone, Default)]
pub struct ResourcePack {
    pub entries: HashMap<String, Resource>,
}

#[derive(Clone, Encode, Decode)]
pub enum Resource {
    Block(BlockDefinition),
}

type SerType = Vec<(String, Resource)>;

impl ResourcePack {
    pub fn load(&mut self, path: impl AsRef<Path>) -> anyhow::Result<()> {
        let path = path.as_ref();
        info!("loading resource pack from {path:?}");
        let mut file = File::open(path)?;

        let mut buf = [0u8; 8 + 4];
        file.read_exact(&mut buf)?;
        if buf[0..8] != RESOURCEPACK_MAGIC {
            bail!("invalid magic")
        }
        if buf[8..12] != RESOURCEPACK_VERSION.to_le_bytes() {
            bail!("unsupported pack version")
        }

        let file = zstd::stream::read::Decoder::new(file)?;
        let mut file = BufReader::new(file);
        let entries =
            bincode::decode_from_std_read::<SerType, _, _>(&mut file, bincode::config::standard())?;
        let len = entries.len();
        for (idn, res) in entries {
            self.add(&idn, res)
        }
        info!("done, {} resources loaded", len);
        Ok(())
    }
    pub fn save(self, path: &Path) -> anyhow::Result<()> {
        info!("saving resource pack to {path:?}");
        let mut file = File::create(path)?;
        file.write_all(&RESOURCEPACK_MAGIC)?;
        file.write_all(&RESOURCEPACK_VERSION.to_le_bytes())?;

        let out = self.entries.into_iter().collect::<SerType>();
        info!("saving {} resources", out.len());

        let mut zfile = zstd::stream::write::Encoder::new(file, 15)?;
        let mut file = BufWriter::new(&mut zfile);
        bincode::encode_into_std_write::<SerType, _, _>(
            out,
            &mut file,
            bincode::config::standard(),
        )?;
        drop(file);
        zfile.finish()?;
        info!("done");
        Ok(())
    }

    pub fn add(&mut self, idn: &str, res: Resource) {
        self.entries.insert(idn.to_owned(), res);
    }
    pub fn get<'a>(&'a self, idn: &str) -> Option<&'a Resource> {
        self.entries.get(idn)
    }
}

impl ResourcePack {
    pub fn blockdefs(
        &self,
        idns: &[&str],
        fallback: Option<BlockDefinition>,
    ) -> anyhow::Result<Vec<BlockDefinition>> {
        idns.iter()
            .map(|id| {
                Ok(
                    match self
                        .get(id)
                        .or(fallback.clone().map(Resource::Block).as_ref())
                        .ok_or(anyhow!("cannot find {id}"))?
                    {
                        Resource::Block(b) => b.to_owned(),
                    },
                )
            })
            .try_collect()
    }
}

#[macro_export]
macro_rules! define_blocks {
    (enum $i:ident { $($name:ident = $idn:literal),* }) => {
        #[derive(Debug, Clone, Encode, Decode, PartialEq, Eq)]
        pub enum $i { $($name),* }
        impl $i { fn idns() -> &'static [&'static str] {
            &[$($idn),*]
        }}
    };
}
