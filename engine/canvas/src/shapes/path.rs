/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use voxelwagen_common::UPos;

pub trait PathSink {
    fn point(&mut self, p: UPos);
    fn close(&mut self);
}

pub struct PathBuilder<T> {
    out: T,
    cursor: UPos,
}
impl<T: PathSink> PathBuilder<T> {
    pub fn new(out: T) -> Self {
        PathBuilder {
            out,
            cursor: UPos::ZERO,
        }
    }
    #[inline]
    pub fn move_to(&mut self, p: UPos) {
        self.out.close();
        self.cursor = p;
        self.out.point(p);
    }
    #[inline]
    pub fn line_to(&mut self, p: UPos) {
        self.cursor = p;
        self.out.point(p);
    }

    pub fn bezier_quadratic_to(&mut self, c2: UPos, c3: UPos) {
        let samples = 10;
        let c1 = self.cursor;
        for i in 1..=samples {
            let t = (i as f32) / samples as f32;
            let u = 1. - t;
            self.out
                .point(c1 * (u * u) + c2 * (2. * u * t) + c3 * (t * t))
        }
        self.cursor = c3;
    }
    pub fn bezier_cubic_to(&mut self, c2: UPos, c3: UPos, c4: UPos) {
        let samples = 10;
        let c1 = self.cursor;
        for i in 1..=samples {
            let t = (i as f32) / samples as f32;
            let u = 1. - t;
            let t2 = t * t;
            let u2 = u * u;
            self.out
                .point(c1 * (u2 * u) + c2 * (3. * u2 * t) + c3 * (3. * u * t2) + c4 * (t2 * t))
        }
        self.cursor = c4;
    }

    #[inline]
    pub fn close(&mut self) {
        self.out.close();
    }
}
