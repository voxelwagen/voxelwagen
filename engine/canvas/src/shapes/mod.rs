/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
pub mod path;
pub mod triangulate;

use voxelwagen_common::{UPos, UVec};

pub trait ShapeSurface {
    fn vertex(&mut self, p: UPos) -> u32;
    fn tri(&mut self, index: [u32; 3]);
}

impl<T: ShapeSurface> ShapeSurface for &mut T {
    #[inline]
    fn vertex(&mut self, p: UPos) -> u32 {
        (*self).vertex(p)
    }
    #[inline]
    fn tri(&mut self, index: [u32; 3]) {
        (*self).tri(index)
    }
}

#[derive(Default, Debug)]
pub struct ShapeCanvas {
    vertex: Vec<UPos>,
    index: Vec<[u32; 3]>,
}

impl ShapeSurface for ShapeCanvas {
    #[inline]
    fn vertex(&mut self, p: UPos) -> u32 {
        let i = self.vertex.len();
        self.vertex.push(p);
        i as u32
    }
    #[inline]
    fn tri(&mut self, i: [u32; 3]) {
        self.index.push(i);
    }
}
impl ShapeCanvas {
    pub fn write(&self, mut target: impl ShapeSurface) {
        let mut new_index = Vec::new();
        for v in &self.vertex {
            new_index.push(target.vertex(*v))
        }
        for i in &self.index {
            target.tri(*i)
        }
    }
    pub fn translate_in_place(&mut self, offset: UVec) {
        self.vertex.iter_mut().for_each(|v| *v += offset)
    }
    pub fn scale_in_place(&mut self, scale: f32) {
        self.vertex.iter_mut().for_each(|v| *v *= scale)
    }
    pub fn write_transformed(&self, mut target: impl ShapeSurface, offset: UVec, scale: f32) {
        let mut new_index = Vec::new();
        for v in &self.vertex {
            new_index.push(target.vertex(*v * scale + offset))
        }
        for i in &self.index {
            target.tri([
                new_index[i[0] as usize],
                new_index[i[1] as usize],
                new_index[i[2] as usize],
            ])
        }
    }
}
