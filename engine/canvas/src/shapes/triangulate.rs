/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::{path::PathSink, ShapeSurface};
use voxelwagen_common::UPos;

pub struct PolygonFan<T> {
    first: Option<u32>,
    last: Option<u32>,
    target: T,
}

impl<T: ShapeSurface> PolygonFan<T> {
    pub fn new(target: T) -> Self {
        Self {
            first: None,
            last: None,
            target,
        }
    }
}
impl<T: ShapeSurface> PathSink for PolygonFan<T> {
    fn point(&mut self, p: UPos) {
        if let Some(first) = self.first {
            let v = self.target.vertex(p);

            if let Some(last) = self.last {
                self.target.tri([first, last, v]);
            }
            self.last = Some(v);
        } else {
            self.first = Some(self.target.vertex(p));
        }
    }
    fn close(&mut self) {
        self.first = None;
        self.last = None;
    }
}

pub struct PolygonEarclip<T> {
    points: Vec<UPos>,
    target: T,
}
impl<T: ShapeSurface> PolygonEarclip<T> {
    pub fn new(target: T) -> Self {
        Self {
            points: Default::default(),
            target,
        }
    }
}
impl<T: ShapeSurface> PathSink for PolygonEarclip<T> {
    fn point(&mut self, p: UPos) {
        self.points.push(p);
    }
    fn close(&mut self) {
        let np = self.points.len();
        let mut vertex_order = (0..np).collect::<Vec<_>>();
        vertex_order.sort_by_key(|i| (self.points[*i].x * 100000.) as i64);
        let mut vertex_index = self
            .points
            .iter()
            .map(|p| Some(self.target.vertex(*p)))
            .collect::<Vec<_>>();

        // last two verts are covered by the third-last vertecie's triangle
        vertex_order.pop();
        vertex_order.pop();
        for e in vertex_order {
            let this = vertex_index[e].take().unwrap();
            let after = {
                let mut i = e;
                loop {
                    i += 1;
                    i %= np;
                    let e = vertex_index[i];
                    if let Some(e) = e {
                        break e;
                    }
                }
            };
            let before = {
                let mut i = e;
                loop {
                    i += np - 1;
                    i %= np;
                    let e = vertex_index[i];
                    if let Some(e) = e {
                        break e;
                    }
                }
            };
            self.target.tri([before, this, after]);
        }

        self.points.clear();
    }
}
