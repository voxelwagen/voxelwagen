/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
pub mod font;
pub mod primitives;
pub mod shapes;

use crate::shapes::ShapeSurface;
use std::ops::Range;
use voxelwagen_common::{color::Color, range::RangeExt, UPos, UVec};
use voxelwagen_dyndraw::Vertex;

pub trait Surface {
    fn vertex(&mut self, position: UPos, color: Color) -> u32;
    fn tri(&mut self, index: [u32; 3]);
}
impl<T: Surface> Surface for &mut T {
    #[inline]
    fn vertex(&mut self, p: UPos, color: Color) -> u32 {
        (*self).vertex(p, color)
    }
    #[inline]
    fn tri(&mut self, index: [u32; 3]) {
        (*self).tri(index)
    }
}

#[derive(Clone, Default)]
pub struct Canvas {
    vba: Vec<Vertex>,
    iba: Vec<u32>,
}

impl Canvas {
    #[inline]
    pub fn clear(&mut self) {
        self.iba.clear();
        self.vba.clear();
    }
    pub fn finish(self) -> (Vec<Vertex>, Vec<u32>) {
        (self.vba, self.iba)
    }
}
impl Surface for Canvas {
    #[inline]
    fn vertex(&mut self, position: UPos, color: Color) -> u32 {
        let i = self.vba.len() as u32;
        self.vba.push(Vertex {
            position: position.array(),
            color: color.array(),
        });
        i
    }
    #[inline]
    fn tri(&mut self, index: [u32; 3]) {
        self.iba.extend(index)
    }
}

pub struct ShapeColor<'a, T>(pub Color, pub &'a mut T);
impl<T: Surface> ShapeSurface for ShapeColor<'_, T> {
    #[inline]
    fn vertex(&mut self, p: UPos) -> u32 {
        self.1.vertex(p, self.0)
    }
    #[inline]
    fn tri(&mut self, index: [u32; 3]) {
        self.1.tri(index)
    }
}

pub struct TransformSurface<T> {
    pub offset: UVec,
    pub scale: UVec,
    pub target: T,
}
impl<T> TransformSurface<T> {
    pub fn new_from_to(target: T, from: Range<UPos>, to: Range<UPos>) -> Self {
        let fsize = from.size();
        let tsize = to.size();
        let scale = tsize / fsize;
        let offset = to.start + (from.start * scale);
        Self {
            offset,
            scale,
            target,
        }
    }
}
impl<T: Surface> Surface for TransformSurface<T> {
    #[inline]
    fn vertex(&mut self, position: UPos, color: Color) -> u32 {
        self.target
            .vertex((position * self.scale) + self.offset, color)
    }
    #[inline]
    fn tri(&mut self, index: [u32; 3]) {
        self.target.tri(index)
    }
}
impl<T: ShapeSurface> ShapeSurface for TransformSurface<T> {
    #[inline]
    fn vertex(&mut self, position: UPos) -> u32 {
        self.target.vertex((position * self.scale) + self.offset)
    }
    #[inline]
    fn tri(&mut self, index: [u32; 3]) {
        self.target.tri(index)
    }
}
