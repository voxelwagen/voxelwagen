/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::Surface;
use voxelwagen_common::{color::Color, UPos, UVec};

pub trait SurfacePrimitives {
    fn rect(&mut self, p1: UPos, p2: UPos, color: Color);
}

impl<T: Surface> SurfacePrimitives for T {
    fn rect(&mut self, p1: UPos, p2: UPos, color: Color) {
        let v = [
            self.vertex(UVec { x: p1.x, y: p1.y }, color),
            self.vertex(UVec { x: p2.x, y: p1.y }, color),
            self.vertex(UVec { x: p1.x, y: p2.y }, color),
            self.vertex(UVec { x: p2.x, y: p2.y }, color),
        ];
        self.tri([0, 2, 1].map(|e| v[e]));
        self.tri([1, 2, 3].map(|e| v[e]));
    }
}
