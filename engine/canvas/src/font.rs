/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use super::shapes::{
    path::{PathBuilder, PathSink},
    triangulate::PolygonEarclip,
    ShapeCanvas, ShapeSurface,
};
use crate::TransformSurface;
use log::debug;
use std::collections::HashMap;
use ttf_parser::{Face, OutlineBuilder};
use voxelwagen_common::{UPos, UVec};

pub struct Font {
    render_cache: HashMap<char, (ShapeCanvas, f32)>,
    face: Face<'static>,
}

impl Font {
    pub fn load(data: &'static [u8]) -> Self {
        let face = Face::parse(data, 0).unwrap();
        Font {
            face,
            render_cache: Default::default(),
        }
    }

    pub fn text(
        &mut self,
        mut target: impl ShapeSurface,
        position: UPos,
        scale: f32,
        kerning: f32,
        text: &str,
    ) -> UVec {
        let mut off = UVec::ZERO;
        let mut max_x = 0f32;
        for c in text.chars() {
            if c == '\n' {
                off.y += scale;
                max_x = max_x.max(off.x);
                off.x = 0.;
                continue;
            }
            let (shape, width) = self
                .render_cache
                .entry(c)
                .or_insert_with(|| load_glyph(&self.face, c));
            shape.write_transformed(&mut target, position + off, scale);
            off.x += (*width + kerning) * scale;
        }
        off.y += scale;
        UVec { y: off.y, x: max_x }
    }
}

fn load_glyph(face: &Face<'static>, c: char) -> (ShapeCanvas, f32) {
    if c == ' ' {
        return (ShapeCanvas::default(), 0.);
    }
    debug!("loading glyph for {c:?}");
    let id = face
        .glyph_index(c)
        .expect("font face doesnt have the glyph we need :(");
    let bb = face.glyph_bounding_box(id);

    let upm = face.units_per_em() as f32;
    let scale = 1. / upm;
    let width = bb
        .map(|bb| (bb.x_max as f32 - bb.x_min as f32) * scale)
        .unwrap_or(0.5);

    let mut canvas = ShapeCanvas::default();
    let mut builder = GlyphBuilder {
        c: &mut PathBuilder::new(PolygonEarclip::new(TransformSurface::new_from_to(
            &mut canvas,
            (UVec {
                x: 0.,
                y: face.descender() as f32 * 2.8, // TODO: fudge factor
            })..(UVec {
                x: upm,
                y: -upm + face.descender() as f32 * 2.8,
            }),
            UVec { x: 0., y: 0. }..UVec { x: 1., y: 1. },
        ))),
    };
    face.outline_glyph(id, &mut builder).unwrap();

    (canvas, width)
}

struct GlyphBuilder<'a, T> {
    c: &'a mut PathBuilder<T>,
}
impl<T: PathSink> OutlineBuilder for GlyphBuilder<'_, T> {
    fn move_to(&mut self, x: f32, y: f32) {
        self.c.move_to(UVec { x, y })
    }
    fn line_to(&mut self, x: f32, y: f32) {
        self.c.line_to(UVec { x, y })
    }
    fn quad_to(&mut self, x1: f32, y1: f32, x: f32, y: f32) {
        self.c
            .bezier_quadratic_to(UVec { x: x1, y: y1 }, UVec { x, y })
    }
    fn curve_to(&mut self, x1: f32, y1: f32, x2: f32, y2: f32, x: f32, y: f32) {
        self.c
            .bezier_cubic_to(UVec { x: x1, y: y1 }, UVec { x: x2, y: y2 }, UVec { x, y })
    }
    fn close(&mut self) {
        self.c.close();
    }
}
