/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use voxelwagen_engine::common::Material;

pub fn palette() -> Vec<Material> {
    let mut o = vec![];
    for r in 0..4 {
        for g in 0..4 {
            for b in 0..4 {
                o.push(Material {
                    color: [r * 64, g * 64, b * 64],
                    alpha: 255,
                    roughness: 128,
                    emission: 0,
                    ..Default::default()
                })
            }
        }
    }
    o
}
