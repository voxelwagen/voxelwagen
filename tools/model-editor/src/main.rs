/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
pub mod palette;

use log::info;
use palette::palette;
use parking_lot::RwLock;
use std::{collections::HashSet, ops::Range, sync::Arc};
use voxelwagen_engine::{
    common::{color::Color, compat::init_logging, BPos, EMat, EPos, EVec, Material, UAxis, UVec},
    graphics::{
        defs::{InputEvent, KeyCode, MouseButton, PhysicalKey},
        dyndraw::Vertex,
        GameOutEvents, Graphics, GraphicsCallbacks,
    },
    ui::{input::InputState, layout::flow, state::UiState, theme::Theme, widgets, Container},
    world::World,
};

struct GameWorld {
    state: RwLock<State>,
}

struct State {
    controller: Controller,
    model: ModelWorld,
    invalid: HashSet<BPos>,
    ingame_input: InputState,
    palette: Vec<Material>,
    ui_state: UiState,
    grab: bool,
    active_material: Material,
}

#[derive(Default)]
pub struct ModelWorld([[[Material; 16]; 16]; 16]);

pub struct Controller {
    position: EPos,
    pitch: f32,
    yaw: f32,
    speed: EVec,
    last_facing: EVec,
}

impl World<Material> for GameWorld {
    fn get(&self, p: BPos) -> Material {
        self.state.write().model.get(p).unwrap_or(Material::AIR)
    }
}

impl GraphicsCallbacks for GameWorld {
    fn get_sprite(&self, _id: u64) -> Option<(u32, EPos, EMat)> {
        None
    }
    fn get_camera(&self, delta: f32) -> (EPos, EMat) {
        let mut st = self.state.write();

        let mut speed = EPos::ZERO;
        for k in &st.ingame_input.physical_keys.pressed {
            match *k {
                PhysicalKey::Code(KeyCode::KeyW) => speed.z += -1.0,
                PhysicalKey::Code(KeyCode::KeyS) => speed.z += 1.0,
                PhysicalKey::Code(KeyCode::KeyA) => speed.x += -1.0,
                PhysicalKey::Code(KeyCode::KeyD) => speed.x += 1.0,
                PhysicalKey::Code(KeyCode::ControlLeft) => speed.y += -1.0,
                PhysicalKey::Code(KeyCode::ShiftLeft) => speed.y += 1.0,
                _ => (),
            }
        }
        st.ingame_input.tick();

        let rotation = EMat::rotation_y(st.controller.pitch) * EMat::rotation_x(st.controller.yaw);
        if speed != EPos::ZERO {
            st.controller.speed += (rotation * speed.normalize() * 40.0) * delta;
        }
        st.controller.last_facing = rotation
            * EVec {
                x: 0.,
                y: 0.,
                z: -1.,
            };

        let s = st.controller.speed;
        st.controller.position += s * delta;
        st.controller.speed *= (0.1f32).powf(delta);
        (st.controller.position, rotation)
    }

    fn input_event(&self, input: InputEvent) {
        let mut st = self.state.write();
        if matches!(
            input,
            InputEvent::Key {
                physical: PhysicalKey::Code(KeyCode::Escape),
                down: true,
                ..
            }
        ) {
            st.grab = !st.grab;
        }
        if !st.grab {
            st.ui_state.input_event(&input);
        } else {
            st.ingame_input.handle(&input);
            match input {
                InputEvent::MouseMotion { delta } => {
                    st.controller.pitch += delta.x * -0.001;
                    st.controller.yaw += delta.y * -0.001;
                }
                InputEvent::MouseButton { button, down: true } => {
                    if let Some((before, after)) =
                        st.raycast(st.controller.position, st.controller.last_facing, 20.)
                    {
                        info!("raycast {before} {after}");
                        match button {
                            MouseButton::Left => {
                                st.set_block(after, Material::AIR);
                            }
                            MouseButton::Right => {
                                let p = st.active_material;
                                st.set_block(before, p);
                            }
                            _ => (),
                        }
                    }
                }
                _ => (),
            }
        }
    }

    fn update_ui(&self, _out: &mut GameOutEvents) -> (Vec<Vertex>, Vec<u32>) {
        let mut st = self.state.write();
        st.ui();
        st.ui_state.finish()
    }

    fn mainloop_tick(
        &self,
        invalid: &mut Vec<Range<BPos>>,
        _invalid_sprites: &mut Vec<u64>,
        out: &mut GameOutEvents,
    ) {
        let mut st = self.state.write();
        invalid.extend(st.invalid.drain().map(|e| e..e + BPos::ONE));
        out.cursor_grab = st.grab;
    }
}

impl State {
    pub fn ui(&mut self) {
        self.ui_state.draw(widgets::pad(
            10.,
            flow(UAxis::Vertial, |ui| {
                ui.add(widgets::label(
                    format!(
                        "{} {} {}",
                        self.controller.position, self.controller.pitch, self.controller.yaw
                    ),
                    100.0,
                    Color::WHITE,
                ));
                for row in self.palette.chunks(4) {
                    ui.add(flow(UAxis::Horizontal, |ui| {
                        for c in row.iter() {
                            let p = if *c == self.active_material {
                                (ui.ui.animation.time() * 10.).sin() * 2. + 5.
                            } else {
                                0.
                            };

                            let r = widgets::fixed(
                                UVec::ONE * 30.,
                                widgets::pad(
                                    p,
                                    widgets::fill_rect(Color::rgba(
                                        c.color[0], c.color[1], c.color[2], c.alpha,
                                    )),
                                ),
                            );
                            if ui.add(widgets::clickable(r)) {
                                self.active_material = *c;
                            }
                        }
                    }))
                }
            }),
        ));
    }

    pub fn raycast(&self, origin: EPos, direction: EPos, max_d: f32) -> Option<(BPos, BPos)> {
        let mut d = 0.;
        let step = 0.1;
        while d < max_d {
            let p = origin + direction * d;
            let block = self.model.get(p.bpos());

            if block.is_some() && block != Some(Material::AIR) {
                return Some(((p - direction * step).bpos(), p.bpos()));
            }

            d += step;
        }
        None
    }
    pub fn set_block(&mut self, p: BPos, mat: Material) {
        self.model.set(p, mat);
        self.invalid.insert(p);
    }
}
impl ModelWorld {
    pub fn get(&self, c: BPos) -> Option<Material> {
        self.0
            .get(c.x as usize)?
            .get(c.y as usize)?
            .get(c.z as usize)
            .copied()
    }
    pub fn set(&mut self, c: BPos, b: Material) -> Option<Material> {
        let r = self
            .0
            .get_mut(c.x as usize)?
            .get_mut(c.y as usize)?
            .get_mut(c.z as usize)?;
        let old = *r;
        *r = b;
        Some(old)
    }
}

fn main() {
    init_logging();

    let world = GameWorld {
        state: State {
            active_material: Material::AIR,
            grab: false,
            ingame_input: InputState::default(),
            model: ModelWorld(
                [[[Material {
                    alpha: 255,
                    emission: 0,
                    flags: 0,
                    color: [100, 100, 100],
                    roughness: 127,
                    _pad: 0,
                }; 16]; 16]; 16],
            ),
            ui_state: UiState::new(Theme::default()),
            invalid: Default::default(),
            palette: palette(),
            controller: Controller {
                position: EPos {
                    x: 24.,
                    y: 24.,
                    z: 24.,
                },
                pitch: 0.75,
                yaw: -0.7,
                last_facing: EVec::ONE,
                speed: EVec::ZERO,
            },
        }
        .into(),
    };
    Graphics::run(Arc::new(world));
}
