/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use clap::{Parser, Subcommand};
use std::path::PathBuf;
use voxelwagen_engine::resourcepack::ResourcePack;

#[derive(Parser)]
struct Args {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    Inspect { resourcepack: PathBuf },
}

fn main() {
    let args = Args::parse();

    match args.command {
        Command::Inspect { resourcepack } => {
            let mut rp = ResourcePack::default();
            rp.load(resourcepack).unwrap();
            for k in rp.entries.keys() {
                println!("{k:?}")
            }
        }
    }
}
