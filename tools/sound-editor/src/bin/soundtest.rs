/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use std::fs::read_to_string;
use voxelwagen_compat::init_logging;
use voxelwagen_sound::{
    compose::load_pianoletternotes,
    instruments::*,
    sequencer::{Sequencer, Span},
    synths::{stuff::gain, DynSynth},
};

fn main() {
    init_logging();

    let (sq, _) = Sequencer::new();

    // let timescale = 1. / 12.;
    let timescale = 1. / 12.;
    let events = load_pianoletternotes(&read_to_string(std::env::args().nth(1).unwrap()).unwrap())
        .into_iter()
        .map(|(sp, p)| ((sp.start * timescale)..(sp.start * timescale + 1.), p))
        .collect::<Vec<_>>();

    let _duration = events.last().unwrap().0.end;

    sq.extend(
        events
            .into_iter()
            .map(|(r, sp)| (Span(r), Box::new(guitar(sp.frequency() * 2.)) as DynSynth)),
    );

    sq.extend((0..200).map(|e| {
        let et = e as f64 * timescale * 16.;
        (Span((et)..(et + 1.)), Box::new(Drum::Hihat) as DynSynth)
    }));
    // sq.extend((0..200).map(|e| {
    //     let et = e as f64 * timescale * 4.;
    //     (Span((et)..(et + 1.)), Box::new(Drum::Clap) as DynSynth)
    // }));

    // sound::export_synth(&(Box::new(gain(0.05, sq)) as DynSynth), _duration);

    let _so = voxelwagen_sound::Sound::new(Box::new(gain(0.05, sq)));
    loop {
        std::thread::sleep(std::time::Duration::from_secs(1));
    }
}
