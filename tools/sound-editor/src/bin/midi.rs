/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(exclusive_range_pattern)]
use log::{info, warn};
use std::sync::mpsc::sync_channel;
use voxelwagen_compat::init_logging;
use voxelwagen_sound::{
    compose::Pitch,
    instruments::*,
    sequencer::{Sequencer, Span},
    synths::{stuff::gain, DynInstrument, DynSynth, Synth},
};

fn get_instrument(id: u8) -> DynInstrument {
    fn make_dyn<S: Synth>(i: impl Fn(f64) -> S + Send + Sync + 'static) -> DynInstrument {
        Box::new(move |p: Pitch| Box::new(i(p.frequency())) as DynSynth) as DynInstrument
    }
    match id {
        0 => make_dyn(piano),
        1 => make_dyn(guitar),
        2 => make_dyn(violin),
        _ => make_dyn(piano),
    }
}

fn main() {
    init_logging();
    let (sq, sq2) = Sequencer::new();
    let (sender, receiver) = sync_channel(64);

    let (client, _status) =
        jack::Client::new("voxelwagen-midi-test", jack::ClientOptions::NO_START_SERVER).unwrap();

    let shower = client.register_port("input", jack::MidiIn).unwrap();

    let cback = move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        let show_p = shower.iter(ps);
        for e in show_p {
            let _ = sender.try_send(e.bytes.to_vec());
        }
        jack::Control::Continue
    };

    let active_client = client
        .activate_async((), jack::ClosureProcessHandler::new(cback))
        .unwrap();

    let so = voxelwagen_sound::Sound::new(Box::new(gain(0.005, sq2)));

    let mut instruments = [0; 16];

    while let Ok(m) = receiver.recv() {
        info!("{m:?}");
        let channel = m.first().unwrap_or(&0) % 16;
        match m.first() {
            // set program
            Some(192..208) => {
                instruments[channel as usize] = *m.get(1).unwrap();
            }
            // set note
            Some(144..160) => {
                let freq = Pitch(*m.get(1).unwrap() as i32);
                let velocity = *m.get(2).unwrap();
                if velocity > 0 {
                    let t = so.time();
                    let instrument = get_instrument(instruments[channel as usize]);
                    let synth = instrument.note(freq);
                    sq.extend(Some((Span(t..t + 1.), synth)).into_iter());
                }
            }

            _ => warn!("unknown midi packet {m:?}"),
        }
    }

    active_client.deactivate().unwrap();
}
