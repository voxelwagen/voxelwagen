.PHONY: web
web-watch:
	CARGO_TARGET_DIR=target_web cargo watch -x 'build --target wasm32-unknown-unknown --bin voxelwagen-testgame && wasm-bindgen --target web --out-dir engine/testgame/web target_web/wasm32-unknown-unknown/debug/voxelwagen-testgame.wasm'

web:
	CARGO_TARGET_DIR=target_web cargo build --target wasm32-unknown-unknown --bin voxelwagen-testgame --release
	wasm-bindgen --target web --out-dir engine/testgame/web target_web/wasm32-unknown-unknown/release/voxelwagen-testgame.wasm
