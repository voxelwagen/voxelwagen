/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{player::Player, world::GameWorld};
use bincode::{Decode, Encode};
use log::trace;
use std::{collections::BTreeMap, fmt::Debug, time::Duration};
use voxelwagen_engine::{
    common::{vector::Vec2, BPos, EVec},
    statesync::System,
    world::cache::WorldCache,
};
use voxelwagen_game_worldgen::blocks::Block;

#[derive(Debug, Clone, Encode, Decode)]
pub enum Input {
    Join(u64),
    Leave(u64),
    Player(u64, PlayerInput),
}

#[derive(Debug, Clone, Encode, Decode)]
pub enum PlayerInput {
    Jump,
    SetMovementMode(bool),
    Movement(EVec),
    Look(Vec2<f32>),
    Break(bool),
}

#[derive(Debug)]
pub enum SideEffect {
    InvalidateBlock(BPos),
    InvalidateSprite(u64),
}

#[derive(Clone, Encode, Decode)]
pub struct State {
    pub world: WorldCache<Block, GameWorld>,
    pub players: BTreeMap<u64, Player>,
}

impl Debug for State {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("State").field(&"<opaque>").finish()
    }
}

impl Default for State {
    fn default() -> Self {
        Self {
            players: Default::default(),
            world: GameWorld::default().into(),
        }
    }
}
impl System for State {
    type Input = Input;
    type SideEffect = SideEffect;
    fn input(&mut self, input: Self::Input) -> Vec<Self::SideEffect> {
        let mut se = vec![];
        match input {
            Input::Join(id) => {
                self.players.insert(id, Player::default());
                se.push(SideEffect::InvalidateSprite(id));
            }
            Input::Leave(id) => {
                self.players.remove(&id);
                se.push(SideEffect::InvalidateSprite(id));
            }
            Input::Player(id, x) => self.players.get_mut(&id).unwrap().input(x),
        }
        se
    }
    fn tick(&mut self) -> (Duration, Vec<Self::SideEffect>) {
        trace!("tick");
        for p in self.players.values_mut() {
            p.movement_tick(&self.world.inner, 0.01);
            p.interact_tick(&mut self.world.inner, 0.01);
        }

        let se = self
            .world
            .inner
            .invalid
            .drain(..)
            .map(SideEffect::InvalidateBlock)
            .collect();

        (Duration::from_millis(10), se)
    }
}
