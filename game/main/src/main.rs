/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use log::debug;
use rand::random;
use std::{
    collections::BTreeMap,
    net::SocketAddr,
    ops::Range,
    str::FromStr,
    sync::{Arc, RwLock},
};
use voxelwagen_engine::{
    common::{
        compat::{init_logging, instant::Instant},
        range::ForEachExt,
        vector::{
            pack::{v2, v3},
            Vec2, Vec3,
        },
        BPos, BlockDefinition, EMat, EPos, EVec, Material,
    },
    graphics::{
        defs::{InputEvent, KeyCode, MouseButton, PhysicalKey},
        dyndraw, GameOutEvents, Graphics, GraphicsCallbacks,
    },
    network::{NetworkCallbacks, NetworkState},
    statesync::{ApplyEffect, Packet, StateSync},
    world::{preload::PreloadedWorld, World},
};
use voxelwagen_game::{
    front::Front,
    state::{Input, PlayerInput, SideEffect, State},
};
use voxelwagen_game_worldgen::blocks::{block_defs, Block};

struct Game {
    player_id: u64,
    network: RwLock<Option<Arc<NetworkState<Packet<State>>>>>,
    state: RwLock<StateSync<State>>,
    kstate: RwLock<KState>,
    block_defs: Vec<BlockDefinition>,
}

pub struct KState {
    front: Front,
    grab: bool,
    last_dir: EVec,
    last_update: Instant,
    last_sprite_inval: Instant,
    look_accumulator: Vec2<f32>,
}

impl NetworkCallbacks<Packet<State>> for Game {
    fn on_packet(&self, address: SocketAddr, packet: Packet<State>) {
        self.state.write().unwrap().network_in(address, packet);
    }
    fn on_establish(&self, address: SocketAddr, active: bool) {
        self.state.write().unwrap().init_peer(address, active);
        if active {
            self.state
                .write()
                .unwrap()
                .input(Input::Join(self.player_id));
        }
    }
}

impl World<Material> for Game {
    fn get(&self, p: BPos) -> Material {
        let pg: Vec3<i64> = p.array().map(|c| c / 16).into();
        let pw: Vec3<i64> = p.array().map(|c| c.rem_euclid(16)).into();
        let block = self.state.read().unwrap().head().world.get(pg);
        self.block_defs[block as usize].model.0[pw.x as usize][pw.y as usize][pw.z as usize]
    }
    fn get_chunk(
        &self,
        range: Range<BPos>,
        lod: usize,
    ) -> voxelwagen_engine::world::preload::PreloadedWorld<Material> {
        if lod >= 16 {
            PreloadedWorld::new(self, range, lod)
        } else {
            // small lod would sample every block many times. this code caches it.
            let mut w = PreloadedWorld::new_empty(range.clone(), lod);
            // let mut blocks =
            //     HashMap::with_capacity(range.volume() as usize / lod.pow(3) / 16usize.pow(3));
            let mut blocks = BTreeMap::new();
            (range.start..range.end).for_each_step(lod, |p| {
                let pg: Vec3<i64> = p.array().map(|c| c / 16).into();
                let pw: Vec3<i64> = p.array().map(|c| c.rem_euclid(16)).into();
                let block = blocks.entry(pg).or_insert_with(|| {
                    let g = self.state.read().unwrap();
                    let head = &g.head().world.inner;
                    head.get(pg)
                });
                let mat = self.block_defs[*block as usize].model.0[pw.x as usize][pw.y as usize]
                    [pw.z as usize];

                w[p] = mat;
            });
            w
        }
    }
}

impl GraphicsCallbacks for Game {
    fn get_sprite(&self, id: u64) -> Option<(u32, EPos, EMat)> {
        self.state
            .read()
            .unwrap()
            .head()
            .players
            .get(&id)
            .map(|pl| {
                (
                    Block::Player as u32,
                    pl.position - v3(0.5, 0.5, 0.5),
                    EMat::UNIT,
                )
            })
    }
    fn get_camera(&self, _delta: f32) -> (EPos, EMat) {
        let s = self.state.read().unwrap();
        let player = &s.head().players.get(&self.player_id).unwrap();
        (
            (player.position + player.rotation() * v3(0., 0., 5.)) * 16.,
            player.rotation(),
        )
    }

    fn update_ui(&self, out: &mut GameOutEvents) -> (Vec<dyndraw::Vertex>, Vec<u32>) {
        self.kstate.write().unwrap().front.update_ui(out)
    }

    fn input_event(&self, input: InputEvent) {
        let mut c = self.kstate.write().unwrap();
        c.front.input(&input);
        let send_input = |i: PlayerInput| {
            self.state
                .write()
                .unwrap()
                .input(Input::Player(self.player_id, i))
        };
        match input {
            InputEvent::MouseButton {
                button: MouseButton::Left,
                down,
            } => send_input(PlayerInput::Break(down)),
            InputEvent::MouseMotion { delta } => {
                c.look_accumulator += delta;
            }
            InputEvent::Key {
                physical: PhysicalKey::Code(KeyCode::Escape),
                down,
                ..
            } if down => c.grab = !c.grab,
            InputEvent::Key {
                physical: PhysicalKey::Code(physical),
                down,
                ..
            } if down => match physical {
                KeyCode::KeyE => send_input(PlayerInput::SetMovementMode(true)),
                KeyCode::KeyR => send_input(PlayerInput::SetMovementMode(false)),
                KeyCode::Space => send_input(PlayerInput::Jump),
                _ => (),
            },
            // InputEvent::MouseButton { button, down: true } => {
            //     if let Some((before, after)) = self.raycast(c.position, c.last_facing, 20.) {
            //         match button {
            //             MouseButton::Left => {
            //                 self.set_block(after, Block::Air);
            //             }
            //             MouseButton::Right => {
            //                 self.set_block(before, Block::Stone);
            //             }
            //             _ => (),
            //         }
            //     }
            // }
            _ => (),
        }
    }

    fn mainloop_tick(
        &self,
        invalid_blocks: &mut Vec<Range<BPos>>,
        invalid_sprites: &mut Vec<u64>,
        out: &mut GameOutEvents,
    ) {
        let mut ks = self.kstate.write().unwrap();
        out.cursor_grab = ks.grab;
        {
            if ks.last_update.elapsed().as_millis() < 10 {
                return;
            }
            ks.last_update = Instant::now();
        }
        let invalidate_all_sprites = {
            if ks.last_sprite_inval.elapsed().as_millis() > 1000 {
                ks.last_sprite_inval = Instant::now();
                true
            } else {
                false
            }
        };
        // TODO dont do this every tick
        let dir = {
            let mut dir = EPos::ZERO;
            for k in &ks.front.ui.input.physical_keys.pressed {
                match *k {
                    PhysicalKey::Code(KeyCode::KeyW) => dir.z += -1.0,
                    PhysicalKey::Code(KeyCode::KeyS) => dir.z += 1.0,
                    PhysicalKey::Code(KeyCode::KeyA) => dir.x += -1.0,
                    PhysicalKey::Code(KeyCode::KeyD) => dir.x += 1.0,
                    PhysicalKey::Code(KeyCode::ControlLeft) => dir.y += -1.0,
                    PhysicalKey::Code(KeyCode::ShiftLeft) => dir.y += 1.0,
                    _ => (),
                }
            }
            dir
        };
        let look = ks.look_accumulator * -0.001;
        ks.look_accumulator = v2(0., 0.);
        let dir_changed = (dir - ks.last_dir).length() > 0.01;
        ks.last_dir = dir;
        drop(ks);

        {
            let mut state = self.state.write().unwrap();
            if dir_changed {
                state.input(Input::Player(self.player_id, PlayerInput::Movement(dir)));
            }
            if look.length() > 0.0001 {
                state.input(Input::Player(self.player_id, PlayerInput::Look(look)));
            }
            state.tick();
            state.extract_side_effects(|e| {
                debug!("apply {e:?}");
                match e {
                    ApplyEffect::Do(SideEffect::InvalidateBlock(p))
                    | ApplyEffect::Undo(SideEffect::InvalidateBlock(p)) => {
                        invalid_blocks.push(p..p + BPos::ONE)
                    }
                    ApplyEffect::Do(SideEffect::InvalidateSprite(id))
                    | ApplyEffect::Undo(SideEffect::InvalidateSprite(id)) => {
                        invalid_sprites.push(id)
                    }
                }
            });
            // TODO this is a hack to compensate missing side effects, we should have statesync report this.
            if invalidate_all_sprites {
                invalid_sprites.extend(state.head().players.keys())
            }
        }

        loop {
            let mut state = self.state.write().unwrap();
            let packet = state.network_out();
            drop(state);
            if let Some(packet) = packet {
                self.network
                    .write()
                    .unwrap()
                    .as_mut()
                    .unwrap()
                    .broadcast(packet)
            } else {
                break;
            }
        }
    }
}

impl Game {
    pub fn new() -> Arc<Self> {
        let secret = [1, 2, 3, 4, 5];
        let game = Arc::new(Game {
            player_id: random(),
            state: StateSync::create(State::default()).into(),
            kstate: KState {
                last_dir: Default::default(),
                last_sprite_inval: Instant::now(),
                last_update: Instant::now(),
                look_accumulator: v2(0., 0.),
                grab: true,
                front: Front::new(),
            }
            .into(),
            block_defs: block_defs(),
            network: Default::default(),
        });
        *game.network.write().unwrap() = Some(NetworkState::new(secret.into(), game.clone()));
        game.state
            .write()
            .unwrap()
            .input(Input::Join(game.player_id));
        game
    }
}

fn main() {
    init_logging();
    let state = Game::new();
    if let Some(p) = std::env::args().nth(1) {
        state
            .network
            .write()
            .unwrap()
            .as_mut()
            .unwrap()
            .add(SocketAddr::from_str(&p).unwrap());
    }
    Graphics::run(state)
}
