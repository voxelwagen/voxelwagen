/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use crate::{state::PlayerInput, world::GameWorld};
use bincode::{Decode, Encode};
use voxelwagen_engine::{
    common::{
        face::Axis,
        range::RangeIter,
        vector::{pack::v3, Vec2, Vec3},
        BPos, EMat, EPos, EVec,
    },
    world::{World, WorldMut},
};
use voxelwagen_game_worldgen::blocks::Block;

#[derive(Clone, Encode, Decode)]
pub struct Player {
    flying: bool,
    input_direction: EPos,
    jump: bool,
    pub position: EPos,
    velocity: EVec,
    pitch: f32,
    yaw: f32,
    break_enable: bool,
}

impl Default for Player {
    fn default() -> Self {
        Self {
            flying: false,
            break_enable: false,
            position: EPos {
                x: 0.,
                y: 70.,
                z: 0.,
            },
            jump: false,
            pitch: 0.,
            yaw: 0.,
            input_direction: EVec::ZERO,
            velocity: EVec::ZERO,
        }
    }
}

impl Player {
    pub fn rotation(&self) -> EMat {
        EMat::rotation_y(self.pitch) * EMat::rotation_x(self.yaw)
    }
    pub fn view_facing(&self) -> EVec {
        self.rotation()
            * EVec {
                x: 0.,
                y: 0.,
                z: -1.,
            }
    }

    pub fn input(&mut self, input: PlayerInput) {
        match input {
            PlayerInput::SetMovementMode(f) => self.flying = f,
            PlayerInput::Movement(x) => self.input_direction = x,
            PlayerInput::Jump => self.jump = true,
            PlayerInput::Look(Vec2 { x, y }) => {
                self.pitch += x;
                self.yaw += y;
            }
            PlayerInput::Break(s) => self.break_enable = s,
        }
    }

    pub fn interact_tick(&mut self, world: &mut GameWorld, _dt: f32) {
        if self.break_enable {
            if let Some((_, b)) = self.raycast(world, 10.) {
                world.set(b, Block::Air)
            }
        }
    }

    fn raycast(&self, world: &GameWorld, max_d: f32) -> Option<(BPos, BPos)> {
        let facing = self.view_facing();
        let mut d = 0.;
        let step = 0.1;
        while d < max_d {
            let p = self.position + facing * d;
            let block = world.get(p.bpos());
            if block != Block::Air {
                return Some(((p - facing * step).bpos(), p.bpos()));
            }
            d += step;
        }
        None
    }

    pub fn movement_tick(&mut self, world: &GameWorld, dt: f32) {
        if self.flying {
            if self.input_direction != EPos::ZERO {
                self.velocity += (self.rotation() * self.input_direction.normalize() * 50.0) * dt;
            }
            self.position += self.velocity * dt;
            self.velocity *= (0.7f32).powf(dt);
        } else {
            let (mut on_ground, mut on_side) = (false, false);

            for ax in Axis::LIST {
                let next_position = self.position + self.velocity * ax.edir() * dt;
                let coll = collide(world, next_position);
                if coll {
                    self.velocity *= Vec3::splat(1.) - ax.edir();
                    match ax {
                        Axis::X => on_side = true,
                        Axis::Y => on_ground = true,
                        Axis::Z => on_side = true,
                    }
                } else {
                    self.position = next_position;
                }
            }

            // This is a bit weird but feels like good movement. If no buttons are pressed, the velocity will be compensated.
            let dir = (self.rotation() * self.input_direction)
                .xz()
                .normalize_or_zero()
                - self.velocity.xz() * 0.05;
            let dir_vel = dir * if on_ground { 50. } else { 10. } * dt;
            self.velocity.x += dir_vel.x;
            self.velocity.z += dir_vel.y;
            self.velocity.y -= 50. * dt;

            if on_ground || on_side {
                self.velocity *= (0.05f32).powf(dt);
            }
            if on_ground && self.jump {
                self.velocity.y += 20.;
            }
            self.jump = false;
        }
    }
}

fn collide(world: &GameWorld, pos: Vec3<f32>) -> bool {
    let mut acc = false;
    for p in RangeIter::new(v3(0i64, 0, 0)..v3(2, 2, 2), 1) {
        let p = pos + Vec3::splat(-0.5) + p.epos();
        if world.get(p.bpos()) != Block::Air {
            acc = true;
        }
    }
    acc
}
