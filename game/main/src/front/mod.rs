/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use voxelwagen_engine::{
    common::{color::Color, UVec},
    graphics::{defs::InputEvent, dyndraw, GameOutEvents},
    ui::{
        state::UiState,
        theme::Theme,
        widgets::{label, rect},
    },
};

pub struct Front {
    pub ui: UiState,

    pub active_slot: usize,
}

impl Default for Front {
    fn default() -> Self {
        Self {
            ui: UiState::new(Theme::default()),
            active_slot: Default::default(),
        }
    }
}
impl Front {
    pub fn new() -> Self {
        Self {
            active_slot: 0,
            ui: UiState::new(Theme::default()),
        }
    }

    pub fn input(&mut self, i: &InputEvent) {
        self.ui.input_event(i);
        if let InputEvent::MouseWheel(UVec { y, .. }) = i {
            if *y > 0. {
                self.active_slot += 1;
            }
            if *y < 0. && self.active_slot > 0 {
                self.active_slot -= 1;
            }
        }
    }

    pub fn update_ui(&mut self, out: &mut GameOutEvents) -> (Vec<dyndraw::Vertex>, Vec<u32>) {
        // self.ui.draw(wheel(
        //     self.active_slot,
        //     40.,
        //     &[
        //         "testing", "the", "wheel", "thing", "it", "probably", "doesnt", "work", "but", "i",
        //         "will", "try", "anyway", "running", "out", "of", "text",
        //     ],
        //     |a, s| {
        //         label(
        //             *s,
        //             40. + 40. * a,
        //             Color::mix(a, Color::grey(128), Color::grey(255)),
        //         )
        //     },
        // ));

        self.ui.draw(rect(
            Color::rgba(0, 0, 0, 100),
            label(&out.renderer_stats, 30., Color::WHITE),
        ));

        self.ui.finish()
    }
}
