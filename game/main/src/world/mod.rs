/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/

use bincode::{Decode, Encode};
use std::collections::BTreeMap;
use voxelwagen_engine::{
    common::BPos,
    world::{World, WorldMut},
};
use voxelwagen_game_worldgen::{blocks::Block, Worldgen};

#[derive(Clone, Encode, Decode)]
pub struct GameWorld {
    pub blocks: BTreeMap<BPos, Block>,
    pub invalid: Vec<BPos>,
    pub generator: Worldgen,
}

impl Default for GameWorld {
    fn default() -> Self {
        Self {
            blocks: Default::default(),
            generator: Worldgen::new(0xdeadbeef),
            invalid: Default::default(),
        }
    }
}

impl World<Block> for GameWorld {
    fn get(&self, p: BPos) -> Block {
        self.blocks
            .get(&p)
            .cloned()
            .unwrap_or_else(|| self.generator.generate(p))
    }
}
impl WorldMut<Block> for GameWorld {
    fn set(&mut self, p: BPos, b: Block) {
        self.blocks.insert(p, b);
        self.invalid.push(p);
    }
}
