/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
#![feature(if_let_guard)]

use bincode::{Decode, Encode};
use blocks::Block;
use voxelwagen_engine::common::BPos;
pub mod blocks;

#[derive(Clone, Encode, Decode)]
pub struct Worldgen {}
impl Worldgen {
    pub fn new(_seed: u32) -> Self {
        Worldgen {}
    }
    pub fn generate(&self, p: BPos) -> Block {
        let p = p.epos() * 0.01;

        let height = (p.x.sin() + p.z.sin()) - 1.2;
        if p.y > height {
            Block::Air
        } else {
            Block::Stone
        }
    }
}

// use bincode::{BorrowDecode, Decode, Encode};
// use blocks::Block::{self, *};
// use noise::{Fbm, MultiFractal, NoiseFn, Perlin, Turbulence};
// use voxelwagen_engine::common::{
//     matrix::{pack::mat2, Mat2},
//     vector::{
//         pack::{v2, v3},
//         Vec2, Vec3,
//     },
// };

// #[derive(Clone)]
// pub struct Worldgen {
//     seed: u32,
//     // Low frequency
//     temperature: Fbm<Perlin>,
//     humidity: Fbm<Perlin>,
//     continentality: Fbm<Perlin>,
//     erosion: Fbm<Perlin>,

//     // High frequency
//     land: Fbm<Perlin>,
//     mushroom: Vornoi,
//     vines: Perlin,
//     cave: Turbulence<Perlin, Perlin>,
//     ore: Fbm<Perlin>,
//     patches: Fbm<Perlin>,
//     random: XorshiftNoise,
// }

// impl Decode for Worldgen {
//     fn decode<D: bincode::de::Decoder>(
//         decoder: &mut D,
//     ) -> Result<Self, bincode::error::DecodeError> {
//         Ok(Self::new(Decode::decode(decoder)?))
//     }
// }
// impl Encode for Worldgen {
//     fn encode<E: bincode::enc::Encoder>(
//         &self,
//         encoder: &mut E,
//     ) -> Result<(), bincode::error::EncodeError> {
//         Encode::encode(&self.seed, encoder)
//     }
// }
// impl<'de> BorrowDecode<'de> for Worldgen {
//     fn borrow_decode<D: bincode::de::BorrowDecoder<'de>>(
//         decoder: &mut D,
//     ) -> Result<Self, bincode::error::DecodeError> {
//         Ok(Self::new(BorrowDecode::borrow_decode(decoder)?))
//     }
// }

// fn fbm(seeder: &mut Xorshift, f: f64) -> Fbm<Perlin> {
//     Fbm::new(seeder.gen())
//         .set_frequency(f)
//         .set_octaves(4)
//         .set_sources(vec![
//             Perlin::new(seeder.gen()),
//             Perlin::new(seeder.gen()),
//             Perlin::new(seeder.gen()),
//             Perlin::new(seeder.gen()),
//         ])
// }

// impl Worldgen {
//     pub fn new(seed: u32) -> Self {
//         let mut seeder = Xorshift(seed);
//         Self {
//             seed,
//             // Low frequency
//             temperature: fbm(&mut seeder, 0.005),
//             continentality: fbm(&mut seeder, 0.001),
//             erosion: fbm(&mut seeder, 0.0001),
//             humidity: fbm(&mut seeder, 0.005),

//             // High frequency
//             land: fbm(&mut seeder, 0.005),
//             vines: Perlin::new(seeder.gen()),
//             cave: Turbulence::new(Perlin::new(seeder.gen())),
//             mushroom: Vornoi,
//             ore: fbm(&mut seeder, 0.3),
//             patches: fbm(&mut seeder, 0.01),
//             random: XorshiftNoise(seeder.gen()),
//         }
//     }
//     pub fn generate(&self, p: Vec3<i64>) -> Block {
//         Sample {
//             p: p.f64(),
//             gen: &self,
//             density: None,
//             surface: None,
//         }
//         .generate()
//     }
// }
// pub struct Sample<'a> {
//     gen: &'a Worldgen,
//     p: Vec3<f64>,
//     density: Option<(f64, Vec3<f64>)>,
//     surface: Option<(Vec3<f64>, f64, Vec3<f64>)>,
// }

// impl Sample<'_> {
//     fn compute_land_density(&self, p: Vec3<f64>) -> f64 {
//         let land = self.gen.land.get(p.array());
//         let continentality = self.gen.continentality.get([p.x, p.z]);
//         let erosion = self.gen.erosion.get([p.x, p.z]);
//         // TODO fix erosion
//         land.remap(-5., 5.) - p.y * erosion.remap(0.01, 0.1) + continentality.remap(-5., 10.)
//     }
//     fn compute_land_density_grad(&self, p: Vec3<f64>) -> (f64, Vec3<f64>) {
//         let h = 0.01;
//         let s = self.compute_land_density(p);
//         (
//             s,
//             Vec3 {
//                 x: self.compute_land_density(p + Vec3 { x: h, y: 0., z: 0. }) - s,
//                 y: self.compute_land_density(p + Vec3 { x: 0., y: h, z: 0. }) - s,
//                 z: self.compute_land_density(p + Vec3 { x: 0., y: 0., z: h }) - s,
//             } / h,
//         )
//     }
//     fn compute_surface_grad(&self, mut p: Vec3<f64>) -> (Vec3<f64>, f64, Vec3<f64>) {
//         let mut last_x = 0.;
//         let mut last_dx = Vec3 {
//             x: 0.,
//             y: 0.,
//             z: 0.,
//         };
//         for _ in 0..8 {
//             let (x, dx) = self.compute_land_density_grad(p);
//             p += dx * x * -30.;
//             last_x = x;
//             last_dx = dx;
//             if x.abs() < 0.1 {
//                 break;
//             }
//         }
//         (p, last_x, last_dx)
//     }

//     fn density(&mut self) -> (f64, Vec3<f64>) {
//         match self.density {
//             Some(x) => x,
//             None => {
//                 let x = self.compute_land_density_grad(self.p);
//                 self.density = Some(x);
//                 x
//             }
//         }
//     }
//     fn surface(&mut self) -> (Vec3<f64>, f64, Vec3<f64>) {
//         match self.surface {
//             Some(x) => x,
//             None => {
//                 let x = self.compute_surface_grad(self.p);
//                 self.surface = Some(x);
//                 x
//             }
//         }
//     }

//     fn surface_vegetation(&mut self) -> Option<Block> {
//         let (p_surface, surface_density, density_grad) = self.surface().to_owned();
//         let height = self.p.distance(p_surface);
//         let up = density_grad.y < 0.;
//         let temperature = self.gen.temperature.get(self.p.array());
//         let humidity = self.gen.humidity.get(self.p.array());

//         if surface_density.abs() > 0.1 || p_surface.y < 0. {
//             return None;
//         }

//         if up && humidity > 0. && temperature > 0. {
//             let vorn = self.gen.mushroom.get(p_surface.xy() * 0.06);
//             if vorn < 0.3 && height > 10. && height <= 11. {
//                 return Some(MushroomCapTop);
//             }
//             if vorn < 0.3 && height > 9. && height <= 10. {
//                 return Some(MushroomCapBottom);
//             }
//             if vorn < 0.05 && height <= 9. {
//                 return Some(MushroomStem);
//             }
//         }
//         if density_grad.normalize().xz().length() > 0.9
//             && self.gen.vines.get((self.p * v3(0.2, 0.01, 0.2)).into()) > 0.7
//         {
//             if height > 4. && height < 5. {
//                 return Some(Vines);
//             }
//         }

//         None
//     }

//     fn cave(&mut self) -> bool {
//         self.gen.cave.get((self.p * 0.01).array()) > 0.5
//     }
//     fn seafloor(&self) -> Block {
//         if self.gen.patches.get(self.p.array()) > 0.8 {
//             if self.gen.random.get(self.p.array()) > 0.99 {
//                 SeafloorShells
//             } else {
//                 Seafloor
//             }
//         } else {
//             Stone
//         }
//     }

//     pub fn generate(&mut self) -> Block {
//         use Block::*;

//         let (density, density_grad) = self.density();
//         let up = density_grad.y < 0.;

//         let sea = self.p.y < 0.;
//         let ore = self.gen.ore.get(self.p.array()) > 0.97;

//         #[allow(overlapping_range_endpoints)]
//         match (density * 10.) as i64 {
//             -4..=0 if sea => self.seafloor(),
//             ..=-4 if sea => Water,
//             -20..=0 if let Some(b) = self.surface_vegetation() => b,
//             ..=-5 => Air,
//             _ if self.cave() => Air,
//             -4..=-2 if up && !sea => Grass,
//             -2..=0 if up => Dirt,
//             20.. if ore => Ore,
//             ..=60 => Stone,
//             _ => Deepstone,
//         }
//     }
// }

// #[derive(Debug, Clone, Copy)]
// struct XorshiftNoise(pub u32);
// impl XorshiftNoise {
//     pub fn get(&self, [x, y, z]: [f64; 3]) -> f64 {
//         fn xs(mut x: u32) -> u32 {
//             x ^= x << 13;
//             x ^= x >> 17;
//             x ^= x << 5;
//             x
//         }
//         let [x, y, z] = [x, y, z].map(|c| c as i32 as u32);
//         let k = xs(xs(self.0 ^ xs(x ^ xs(y ^ xs(z)))));
//         k as f64 / u32::MAX as f64
//     }
// }

// #[derive(Debug, Clone, Copy)]
// struct Vornoi;
// impl Vornoi {
//     pub fn get(&self, p: Vec2<f64>) -> f64 {
//         const RS: Mat2<f64> = mat2(3324.25341432, 7546.2354124, 2345.32456, 6578.409232);
//         fn random(s: Vec2<f64>) -> Vec2<f64> {
//             (((RS * s).sin() * 34587.123).fract() - v2(0.5, 0.5)) * 0.8
//         }
//         let f = p.fract();
//         let k = p.floor();
//         let p1 = f - random(k + v2(0., 0.)) + v2(0., 0.);
//         let p2 = f - random(k + v2(1., 0.)) + v2(1., 0.);
//         let p3 = f - random(k + v2(0., 1.)) + v2(0., 1.);
//         let p4 = f - random(k + v2(1., 1.)) + v2(1., 1.);
//         return (p1.length().min(p2.length())).min(p3.length().min(p4.length()));
//     }
// }

// pub struct Xorshift(u32);
// impl Xorshift {
//     pub fn gen(&mut self) -> u32 {
//         self.0 ^= self.0 << 13;
//         self.0 ^= self.0 >> 17;
//         self.0 ^= self.0 << 5;
//         self.0
//     }
// }

// trait FloatExt {
//     fn remap(self, min: Self, max: Self) -> Self;
// }
// impl FloatExt for f64 {
//     #[inline(always)]
//     fn remap(self, min: Self, max: Self) -> Self {
//         (self * 0.5 + 0.5) * (max - min) + min
//     }
// }
