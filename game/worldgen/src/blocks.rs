/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use bincode::{Decode, Encode};
use voxelwagen_engine::common::{model::Model, BlockDefinition, Material};

pub fn block_defs() -> Vec<BlockDefinition> {
    // let atlas = image::open("assets/atlas.png").unwrap().into_rgba8();
    let atlas = image::load_from_memory(include_bytes!("../../assets/atlas.png"))
        .unwrap()
        .into_rgba8();
    Block::defs()
        .iter()
        .map(|e| {
            let mut model = match e {
                BD::Texture(t, r, e) => {
                    let mut tex: [[[u8; 4]; 16]; 16] = Default::default();
                    #[allow(clippy::needless_range_loop)]
                    for x in 0..16 {
                        for y in 0..16 {
                            tex[x][y] = atlas
                                .get_pixel(x as u32 + t[0] * 16, y as u32 + t[1] * 16)
                                .0
                        }
                    }
                    Model::from_textures(&[tex, tex, tex, tex, tex, tex], true, *r, *e)
                }
                BD::Mat(e) => Model([[[*e; 16]; 16]; 16]),
            };
            model.fill_internal_spaces();
            BlockDefinition {
                name: String::new(),
                model,
            }
        })
        .collect()
}

macro_rules! define_blocks {
    (enum $i:ident { $($name:ident = $def:expr),* }) => {
        #[derive(Debug, Clone, Copy, Encode, Decode, PartialEq, Eq)]
        pub enum $i { $($name),* }
        impl $i { fn defs() -> &'static [BD] {
            &[$($def),*]
        }}
    };
}

enum BD {
    Texture([u32; 2], u8, u8),
    Mat(Material),
}
use BD::*;

define_blocks! {enum Block {
    Air = Mat(Material::AIR),
    Stone = Texture([11,15],0,0),
    Deepstone = Texture([9,15],0,0),
    Ore = Texture([10,15],0,250),
    Dirt = Texture([13,12],0,0),
    Grass = Texture([9,13],0,0),
    Seafloor = Texture([12,12],0,0),
    SeafloorShells = Texture([11,12],0,0),

    Vines = Texture([12,15],0,0),
    MushroomRoot = Texture([10,14],0,5),
    MushroomStem = Texture([11,14],0,5),
    MushroomCapTop = Texture([12,14],0,5),
    MushroomCapBottom = Texture([13,14],0,5),

    Player = Texture([6,13],0,20),

    Water = Mat(Material { color: [0, 0, 30], alpha: 180, roughness: 1, flags: Material::FLAG_FLUID, emission: 0, _pad: 0, })
}}
