/*
    This file is part of voxelwagen (https://codeberg.org/voxelwagen/voxelwagen)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2024 metamuffin <metamuffin.org>
*/
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rand::random;
use voxelwagen_engine::common::vector::Vec3;
use voxelwagen_game_worldgen::Worldgen;

pub fn criterion_benchmark(c: &mut Criterion) {
    let gen = Worldgen::new(0xdeadbeef);
    c.bench_function("worldgen_generate", |b| {
        b.iter(|| {
            gen.generate(black_box(Vec3 {
                x: (random::<f32>() * 1000f32) as i64,
                y: (random::<f32>() * 1000f32) as i64,
                z: (random::<f32>() * 1000f32) as i64,
            }))
        })
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
